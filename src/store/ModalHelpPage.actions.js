import { push } from 'connected-react-router';
import * as ModalActions from './Modal.actions';
import { postHelpPage, putHelpPage, deleteHelpPage } from './Help.actions';

export const ADD_HELP_PAGE = 'ADD_HELP_PAGE';
export const EDIT_HELP_PAGE = 'EDIT_HELP_PAGE';

// flock actions
export const openDialogAddHelpPage = data => (dispatch) => {
  dispatch({ type: ModalActions.OPEN_DIALOG, target: ADD_HELP_PAGE, payload: data });
};

export const openDialogEditHelpPage = data => (dispatch) => {
  dispatch({ type: ModalActions.OPEN_DIALOG, target: EDIT_HELP_PAGE, payload: data });
};

export const finishDialogAddHelpPage = data => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => postHelpPage(data), ADD_HELP_PAGE)(dispatch)
    .then(() => {
      dispatch(push(`/help/${data.slug}`));
    });
};

export const finishDialogEditHelpPage = data => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => putHelpPage(data), EDIT_HELP_PAGE)(dispatch)
    .then(() => {
      dispatch(push(`/help/${data.slug}`));
    });
};

export const deleteDialogHelpPage = id => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => deleteHelpPage(id), EDIT_HELP_PAGE)(dispatch)
    .then(() => dispatch(push('/help')));
};

export default ModalActions;
