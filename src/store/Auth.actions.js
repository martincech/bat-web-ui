import auth from '../utils/auth';
import rest from './rest.actions';
import UserApi from '@veit/bat-cloud-api/lib/api/UserApi';

export const LOGIN = 'LOGIN';
export const LOGIN_REDIRECT = 'LOGIN_REDIRECT';
export const LOGOUT_REDIRECT = 'LOGOUT_REDIRECT';
export const REFRESH = 'REFRESH';
export const GET_AUTH_USER = 'GET_AUTH_USER';

const restUser = rest(new UserApi());

export const loginRedirect = () => async (dispatch) => {
  dispatch({ type: LOGIN_REDIRECT });
  auth.signIn();
};

export const logoutRedirect = () => async (dispatch) => {
  dispatch({ type: LOGOUT_REDIRECT });
  auth.signOut();
};

export const login = () => (dispatch) => {
  dispatch({ type: LOGIN });
};

export const refresh = () => async (dispatch) => {
  dispatch({ type: REFRESH });
};

export const getCurrentUser = restUser.createGetBy(GET_AUTH_USER, 'userCurrentGet');
