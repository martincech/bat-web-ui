import {
  GET_USER,
  GETBY_USER,
  POST_USER,
  PUT_USER,
  DELETE_USER,
} from './User.actions';

const initialState = {
  items: [],
  item: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER:
      return { ...state, items: action.payload || [] };
    case GETBY_USER:
      return { ...state, item: action.payload };
    case POST_USER:
      return { ...state, items: [action.payload, ...state.items] };
    case PUT_USER:
      return {
        ...state,
        item: action.payload,
        items: state.items.map(f => (f.id === action.payload.id ? action.payload : f)),
      };
    case DELETE_USER:
      return { ...state, items: state.items.filter(f => f.id !== action.payload) };
    default:
      return state;
  }
};


export default reducer;
