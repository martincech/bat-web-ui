import {
  GET_BIRD,
  GET_PUBLIC_BIRD,
  GETBY_BIRD,
  POST_BIRD,
  PUT_BIRD,
  DELETE_BIRD,
} from './Bird.actions';

const initialState = {
  items: [],
  public: [],
  item: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_BIRD:
      return { ...state, items: action.payload || [] };
    case GET_PUBLIC_BIRD:
      return { ...state, public: action.payload };
    case GETBY_BIRD:
      return { ...state, item: action.payload };
    case POST_BIRD:
      return { ...state, items: [...state.items, action.payload] };
    case PUT_BIRD:
      return {
        ...state,
        item: action.payload,
        items: state.items.map(f => (f.id === action.payload.id ? action.payload : f)),
      };
    case DELETE_BIRD:
      return { ...state, items: state.items.filter(f => f.id !== action.payload) };
    default:
      return state;
  }
};


export default reducer;
