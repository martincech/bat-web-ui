import * as ModalActions from './Modal.actions';
import {
  deleteFarm, putFarm, postFarm, getFarm, getFarmBy, DELETE_FARM, expandFarm,
} from './Farm.actions';

export const ADD_FARM = 'ADD_FARM';
export const EDIT_FARM = 'EDIT_FARM';

// flock actions
export const openDialogAddFarm = data => (dispatch) => {
  dispatch({ type: ModalActions.OPEN_DIALOG, target: ADD_FARM, payload: data });
};

export const openDialogEditFarm = farmId => (dispatch) => {
  getFarmBy(farmId, expandFarm.detail())(dispatch).then((data) => {
    dispatch({ type: ModalActions.OPEN_DIALOG, target: EDIT_FARM, payload: data });
  });
};

export const finishDialogAddFarm = data => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => postFarm(data), ADD_FARM)(dispatch)
    .then(() => {
      getFarm(expandFarm.list())(dispatch);
    });
};

export const finishDialogEditFarm = data => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => putFarm(data.id, data), EDIT_FARM)(dispatch)
    .then(() => {
      getFarm(expandFarm.list())(dispatch);
    });
};

export const deleteDialogFarm = id => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => deleteFarm(id), DELETE_FARM)(dispatch);
};

export default ModalActions;
