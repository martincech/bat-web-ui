import {
  GET_DEVICE,
  GETBY_DEVICE,
  GETBY_PARENT_DEVICE,
  POST_DEVICE,
  PUT_DEVICE,
  DELETE_DEVICE,
} from './Device.actions';

const initialState = {
  items: [],
  item: null,
  parent: null,
};

function mapUpdate(item, update) {
  if (item == null || update == null || item.id !== update.id) return item;
  return {
    ...update,
    lastStatistics: item.lastStatistics,
  };
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DEVICE:
      return { ...state, items: action.payload || [] };
    case GETBY_DEVICE:
      return { ...state, item: action.payload };
    case GETBY_PARENT_DEVICE:
      return { ...state, parent: action.payload };
    case POST_DEVICE:
      return { ...state, items: [...state.items, action.payload] };
    case PUT_DEVICE:
      return {
        ...state,
        item: action.payload,
        items: state.items.map(f => mapUpdate(f, action.payload)),
      };
    case DELETE_DEVICE:
      return { ...state, items: state.items.filter(f => f.id !== action.payload) };
    default:
      return state;
  }
};


export default reducer;
