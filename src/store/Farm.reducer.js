import {
  GET_FARM,
  GETBY_FARM,
  POST_FARM,
  PUT_FARM,
  DELETE_FARM,
} from './Farm.actions';

const initialState = {
  items: [],
  item: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_FARM:
      return { ...state, items: action.payload || [] };
    case GETBY_FARM:
      return { ...state, item: action.payload };
    case POST_FARM:
      return { ...state, items: [action.payload, ...state.items] };
    case PUT_FARM:
      return {
        ...state,
        item: action.payload,
        items: state.items.map(f => (f.id === action.payload.id ? action.payload : f)),
      };
    case DELETE_FARM:
      return { ...state, items: state.items.filter(f => f.id !== action.payload) };
    default:
      return state;
  }
};


export default reducer;
