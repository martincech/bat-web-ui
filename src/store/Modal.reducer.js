import {
  OPEN_DIALOG,
  FINISH_DIALOG,
  FINISH_DIALOG_REQUEST,
  FINISH_DIALOG_ERROR,
  CANCEL_DIALOG,
  UPDATE_DIALOG_FORM,
  SET_STEP_DIALOG,
  SET_DIALOG_VALIDATORS,
  DIALOG_SET_LOADER,
} from './Modal.actions';

function errors(state) {
  if (state == null || state.data == null || state.validators == null) return {};
  let result = {};
  Object.keys(state.validators)
    .forEach((key) => {
      const validatator = state.validators[key];
      if (Array.isArray(validatator)) {
        const res = validatator
          .filter(f => f instanceof Function)
          .reduce((r, v) => ({ ...r, ...v(state.data) }), {});

        result = { ...result, [key]: res };
      }

      if (typeof validatator === 'function') {
        result = { ...result, [key]: validatator(state.data) };
      }
      result[key].hasError = Object.keys((result[key] || {}))
        .reduce((r, v) => r || result[key][v], false);
    });
  return result;
}

const initialState = {};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_DIALOG:
      return {
        type: action.type,
        data: Object.assign({}, action.payload || {}),
        base: Object.assign({}, action.payload || {}),
        target: action.target,
        step: 1,
      };
    case CANCEL_DIALOG:
      return {};
    case FINISH_DIALOG:
      return { update: true };
    case SET_STEP_DIALOG: {
      const newState = {
        ...state,
        step: action.payload,
      };
      return {
        ...newState,
        errors: errors(newState),
      };
    }
    case UPDATE_DIALOG_FORM: {
      const newState = {
        ...state,
        data: {
          ...state.data,
          ...action.payload,
        },
      };
      return {
        ...newState,
        errors: errors(newState),
      };
    }
    case SET_DIALOG_VALIDATORS: {
      const newState = {
        ...state,
        validators: action.payload,
      };
      return {
        ...newState,
        errors: errors(newState),
      };
    }
    case FINISH_DIALOG_REQUEST:
      return {
        ...state,
        isExecuting: true,
      };
    case FINISH_DIALOG_ERROR:
      return {
        ...state,
        isExecuting: false,
      };
    case DIALOG_SET_LOADER:
      return {
        ...state,
        isExecuting: action.payload === true,
      };
    default:
      return state;
  }
};


export default reducer;
