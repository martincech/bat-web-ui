import * as ModalActions from './Modal.actions';
import toastMessage from '../utils/toastMessage';
import sendFeedback from '../utils/feedback';

export const SEND_FEEDBACK = 'SEND_FEEDBACK';

export const openDialogSendFeedback = data => (dispatch) => {
  dispatch({ type: ModalActions.OPEN_DIALOG, target: SEND_FEEDBACK, payload: data });
};

export const finishDialogSendFeedback = data => (dispatch, getState) => {
  ModalActions
    .finishDialogRequest(() => () => sendFeedback(data, getState(), 'feedback'), SEND_FEEDBACK)(dispatch)
    .then(() => toastMessage.success('Thanks for the feedback!'));
};

export default ModalActions;
