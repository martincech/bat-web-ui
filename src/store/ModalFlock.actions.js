import { push } from 'connected-react-router';
import dayjs from 'dayjs';
import * as ModalActions from './Modal.actions';
import { postFlock, putFlock, deleteFlock } from './Flock.actions';

export const ADD_FLOCK = 'ADD_FLOCK';
export const EDIT_FLOCK = 'EDIT_FLOCK';

const newFlockTemplate = () => ({
  targetAge: 150,
  initialAge: 1,
  initialWeight: 40,
  startDate: dayjs().format('YYYY-MM-DD'),
});

// flock actions
export const openDialogAddFlock = data => (dispatch) => {
  dispatch({
    type: ModalActions.OPEN_DIALOG, target: ADD_FLOCK, payload: data || newFlockTemplate(),
  });
};

export const openDialogEditFlock = data => (dispatch) => {
  dispatch({ type: ModalActions.OPEN_DIALOG, target: EDIT_FLOCK, payload: data });
};

export const finishDialogAddFlock = data => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => postFlock(data), ADD_FLOCK)(dispatch)
    .then((f) => {
      dispatch(push(`/flock/${f.id}`));
    });
};

export const finishDialogEditFlock = data => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => putFlock(data.id, data), EDIT_FLOCK)(dispatch);
};

export const deleteDialogFlock = id => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => deleteFlock(id), EDIT_FLOCK)(dispatch)
    .then(() => {
      dispatch(push('/flocks'));
    });
};

export default ModalActions;
