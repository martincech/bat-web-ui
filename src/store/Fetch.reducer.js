import { FETCH_ALL, FETCH_ALL_ERROR, FETCH_ALL_SUCCESS } from './Fetch.actions';

const initialState = {
  isFetching: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALL:
      return { isFetching: true };
    case FETCH_ALL_SUCCESS:
      return { isFetching: false, error: false };
    case FETCH_ALL_ERROR:
      return { isFetching: false, error: true };
    default:
      return state;
  }
};

export default reducer;
