import {
  GET_FLOCK,
  GETBY_FLOCK,
  POST_FLOCK,
  PUT_FLOCK,
  DELETE_FLOCK,
} from './Flock.actions';

import { sortByProgress } from '../utils/flock';

const initialState = {
  items: [],
  item: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_FLOCK:
      return { ...state, items: sortByProgress(action.payload) };
    case GETBY_FLOCK:
      return { ...state, item: action.payload };
    case POST_FLOCK:
      return { ...state, items: sortByProgress([...state.items, action.payload]) };
    case PUT_FLOCK:
      return {
        ...state,
        item: action.payload,
        items: state.items.map(f => (f.id === action.payload.id ? action.payload : f)),
      };
    case DELETE_FLOCK:
      return {
        ...state,
        items: state.items.filter(f => f.id !== action.payload),
      };
    default:
      return state;
  }
};


export default reducer;
