import * as ModalActions from './Modal.actions';
import {
  deleteHouse, putHouse, postHouse, getHouse, expandHouse,
} from './House.actions';

export const ADD_HOUSE = 'ADD_HOUSE';
export const EDIT_HOUSE = 'EDIT_HOUSE';

// flock actions
export const openDialogAddHouse = data => (dispatch) => {
  dispatch({ type: ModalActions.OPEN_DIALOG, target: ADD_HOUSE, payload: data });
};

export const openDialogEditHouse = data => (dispatch) => {
  dispatch({ type: ModalActions.OPEN_DIALOG, target: EDIT_HOUSE, payload: data });
};

export const finishDialogAddHouse = data => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => postHouse(data), ADD_HOUSE)(dispatch)
    .then(() => {
      getHouse(expandHouse.scalesCount)(dispatch);
    });
};

export const finishDialogEditHouse = data => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => putHouse(data.id, data), EDIT_HOUSE)(dispatch)
    .then(() => {
      getHouse(expandHouse.scalesCount)(dispatch);
    });
};

export const deleteDialogHouse = id => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => deleteHouse(id), EDIT_HOUSE)(dispatch);
};

export default ModalActions;
