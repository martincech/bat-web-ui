export const FETCH_ALL = 'FETCH_ALL';
export const FETCH_ALL_SUCCESS = 'FETCH_ALL_SUCCESS';
export const FETCH_ALL_ERROR = 'FETCH_ALL_ERROR';

export const fetchAll = promises => (dispatch) => {
  dispatch({ type: FETCH_ALL });
  Promise.all(promises())
    .then(() => dispatch({ type: FETCH_ALL_SUCCESS }))
    .catch(error => dispatch({ type: FETCH_ALL_ERROR, payload: error }));
};
