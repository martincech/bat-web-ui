

import { LOGIN, REFRESH, GET_AUTH_USER } from './Auth.actions';
import auth from '../utils/auth';

const isAuthenticated = auth.isUserSignedIn();
auth.updateApiHeader();
const initialState = {
  isAuthenticated,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
    case REFRESH:
      return { ...state, isAuthenticated: true };
    case GET_AUTH_USER:
      return { ...state, user: action.payload };
    default:
      return state;
  }
};

export default reducer;
