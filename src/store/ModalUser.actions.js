import * as ModalActions from './Modal.actions';
import toastMessage from '../utils/toastMessage';
import {
  deleteUser, putUser, postUser, resetPasswordUser, registerUser,
} from './User.actions';

export const INVITE_USER = 'INVITE_USER';
export const EDIT_USER = 'EDIT_USER';
export const REGISTER_USER = 'REGISTER_USER';

export const openDialogInviteUser = data => (dispatch) => {
  dispatch({ type: ModalActions.OPEN_DIALOG, target: INVITE_USER, payload: data });
};

export const openDialogEditUser = data => (dispatch) => {
  dispatch({ type: ModalActions.OPEN_DIALOG, target: EDIT_USER, payload: data });
};

export const finishDialogInviteUser = data => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => postUser(data), INVITE_USER)(dispatch);
};

export const finishDialogEditUser = data => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => putUser(data.id, data), EDIT_USER)(dispatch);
};

export const deleteDialogUser = id => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => deleteUser(id), EDIT_USER)(dispatch);
};

export const resetPasswordDialogUser = user => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => resetPasswordUser(user.id), EDIT_USER)(dispatch)
    .then(() => toastMessage.success(`New password was sent to ${user.email} for user ${user.name}!`));
};

export const openDialogRegisterUser = () => (dispatch) => {
  dispatch({ type: ModalActions.OPEN_DIALOG, target: REGISTER_USER, payload: {} });
};

export const finishDialogRegisterUser = user => (dispatch) => {
  ModalActions.setLoader(true)(dispatch);
  return registerUser(user)(dispatch)
    .then((result) => {
      ModalActions.setLoader(false)(dispatch);
      return { status: 201, data: result.data };
    }).catch((e) => {
      ModalActions.setLoader(false)(dispatch);
      return e;
    });
};

export default ModalActions;
