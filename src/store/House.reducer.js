import {
  GET_HOUSE,
  GETBY_HOUSE,
  POST_HOUSE,
  PUT_HOUSE,
  DELETE_HOUSE,
} from './House.actions';

const initialState = {
  items: [],
  item: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_HOUSE:
      return { ...state, items: action.payload || [] };
    case GETBY_HOUSE:
      return { ...state, item: action.payload };
    case POST_HOUSE:
      return { ...state, items: [action.payload, ...state.items] };
    case PUT_HOUSE:
      return {
        ...state,
        item: action.payload,
        items: state.items.map(f => (f.id === action.payload.id ? action.payload : f)),
      };
    case DELETE_HOUSE:
      return { ...state, items: state.items.filter(f => f.id !== action.payload) };
    default:
      return state;
  }
};


export default reducer;
