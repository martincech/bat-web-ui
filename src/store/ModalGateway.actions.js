import * as ModalActions from './Modal.actions';
import toastMessage from '../utils/toastMessage';
import sendFeedback from '../utils/feedback';

export const SEND_GATEWAY_REQUEST = 'SEND_GATEWAY_REQUEST';

export const openDialogSendGatewayRequest = () => (dispatch) => {
  dispatch({
    type: ModalActions.OPEN_DIALOG,
    target: SEND_GATEWAY_REQUEST,
    payload: {},
  });
};

export const finishDialogSendGatewayRequest = data => (dispatch, getState) => {
  ModalActions
    .finishDialogRequest(() => () => sendFeedback(data, getState(), 'gateway_request'), SEND_GATEWAY_REQUEST)(dispatch)
    .then(() => toastMessage.success('Thanks for the feedback!'));
};

export default ModalActions;
