import rest from './rest.actions';
import DeviceApi from '@veit/bat-cloud-api/lib/api/DeviceApi';

export const DEVICE = 'DEVICE';
export const GET_DEVICE = 'GET_DEVICE';
export const GETBY_PARENT_DEVICE = 'GETBY_PARENT_DEVICE';
export const GETBY_DEVICE = 'GETBY_DEVICE';
export const POST_DEVICE = 'POST_DEVICE';
export const PUT_DEVICE = 'PUT_DEVICE';
export const DELETE_DEVICE = 'DELETE_DEVICE';

const expand = {
  description: 'description',
  farm: 'farm',
  house: 'house',
  lastStatistics: 'lastStatistics',
};

export const expandDevice = Object.freeze({
  ...expand,
  all: Object.values(expand),
});

const restDevice = rest(new DeviceApi());

export const getDevice = restDevice.createGet(GET_DEVICE, 'deviceGet');
export const getDeviceBy = restDevice.createGetBy(GETBY_DEVICE, 'deviceDeviceIdGet');
export const getDeviceByParent = restDevice.createGetBy(GETBY_PARENT_DEVICE, 'deviceDeviceIdGet');
export const postDevice = restDevice.createPost(POST_DEVICE, 'devicePost');
export const putDevice = restDevice.createPut(PUT_DEVICE, 'deviceDeviceIdPut');
export const deleteDevice = restDevice.createDelete(DELETE_DEVICE, 'deviceDeviceIdDelete');
