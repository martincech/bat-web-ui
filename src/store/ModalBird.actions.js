import { push } from 'connected-react-router';
import * as ModalActions from './Modal.actions';
import {
  deleteBird, putBird, postBird, DELETE_BIRD,
} from './Bird.actions';

export const ADD_BIRD = 'ADD_BIRD';
export const EDIT_BIRD = 'EDIT_BIRD';

// bird actions
export const openDialogAddBird = data => (dispatch) => {
  ModalActions.openDialog(ADD_BIRD, data)(dispatch);
};

export const openDialogEditBird = data => (dispatch) => {
  ModalActions.openDialog(EDIT_BIRD, data)(dispatch);
};

export const finishDialogAddBird = data => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => postBird(data), ADD_BIRD)(dispatch)
    .then((result) => {
      dispatch(push(`/bird/${result.id}`));
    });
};

export const finishDialogEditBird = data => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => putBird(data.id, data), EDIT_BIRD)(dispatch);
};

export const deleteDialogBird = id => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => deleteBird(id), DELETE_BIRD)(dispatch)
    .then(() => {
      dispatch(push('/birds'));
    });
};

export default ModalActions;
