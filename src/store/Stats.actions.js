
import rest from './rest.actions';
import StatisticApi from '@veit/bat-cloud-api/lib/api/StatisticApi';

export const STATS = 'STATS';

export const GETBY_FLOCK_STATS = 'GETBY_FLOCK_STATS';
export const GETBY_DEVICE_STATS = 'GETBY_DEVICE_STATS';

const restStatsFlock = rest(new StatisticApi());
const restStatsDevice = rest(new StatisticApi());

export const getStatsFlockBy = restStatsFlock.createGetBy(GETBY_FLOCK_STATS, 'statisticAllByFlockFlockIdGet');
export const getStatsDeviceBy = restStatsDevice.createGetBy(GETBY_DEVICE_STATS, 'statisticAllByDeviceDeviceIdGet');
