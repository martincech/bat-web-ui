import {
  GETBY_DEVICE_STATS,
  GETBY_FLOCK_STATS,
} from './Stats.actions';

const initialState = {
  item: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GETBY_DEVICE_STATS:
    case GETBY_FLOCK_STATS:
      return { ...state, item: action.payload };
    default:
      return state;
  }
};


export default reducer;
