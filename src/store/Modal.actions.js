export const OPEN_DIALOG = 'OPEN_DIALOG';
export const FINISH_DIALOG = 'FINISH_DIALOG';
export const FINISH_DIALOG_REQUEST = 'FINISH_DIALOG_REQUEST';
export const FINISH_DIALOG_ERROR = 'FINISH_DIALOG_ERROR';
export const CANCEL_DIALOG = 'CANCEL_DIALOG';
export const UPDATE_DIALOG_FORM = 'UPDATE_DIALOG_FORM';
export const SET_STEP_DIALOG = 'SET_STEP_DIALOG';
export const SET_DIALOG_VALIDATORS = 'SET_DIALOG_VALIDATORS';
export const DIALOG_SET_LOADER = 'DIALOG_SET_LOADER';


export const isOpen = (modal, target) => {
  return modal != null && modal.target === target && modal.type === OPEN_DIALOG;
};

export const setValidation = validators => (dispatch) => {
  dispatch({ type: SET_DIALOG_VALIDATORS, payload: validators });
};

export const updateForm = (value, name) => (dispatch) => {
  dispatch({ type: UPDATE_DIALOG_FORM, payload: { [name]: value } });
};

export const updateFormObject = object => (dispatch) => {
  dispatch({ type: UPDATE_DIALOG_FORM, payload: object });
};

export const updateFormEvent = (event, name) => (dispatch) => {
  const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
  updateForm(value, name)(dispatch);
};

export const openDialog = (target, data) => (dispatch) => {
  dispatch({ type: OPEN_DIALOG, target, payload: data });
};

export const finishDialog = (target, data) => (dispatch) => {
  dispatch({ type: FINISH_DIALOG, target, payload: data });
};

export const finishDialogRequest = (action, target, data) => (dispatch) => {
  dispatch({ type: FINISH_DIALOG_REQUEST, target, payload: data });
  return action()(dispatch)
    .then((response) => {
      dispatch({ type: FINISH_DIALOG, target, payload: data });
      return response;
    })
    .catch((error) => {
      dispatch({ type: FINISH_DIALOG_ERROR, target, payload: error });
      throw error;
    });
};

export const cancelDialog = () => (dispatch) => {
  dispatch({ type: CANCEL_DIALOG });
};

export const setStep = step => (dispatch) => {
  dispatch({ type: SET_STEP_DIALOG, payload: step });
};

export const setLoader = isExecuting => (dispatch) => {
  dispatch({ type: DIALOG_SET_LOADER, payload: isExecuting });
};
