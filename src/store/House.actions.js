import rest from './rest.actions';
import HouseApi from '@veit/bat-cloud-api/lib/api/HouseApi';

export const HOUSE = 'HOUSE';
export const GET_HOUSE = 'GET_HOUSE';
export const GETBY_HOUSE = 'GETBY_HOUSE';
export const POST_HOUSE = 'POST_HOUSE';
export const PUT_HOUSE = 'PUT_HOUSE';
export const DELETE_HOUSE = 'DELETE_HOUSE';

const expand = {
  address: 'farm',
  scalesCount: 'scalesCount',
};

export const expandHouse = Object.freeze({
  ...expand,
  all: Object.values(expand),
});

const restHouse = rest(new HouseApi());

export const getHouse = restHouse.createGet(GET_HOUSE, 'houseGet');
export const getHouseBy = restHouse.createGetBy(GETBY_HOUSE, 'houseHouseIdGet');
export const postHouse = restHouse.createPost(POST_HOUSE, 'housePost');
export const putHouse = restHouse.createPut(PUT_HOUSE, 'houseHouseIdPut');
export const deleteHouse = restHouse.createDelete(DELETE_HOUSE, 'houseHouseIdDelete');
