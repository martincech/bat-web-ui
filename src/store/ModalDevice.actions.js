import { push } from 'connected-react-router';
import * as ModalActions from './Modal.actions';
import {
  deleteDevice, postDevice, putDevice, DELETE_DEVICE,
} from './Device.actions';
import { deviceType } from '../utils/enums';

export const ADD_DEVICE = 'ADD_DEVICE';
export const EDIT_DEVICE = 'EDIT_DEVICE';

export const openDialogAddDevice = data => (dispatch) => {
  ModalActions.openDialog(ADD_DEVICE, data)(dispatch);
};

export const openDialogEditDevice = data => (dispatch) => {
  ModalActions.openDialog(EDIT_DEVICE, data)(dispatch);
};

export const finishDialogAddDevice = data => (dispatch) => {
  if (data.type === deviceType.bat2Gsm) {
    ModalActions
      .finishDialogRequest(() => postDevice(data), ADD_DEVICE)(dispatch)
      .then(() => {
        dispatch(push('/help/connect-bat2-gsm'));
      });
  } else {
    ModalActions.finishDialog()(dispatch);
    dispatch(push('/help/connect-bat2-cable'));
  }
};

export const finishDialogEditDevice = data => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => putDevice(data.id, data), EDIT_DEVICE)(dispatch);
};

export const deleteDialogDevice = (id, redirect = 'scales') => (dispatch) => {
  ModalActions
    .finishDialogRequest(() => deleteDevice(id), DELETE_DEVICE)(dispatch)
    .then(() => {
      dispatch(push(`/${redirect}`));
    });
};

export default ModalActions;
