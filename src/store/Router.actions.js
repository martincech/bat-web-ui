import { push } from 'connected-react-router';

const goTo = url => (dispatch) => {
  dispatch(push(url));
};

export default goTo;
