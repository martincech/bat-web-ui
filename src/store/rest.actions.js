import validateToken from '../utils/auth-validate';
import auth from '../utils/auth';
import { getResponseMessage } from '../utils/format';

function map(expand) {
  if (expand == null) return null;
  const params = {};
  if (Array.isArray(expand)) params.expand = expand.reduce((p, c) => `${p},${c}`);
  if (typeof expand === 'string' || expand instanceof String) params.expand = expand;
  return params.expand ? params : expand;
}

const request = async (request, action, dispatch, payload, response) => {
  if (!(await validateToken(dispatch)).resolve()) {
    return Promise.reject();
  }
  dispatch({ type: `${action}_REQUEST`, payload });
  return request()
    .then((result) => {
      dispatch({ type: action, payload: response || result.data });
      return Promise.resolve(response || result.data);
    })
    .catch((error) => {
      dispatch({ type: `${action}_ERROR`, payload: getResponseMessage(error), report: true });
      if (error.status === 401) auth.signOut();
      return Promise.reject(error);
    });
};

const rest = (api) => {
  return {
    createGet: (action, method) => expand => dispatch => request(
      () => api[method](map(expand)), action, dispatch, map(expand),
    ),
    createGetBy: (action, method) => (id, expand) => dispatch => request(
      () => api[method](id, map(expand)), action, dispatch, id,
    ),
    createPost: (action, method) => item => dispatch => request(
      () => api[method](item), action, dispatch, item,
    ),
    createPut: (action, method) => (id, item) => dispatch => request(
      () => api[method](id, item), action, dispatch, item,
    ),
    createPutItem: (action, method) => item => dispatch => request(
      () => api[method](item), action, dispatch, item,
    ),
    createDelete: (action, method) => id => dispatch => request(
      () => api[method](id), action, dispatch, id, id,
    ),
  };
};

export default rest;
