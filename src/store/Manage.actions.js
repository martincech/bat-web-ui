import ApiClient from '@veit/bat-cloud-api/lib/ApiClient';

const get = (path) => {
  return ApiClient.instance.callApi(path, 'GET', {}, {}, {}, {}, {}, ['OAuth2'], ['application/json'], ['application/json'], Object);
};

export const updateGroups = () => {
  return get('/manage/flock-groups');
};

export const updateDeviceMap = () => {
  return get('/manage/device-map');
};
