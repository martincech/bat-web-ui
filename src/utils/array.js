
function isArrayWithValue(array) {
  return !(array == null || !Array.isArray(array) || array.length < 1);
}

function range(count, from = 0) {
  return [...Array(count)].map((_, i) => from + i);
}

function indexOf(array, value, map) {
  if (!isArrayWithValue(array) || value == null) return -1;
  const mapArray = map == null ? array : array.map(map);
  return mapArray.indexOf(value);
}

function closest(array, value, map) {
  if (array == null
    || !Array.isArray(array)
    || array.length < 1
    || value == null
    || (map != null && !(map instanceof Function))) return null;

  return array.reduce((prev, curr) => {
    const prevVal = map == null ? prev : map(prev);
    const currVal = map == null ? curr : map(curr);
    return (Math.abs(currVal - value) < Math.abs(prevVal - value) ? curr : prev);
  });
}

function next(array, value, map) {
  const index = indexOf(array, value, map);
  if (index === -1 || array.length <= index + 1) return null;
  return array[index + 1];
}

function prev(array, value, map) {
  const index = indexOf(array, value, map);
  if (index < 1) return null;
  return array[index - 1];
}

function lastOrDefault(array, defaultValue) {
  if (!isArrayWithValue(array)) return defaultValue;
  return array[array.length - 1];
}

function firstOrDefault(array, defaultValue) {
  if (!isArrayWithValue(array)) return defaultValue;
  return array[0];
}

function toggle(array, item, add) {
  if (!isArrayWithValue(array)) return [item];
  const index = array.indexOf(item);
  const newArray = [...array];
  if ((add === true || add == null) && index === -1) {
    newArray.push(item);
  }
  if ((add === false || add == null) && index !== -1) {
    newArray.splice(index, 1);
  }
  return newArray;
}

export default {
  isArrayWithValue,
  range,
  indexOf,
  closest,
  next,
  prev,
  lastOrDefault,
  firstOrDefault,
  toggle,
};
