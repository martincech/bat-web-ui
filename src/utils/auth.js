import { CognitoAuth } from 'amazon-cognito-auth-js';
import ApiClient from '@veit/bat-cloud-api/lib/ApiClient';
import { login, logoutRedirect } from '../store/Auth.actions';
import authData from '../setup/auth.config';

const auth = new CognitoAuth(authData);

function updateApiHeader() {
  ApiClient.instance.authentications.OAuth2.accessToken = auth.isUserSignedIn()
    ? auth.getCachedSession().getAccessToken().getJwtToken() : null;
}

let callback = null;

function resolve(result) {
  if (callback) {
    callback(result);
    callback = null;
  }
}

export const init = (dispatch) => {
  auth.useCodeGrantFlow();
  auth.userhandler = {
    onSuccess: () => {
      updateApiHeader();
      login()(dispatch);
      resolve(true);
    },
    onFailure: () => {
      logoutRedirect()(dispatch);
      resolve(false);
    },
  };
};

export default {
  signIn: () => auth.getSession(),
  signOut: () => auth.signOut(),
  parseTokens: response => auth.parseCognitoWebResponse(response),
  isUserSignedIn: () => auth.isUserSignedIn(),
  isValid: () => auth.getCachedSession().isValid(),
  getCachedSession: () => auth.getCachedSession(),
  refreshSession: () => {
    return new Promise((cb) => {
      callback = cb;
      auth.refreshSession(auth.getCachedSession().getRefreshToken().getToken());
    });
  },
  updateApiHeader,
};
