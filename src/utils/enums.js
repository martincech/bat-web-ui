
function freeze(obj) {
  return Object.freeze ? Object.freeze(obj) : obj;
}

export const deviceType = freeze({
  bat2Cable: 'bat2CableScale',
  bat2Gsm: 'bat2GsmScale',
  temperature: 'temperature',
  humidity: 'humidity',
  carbonDioxide: 'carbonDioxide',
  terminal: 'batCollectorTerminal',
  isWeight: type => type === 'bat2CableScale' || type === 'bat2GsmScale',
});

export const deviceTypeName = freeze({
  [deviceType.bat2Cable]: 'BAT2 Cable',
  [deviceType.bat2Gsm]: 'BAT2 GSM',
  [deviceType.temperature]: 'Temperature',
  [deviceType.humidity]: 'Humidity',
  [deviceType.carbonDioxide]: 'Carb. Diox.',
  [deviceType.terminal]: 'Terminal',
});

export const unitType = freeze({
  g: 'g',
  kg: 'kg',
  lb: 'lb',
  c: '°C',
  ppm: 'ppm',
  percent: '%',
  pieces: 'pcs',
});

export const unitTypeName = freeze({
  [unitType.g]: 'grams',
  [unitType.kg]: 'kilograms',
  [unitType.lb]: 'pounds',
});

export const dateType = freeze({
  day: 'day',
  week: 'week',
  getPlural: type => `${type}s`,
});

export const sexType = freeze({
  irrelevant: 'irrelevant',
  male: 'male',
  female: 'female',
  mixed: 'mixed',
});

export const roleType = freeze({
  undefined: 'undefined',
  companyAdmin: 'companyAdmin',
  veitOperator: 'veitOperator',
  isAdmin: type => type === 'veitOperator',
});

export const roleTypeName = freeze({
  [roleType.undefined]: 'User',
  [roleType.companyAdmin]: 'Administrator',
  [roleType.veitOperator]: 'VEIT Admin',
});

export const helpActionType = freeze({
  none: 'none',
  requestGateway: 'requestGateway',
});

export const helpActionTypeName = freeze({
  [helpActionType.none]: 'None',
  [helpActionType.requestGateway]: 'Request gateway for country',
});
