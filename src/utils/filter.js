import arrayUtils from './array';

function orderBy(stats, prop) {
  return stats.sort((a, b) => (a[prop] - b[prop]));
}

export function weekStats(stats) {
  if (!arrayUtils.isArrayWithValue(stats)) return [];
  const weeks = {};
  orderBy(stats, 'day').forEach((s) => {
    const week = Math.ceil(s.day / 7);
    (weeks[week] || (weeks[week] = [])).push(s);
  });

  let lastStats = null;
  const weekStats = [];

  Object.keys(weeks).forEach((w) => {
    const weekStat = weeks[w];
    const lastDay = arrayUtils.lastOrDefault(weekStat, {});
    weekStats.push({
      ...lastDay,
      count: weekStat.reduce((a, b) => (a + b.count), 0),
      gain: lastStats == null ? null : lastDay.average - lastStats.average,
    });
    lastStats = lastDay;
  });
  return weekStats;
}

export function weekCurve(points) {
  if (!arrayUtils.isArrayWithValue(points)) return [];
  const weeks = {};
  orderBy(points, 'day').forEach((s) => {
    const week = Math.ceil(s.day / 7);
    (weeks[week] || (weeks[week] = [])).push(s);
  });

  const weekPoints = [];
  Object.keys(weeks).forEach((w) => {
    const lastDay = arrayUtils.lastOrDefault(weeks[w], {});
    weekPoints.push({
      ...lastDay,
      week: Math.ceil(lastDay.day / 7),
    });
  });
  return weekPoints;
}
