import React from 'react';
import { toast } from 'react-toastify';
import { ToastSuccess, ToastError } from '../components/ToastMessage';

const options = { hideProgressBar: true, closeButton: false };

export default {
  error: (message, duration = 10000) => toast(
    <ToastError message={message} />, { ...options, autoClose: duration },
  ),
  success: (message, duration = 10000) => toast(
    <ToastSuccess message={message} />, { ...options, autoClose: duration },
  ),
};
