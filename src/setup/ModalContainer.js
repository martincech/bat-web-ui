import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { OPEN_DIALOG } from '../store/Modal.actions';
import { ADD_BIRD, EDIT_BIRD } from '../store/ModalBird.actions';
import { EDIT_COMPANY } from '../store/ModalCompany.actions';
import { ADD_DEVICE, EDIT_DEVICE } from '../store/ModalDevice.actions';
import { ADD_FARM, EDIT_FARM } from '../store/ModalFarm.actions';
import { SEND_FEEDBACK } from '../store/ModalFeedback.actions';
import { SEND_FEATURE_REQUEST } from '../store/ModalFeature.actions';
import { ADD_FLOCK, EDIT_FLOCK } from '../store/ModalFlock.actions';
import { ADD_HOUSE, EDIT_HOUSE } from '../store/ModalHouse.actions';
import { INVITE_USER, EDIT_USER, REGISTER_USER } from '../store/ModalUser.actions';
import { EDIT_PROFILE } from '../store/ModalProfile.actions';
import { ADD_HELP_PAGE, EDIT_HELP_PAGE } from '../store/ModalHelpPage.actions';
import { SEND_GATEWAY_REQUEST } from '../store/ModalGateway.actions';

import Modals from '../modals/index';

const getModal = (modal) => {
  if (modal == null || modal.type !== OPEN_DIALOG) return null;
  switch (modal.target) {
    case ADD_BIRD:
      return Modals.AddBirdModal;
    case EDIT_BIRD:
      return Modals.EditBirdModal;
    case EDIT_COMPANY:
      return Modals.EditComapnyModal;
    case ADD_DEVICE:
      return Modals.AddDeviceModal;
    case EDIT_DEVICE:
      return Modals.EditDeviceModal;
    case ADD_FARM:
      return Modals.AddFarmModal;
    case EDIT_FARM:
      return Modals.EditFarmModal;
    case SEND_FEEDBACK:
      return Modals.FeedbackModal;
    case SEND_FEATURE_REQUEST:
      return Modals.FeatureRequestModal;
    case ADD_FLOCK:
      return Modals.AddFlockModal;
    case EDIT_FLOCK:
      return Modals.EditFlockModal;
    case ADD_HOUSE:
      return Modals.AddHouseModal;
    case EDIT_HOUSE:
      return Modals.EditHouseModal;
    case EDIT_PROFILE:
      return Modals.EditProfileModal;
    case INVITE_USER:
      return Modals.InviteUserModal;
    case EDIT_USER:
      return Modals.EditUserModal;
    case REGISTER_USER:
      return Modals.RegisterUserModal;
    case ADD_HELP_PAGE:
      return Modals.AddHelpPageModal;
    case EDIT_HELP_PAGE:
      return Modals.EditHelpPageModal;
    case SEND_GATEWAY_REQUEST:
      return Modals.GatewayRequestModal;
    default:
      return null;
  }
};

const ModalContainer = ({ modal }) => {
  const ModalWindow = getModal(modal);
  return ModalWindow == null ? null : <ModalWindow />;
};

ModalContainer.propTypes = {
  modal: PropTypes.shape({
    type: PropTypes.string,
    target: PropTypes.string,
  }).isRequired,
};

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

export default connect(mapStateToProps)(ModalContainer);
