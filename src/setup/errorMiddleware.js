import toastMessage from '../utils/toastMessage';

function debounce(func, wait, immediate) {
  let timeout;
  return (...args) => {
    const context = this;
    const later = () => {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

const debounceMessage = debounce((duration) => {
  toastMessage.error(`${'Error during request.'}`, duration);
}, 1000);

const errorRegEx = new RegExp('^(?!FETCH).*_ERROR');
export default (isDevelopment, duration = 15000) => () => next => (action) => {
  if (errorRegEx.test(action.type) && action.report) {
    if (isDevelopment) {
      toastMessage.error(`${action.type} : ${action.payload}`, duration);
    } else {
      debounceMessage(duration);
    }
  }
  return next(action);
};
