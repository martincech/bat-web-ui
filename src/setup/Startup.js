import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';

import PageNotFound from '../components/PageNotFound';
import Loader from '../components/Loader';

import { getCurrentUser } from '../store/Auth.actions';
import { roleType } from '../utils/enums';

class Startup extends Component {
  componentDidMount() {
    const { auth } = this.props;
    if (auth.isAuthenticated && auth.user == null) {
      this.props.getCurrentUser();
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.auth.isAuthenticated !== this.props.auth.isAuthenticated) {
      this.componentDidMount();
    }
  }

  hasRole = ({ user }) => {
    if (this.props.roles.length === 0) return true;
    return this.props.roles.indexOf(user.role) !== -1;
  }

  render() {
    const { auth } = this.props;
    if (auth.isAuthenticated && auth.user == null) {
      return <Loader />;
    }

    if (auth.user != null
      && roleType.isAdmin(auth.user.role)
      && auth.user.companyId == null
      && this.props.pathname.toLowerCase() !== '/companies') {
      return <Redirect to="/companies" />;
    }

    return this.hasRole(auth) ? this.props.children : <PageNotFound />;
  }
}

const mapStateToProps = (state) => {
  return {
    pathname: state.router.location.pathname,
    auth: state.auth,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  getCurrentUser,
}, dispatch);

Startup.propTypes = {
  getCurrentUser: PropTypes.func.isRequired,
  auth: PropTypes.shape({
    user: PropTypes.object,
    isAuthenticated: PropTypes.bool,
  }).isRequired,
  children: PropTypes.node,
  roles: PropTypes.arrayOf(PropTypes.string),
  pathname: PropTypes.string.isRequired,
};

Startup.defaultProps = {
  children: null,
  roles: [],
};

export default connect(mapStateToProps, mapDispatchToProps)(Startup);
