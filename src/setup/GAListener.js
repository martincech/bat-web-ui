import React from 'react';
import ReactGA from 'react-ga';
import PropTypes from 'prop-types';

ReactGA.initialize(process.env.REACT_APP_GA);

class GAListener extends React.Component {
  static contextTypes = {
    router: PropTypes.shape({
      history: PropTypes.object,
    }),
  };

  componentDidMount() {
    this.sendPageView(this.context.router.history.location);
    this.context.router.history.listen(this.sendPageView);
  }

  sendPageView = (location) => {
    ReactGA.set({ page: location.pathname });
    ReactGA.pageview(location.pathname);
  }

  render() {
    return this.props.children;
  }
}

GAListener.propTypes = {
  children: PropTypes.node.isRequired,
};

export default GAListener;
