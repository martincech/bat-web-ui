import { fetchAll } from '../../store/Fetch.actions';
import { getBird, getPublicBird, expandBird } from '../../store/Bird.actions';

const fetchData = privateBird => (dispatch) => {
  const fn = privateBird ? getBird : getPublicBird;
  fetchAll(() => [
    fn([expandBird.duration, expandBird.scope, expandBird.sex, expandBird.dateType])(dispatch),
  ])(dispatch);
};

export default fetchData;
