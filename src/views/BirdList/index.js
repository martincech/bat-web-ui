import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  Table, Label, AddButton, ButtonSwitch,
} from '@veit/veit-web-controls';
import { IMSettings } from '@veit/veit-web-controls/dist/icons';

import { openDialogAddBird, openDialogEditBird } from '../../store/ModalBird.actions';
import goTo from '../../store/Router.actions';
import fetchData from './fetchData';

import Page from '../../components/Page';
import FeatureRequest from '../../components/FeatureRequest';
import bem from '../../utils/bem';
import { sortString } from '../../utils/sort';
import Duration from '../../components/Duration';

class BirdList extends Component {
  state = {
    private: true,
  }

  componentDidMount() {
    this.props.fetchData(true);
  }

  action = (v) => {
    this.props.fetchData(v);
    this.setState({ private: v });
  }

  actions = () => {
    return (
      <React.Fragment>
        <ButtonSwitch
          options={[{ title: 'Private', value: true }, { title: 'Public', value: false }]}
          selected={this.state.private}
          onChange={this.action}
        />
        <FeatureRequest feature="Add Bird">
          <AddButton>Add Bird</AddButton>
        </FeatureRequest>
      </React.Fragment>
    );
  }

  editBirdHandler(e, bird) {
    e.preventDefault();
    e.stopPropagation();
    this.props.openDialogEditBird(bird);
  }

  render() {
    const bm = bem.view('bird-list');
    const birds = this.state.private ? this.props.bird.items : this.props.bird.public;
    return (
      <Page
        className={bm.b()}
        title="Manage birds"
        actions={this.actions()}
        isFetching={this.props.fetch.isFetching}
        emptyPage={{
          isEmpty: birds.length === 0,
          icon: IMSettings,
          text: 'Nothing here.',
        }}
      >
        <Table type="info">
          <thead>
            <tr>
              <th>Bird</th>
              <th>Product</th>
              <th>Company</th>
              <th>Duration</th>
              {/* <th>Active</th> */}
              {/* <th></th> */}
            </tr>
          </thead>
          <tbody>
            {sortString(birds, s => s.name).map(d => (
              <tr className="has-pointer" onClick={() => this.props.goTo(`/bird/${d.id}`)} key={d.id}>
                <td><Label>{d.name}</Label></td>
                <td><Label>{d.product}</Label></td>
                <td><Label>{d.company}</Label></td>
                <td>
                  {d.duration != null && (
                    <Label>
                      <Duration value={d.duration} unit={d.dateType} />
                    </Label>
                  )}
                </td>
                {/* <td><StatusIcon /></td> */}
                {/* <td className="text-center">
                  <Button
                    onClick={e => this.editBirdHandler(e, d)}
                    className="btn-narrow"
                    color="primary"
                  >
                    Edit Bird
                  </Button>
                </td> */}
              </tr>
            ))}
          </tbody>
        </Table>
      </Page>
    );
  }
}

BirdList.propTypes = {
  fetchData: PropTypes.func.isRequired,
  openDialogAddBird: PropTypes.func.isRequired,
  openDialogEditBird: PropTypes.func.isRequired,
  goTo: PropTypes.func.isRequired,
  bird: PropTypes.shape({
    items: PropTypes.array,
    public: PropTypes.array,
  }),
  fetch: PropTypes.shape({
    isFetching: PropTypes.bool,
  }).isRequired,
};

BirdList.defaultProps = {
  bird: null,
};

const mapStateToProps = (state) => {
  return {
    bird: state.bird,
    fetch: state.fetch,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchData,
  openDialogAddBird,
  openDialogEditBird,
  goTo,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(BirdList);
