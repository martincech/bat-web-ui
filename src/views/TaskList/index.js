import React, { Component } from 'react';

import bem from '../../utils/bem';
import Page from '../../components/Page';

class TaskList extends Component {
  render() {
    const bm = bem.view('task-list');
    return (
      <Page className={bm.b()} title="My Tasks">
      </Page>
    );
  }
}

export default TaskList;
