import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loginRedirect } from '../../store/Auth.actions';
import { Redirect } from 'react-router-dom';


class Login extends Component {
  login = () => {
    this.props.loginRedirect();
    return 'Redirecting';
  }

  render() {
    return (
      <React.Fragment>
        {
          this.props.isAuthenticated
            ? <Redirect to="/flocks" />
            : this.login()
        }
      </React.Fragment>
    );
  }
}

Login.propTypes = {
  loginRedirect: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
};

Login.defaultProps = {
  isAuthenticated: false,
};

const mapDispatchToProps = dispatch => bindActionCreators({
  loginRedirect,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
