import { fetchAll } from '../../store/Fetch.actions';
import { getFlock, expandFlock } from '../../store/Flock.actions';
import { getFarm, expandFarm } from '../../store/Farm.actions';
import { getHouse } from '../../store/House.actions';
import { getBird } from '../../store/Bird.actions';

const fetchData = () => (dispatch) => {
  fetchAll(() => [
    getFlock([
      expandFlock.lastStatistics, expandFlock.endDate, expandFlock.house, expandFlock.device,
    ])(dispatch),
    getBird()(dispatch),
    getFarm([expandFarm.address, expandFarm.country])(dispatch),
    getHouse()(dispatch),
  ])(dispatch);
};

export default fetchData;
