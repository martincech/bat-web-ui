import React from 'react';
import PropTypes from 'prop-types';
import { Label } from '@veit/veit-web-controls';

const StatValue = ({ stats, property, sign }) => {
  if (stats == null || property == null) return null;
  const value = stats[stats.length - 1][property];
  return (
    <Label>
      {sign && value > 0 ? '+' : null}
      {value}
    </Label>
  );
};

StatValue.propTypes = {
  property: PropTypes.string.isRequired,
  stats: PropTypes.arrayOf(PropTypes.object),
  sign: PropTypes.bool,
};

StatValue.defaultProps = {
  stats: null,
  sign: false,
};

export default StatValue;
