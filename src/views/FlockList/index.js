import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import {
  Table, Button, AddButton,
} from '@veit/veit-web-controls';
import { IMBird } from '@veit/veit-web-controls/dist/icons';

import { openDialogAddFlock } from '../../store/ModalFlock.actions';
import goTo from '../../store/Router.actions';

import Page from '../../components/Page';
import bem from '../../utils/bem';

import Farm from '../_partials/Farm';
import fetchData from './fetchData';
import TableRow from './TableRow';

class FlockList extends Component {
  state = {
    seeAllFlocks: true,
  }

  componentDidMount() {
    this.props.fetchData();
  }

  getBird(birdId) {
    return this.props.birds.find(f => f.id === birdId);
  }

  getFlocks() {
    const flocks = this.props.flock.items;
    const today = new Date();
    switch (this.props.match.params.filter) {
      case 'finished':
        return flocks.filter(f => f.endDate);
      case 'scheduled':
        return flocks.filter(f => new Date(f.startDate) > today);
      case 'all':
        return flocks;
      default:
        return flocks.filter(f => !f.endDate && new Date(f.startDate) < today);
    }
  }
  addFlockDialog = () => {
    this.props.openDialogAddFlock();
  }

  seeAllFlocks = (e) => {
    e.preventDefault();
    e.stopPropagation();
    this.setState(prev => ({ seeAllFlocks: !prev.seeAllFlocks }));
  }

  getFarmId = (houseId) => {
    if (houseId == null) return null;
    const house = this.props.house.items.find(f => f.id === houseId);
    return house == null ? null : house.farmId;
  }

  actions() {
    return (
      <React.Fragment>
        <AddButton onClick={this.addFlockDialog}>Add Flock</AddButton>
        <Button tag={Link} to="/farms" color="primary" outline>Manage Farms</Button>
      </React.Fragment>
    );
  }

  render() {
    const bm = bem.view('flock-list');
    return (
      <Page
        className={bm.b()}
        title="My Flocks"
        actions={this.actions()}
        isFetching={this.props.fetch.isFetching}
        emptyPage={{
          isEmpty: this.props.flock.items.length === 0,
          icon: IMBird,
          action: <AddButton onClick={this.addFlockDialog}>Add Flock</AddButton>,
        }}
      >
        <Table type="info">
          <thead>
            <tr>
              <th>Batch (type)</th>
              <th className="text-center">Update</th>
              <th className="text-center">Growth</th>
              <th>Progress</th>
              <th>Sex</th>
              <th>Gain (g)</th>
              <th>Weight (g)</th>
              <th>Count (pcs)</th>
              <th>Unifor. (%)</th>
              <th>Farm</th>
              {/* <th style={{ width: '105px', whiteSpace: 'nowrap' }}>
                    <TextButton onClick={this.seeAllFlocks} active>
                      {this.state.seeAllFlocks ? 'Hide flocks' : 'See all flocks'}
                    </TextButton>
                  </th> */}
            </tr>
          </thead>
          <tbody>
            {
              this.getFlocks().map(i => (
                <TableRow
                  key={i.id}
                  flock={i}
                  seeAllFlocksHandler={this.seeAllFlocks}
                  seeAllFlocks={this.state.seeAllFlocks}
                  farm={<Farm houseId={i.houseId || (i.device || {}).houseId} />}
                  goTo={this.props.goTo}
                  bird={this.getBird(i.birdId)}
                />
              ))}
          </tbody>
        </Table>
      </Page>
    );
  }
}

FlockList.propTypes = {
  fetchData: PropTypes.func.isRequired,
  openDialogAddFlock: PropTypes.func.isRequired,
  goTo: PropTypes.func.isRequired,
  fetch: PropTypes.shape({
    isFetching: PropTypes.bool,
  }).isRequired,
  flock: PropTypes.shape({
    items: PropTypes.array,
  }).isRequired,
  farm: PropTypes.shape({
    items: PropTypes.array,
  }).isRequired,
  house: PropTypes.shape({
    items: PropTypes.array,
  }).isRequired,
  birds: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
  })).isRequired,
  match: PropTypes.shape({
    params: PropTypes.object,
  }).isRequired,
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchData,
  openDialogAddFlock,
  goTo,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    flock: state.flock,
    farm: state.farm,
    house: state.house,
    fetch: state.fetch,
    birds: state.bird.items,
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(FlockList);
