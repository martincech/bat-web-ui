import React from 'react';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import { Label } from '@veit/veit-web-controls';
import DateFormat from '../../components/DateFormat';

const Progress = ({ flock }) => {
  const { endDate, startDate } = flock;
  const isScheduled = dayjs(startDate).toDate() > new Date();
  return endDate != null
    ? (
      <React.Fragment>
        <Label>FINISHED</Label>
        <Label type="info"><DateFormat date={endDate} /></Label>
      </React.Fragment>
    ) : (
      <React.Fragment>
        {
          isScheduled
            ? (
              <React.Fragment>
                <Label>SCHEDULED</Label>
                <Label type="info"><DateFormat date={startDate} /></Label>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <Label>
                  <DateFormat date={startDate} diff="weeks" />
                  {' '}
                  week
                </Label>
                <Label type="info">Growth</Label>
              </React.Fragment>
            )
        }
      </React.Fragment>
    );
};

Progress.propTypes = {
  flock: PropTypes.shape({
    endDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
    startDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
  }).isRequired,
};

export default Progress;
