import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Label, TextButton } from '@veit/veit-web-controls';
import { IMNext } from '@veit/veit-web-controls/dist/icons';

import Panel from '../../components/Panel';

const Reminder = ({
  farm, opened, today, toggleReminder,
}) => {
  const reminders = (farm.reminders || []).filter(f => f.today === today);
  return (
    <div key={farm.id} className={opened ? 'active' : null}>
      <Label className="reminders-item" onClick={() => toggleReminder(farm.id)}>
        <IMNext />
        {farm.name}
        <span className={reminders.length === 0 ? 'disabled' : null}>{reminders.length}</span>
      </Label>
      <div className="reminders-content">
        {reminders.map(i => <Label className="reminders-subitem" key={i.id}>Check LSL Parent Female #3</Label>)}
      </div>
    </div>
  );
};

Reminder.propTypes = {
  farm: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
  opened: PropTypes.bool.isRequired,
  today: PropTypes.bool.isRequired,
  toggleReminder: PropTypes.func.isRequired,
};


class Reminders extends Component {
  state = {
    todayReminders: true,
    openReminders: {},
  }

  showReminders = (todayReminders) => {
    this.setState({ todayReminders });
  }

  toggleReminder = (id) => {
    this.setState((prev) => {
      const reminder = {};
      reminder[id] = !prev.openReminders[id];
      return {
        openReminders: {
          ...prev.openReminders,
          ...reminder,
        },
      };
    });
  }

  render() {
    const { farms } = this.props;
    return (
      <React.Fragment>
        <div>
          <Label tag="span">REMINDERS</Label>
          <div style={{ float: 'right', fontSize: '14px' }}>
            <TextButton
              active={this.state.todayReminders}
              onClick={() => this.showReminders(true)}
            >
              Today
            </TextButton>
            &nbsp;&nbsp;
            <TextButton
              active={!this.state.todayReminders}
              onClick={() => this.showReminders(false)}
            >
              Tomorow
            </TextButton>
          </div>
        </div>
        <Panel className="reminders">
          {
            farms.map(r => (
              <Reminder
                key={r.id}
                farm={r}
                today={this.state.todayReminders}
                toggleReminder={this.toggleReminder}
                opened={this.state.openReminders[r.id] === true}
              />
            ))
          }
        </Panel>
      </React.Fragment>
    );
  }
}

Reminders.propTypes = {
  farms: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
  })).isRequired,
};

export default Reminders;
