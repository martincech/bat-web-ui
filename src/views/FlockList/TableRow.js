import React from 'react';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import Progress from './Progress';
import { Label } from '@veit/veit-web-controls';

import GrowthStatus from '../_partials/GrowthStatus';
import UpdateStatus from '../_partials/UpdateStatus';

import SexIcon from '../../components/SexIcon';
import Round from '../../components/Round';

const TableRow = ({
  flock, goTo, bird, farm,
}) => {
  const stats = (flock.lastStatistics || []).filter(f => f != null);
  const closed = flock.endDate != null || dayjs(flock.startDate).isAfter(dayjs());
  return (
    <tr className="has-pointer" onClick={() => goTo(`/flock/${flock.id}`)}>
      <td>
        <Label>{flock.name}</Label>
        <Label type="info">{bird && bird.name}</Label>
      </td>
      <td className="text-center">
        {!closed && <UpdateStatus lastStatistics={flock.lastStatistics} />}
      </td>
      <td className="text-center">
        {!closed && <GrowthStatus lastStatistics={flock.lastStatistics} />}
      </td>
      <td><Progress flock={flock} /></td>
      <td className="text-center">
        {stats.map(s => <Label key={s.sex}><SexIcon sex={s.sex} /></Label>)}
      </td>
      <td>
        {stats.map(s => <Label key={s.sex}>{s.gain}</Label>)}
      </td>
      <td>
        {stats.map(s => <Label key={s.sex}>{s.average}</Label>)}
      </td>
      <td>
        {stats.map(s => <Label key={s.sex}>{s.count}</Label>)}
      </td>
      <td>
        {stats.map(s => (
          <Label key={s.sex}>
            <Round value={s.uniformity} />
          </Label>
        ))}
      </td>
      <td>{farm}</td>
    </tr>
  );
};

TableRow.propTypes = {
  flock: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
  bird: PropTypes.shape({
    name: PropTypes.string,
  }),
  farm: PropTypes.node,
  seeAllFlocks: PropTypes.bool.isRequired,
  seeAllFlocksHandler: PropTypes.func.isRequired,
  goTo: PropTypes.func.isRequired,
};

TableRow.defaultProps = {
  farm: null,
  bird: null,
};

export default TableRow;
