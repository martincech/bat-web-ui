import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  Table, Button, Input, AddButton, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem,
} from '@veit/veit-web-controls';

import DeleteModal from '../../modals/DeleteModal';
import bem from '../../utils/bem';
import goTo from '../../store/Router.actions';
import { getCompany, deleteCompany, postCompany } from '../../store/Company.actions';
import { putUser } from '../../store/User.actions';
import { getCurrentUser } from '../../store/Auth.actions';
import { updateGroups, updateDeviceMap } from '../../store/Manage.actions';

import toastMessage from '../../utils/toastMessage';

class CompanyList extends Component {
  state = {
    companyName: '',
  }

  componentDidMount() {
    this.props.getCompany();
  }

  connect = (companyId) => {
    const { user } = this.props.auth;
    this.props.putUser(user.id, { ...user, companyId }).then(() => {
      this.props.getCurrentUser(user.id).then(() => {
        if (companyId != null) this.props.goTo('/flocks');
      });
    });
  }

  create = () => {
    this.props.postCompany({ name: this.state.companyName }).then(() => this.setState({ companyName: '' }));
  }

  delete = (id) => {
    this.props.deleteCompany(id);
  }

  updateDeviceGroups = () => {
    updateGroups().then((result) => {
      // eslint-disable-next-line no-console
      console.log(result.data.data);
      toastMessage.success('Device groups updated.');
    });
  }

  updateDeviceMaping = () => {
    updateDeviceMap().then((result) => {
      // eslint-disable-next-line no-console
      console.log(result.data.data);
      toastMessage.success('Device groups updated.');
    });
  }

  render() {
    const bm = bem.view('company-list');
    const { user } = this.props.auth;
    return (
      <div className={bm.b()}>
        <Table>
          <thead>
            <tr>
              <th>
                <Button
                  disabled={user.companyId == null}
                  className="btn-narrow"
                  color="primary"
                  onClick={() => this.props.goTo('/flocks')}
                >
                  Go to cloud
                </Button>
              </th>
              <th>Database</th>
              <th></th>
              <th style={{ width: '1%' }}>
                <UncontrolledDropdown>
                  <DropdownToggle className="sub-actions" outline color="primary">
                    ---
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem className="btn-narrow" color="primary" onClick={this.updateDeviceMaping}>Update Device Mapping</DropdownItem>
                    <DropdownItem className="btn-narrow" color="primary" onClick={this.updateDeviceGroups}>Update Groups</DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.props.company.items.map(c => (
              <tr key={c.id}>
                <td>{c.name}</td>
                <td>{c.id}</td>
                <td>
                  {
                    user.companyId === c.id
                      ? (
                        <Button className="btn-narrow" color="primary" onClick={() => this.connect()}>Disconnect</Button>
                      ) : (
                        <Button className="btn-narrow" color="primary" onClick={() => this.connect(c.id)}>Connect</Button>
                      )
                  }
                </td>
                <td><DeleteModal advaced narrow name="Company" confirm={() => this.delete(c.id)} /></td>
              </tr>
            ))}
            <tr>
              <td>New Company </td>
              <td colSpan="2">
                <Input
                  onChange={e => this.setState({ companyName: e.target.value })}
                  value={this.state.companyName}
                />
              </td>
              <td>
                <AddButton disabled={this.state.companyName.length < 2} onClick={this.create}>
                  Create
                </AddButton>
              </td>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }
}

CompanyList.propTypes = {
  getCompany: PropTypes.func.isRequired,
  postCompany: PropTypes.func.isRequired,
  deleteCompany: PropTypes.func.isRequired,
  goTo: PropTypes.func.isRequired,
  putUser: PropTypes.func.isRequired,
  getCurrentUser: PropTypes.func.isRequired,
  auth: PropTypes.shape({
    isAuthenticated: PropTypes.bool,
    user: PropTypes.object,
  }).isRequired,
  company: PropTypes.shape({
    items: PropTypes.array,
  }).isRequired,
};

const mapStateToProps = (state) => {
  return {
    company: state.company,
    auth: state.auth,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  getCompany,
  postCompany,
  deleteCompany,
  putUser,
  getCurrentUser,
  goTo,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CompanyList);
