import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  Table, Label, Button, AddButton,
} from '@veit/veit-web-controls';
import { IMSettings } from '@veit/veit-web-controls/dist/icons';

import { openDialogAddHouse, openDialogEditHouse } from '../../store/ModalHouse.actions';

import Page from '../../components/Page';
import bem from '../../utils/bem';
import { sortString } from '../../utils/sort';
import fetchData from './fetchData';
import Farm from '../_partials/Farm';

class HouseList extends Component {
  componentDidMount() {
    this.props.fetchData();
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.isFetching !== this.props.isFetching
      || nextProps.houses !== this.props.houses;
  }

  addHouseHandler = () => {
    this.props.openDialogAddHouse();
  }

  editHouseHandler = (house) => {
    this.props.openDialogEditHouse(house);
  }

  getFarmName = (house) => {
    return (this.props.farms.find(f => f.id === house.farmId) || {}).name;
  }

  actions() {
    return (
      <React.Fragment>
        <AddButton onClick={this.addHouseHandler}>
          Add House
        </AddButton>
        <Button to="/farms" tag={Link} color="primary" outline>Manage farms</Button>
      </React.Fragment>
    );
  }

  render() {
    const bm = bem.view('house-list');
    const { houses } = this.props;
    return (
      <Page
        className={bm.b()}
        title="Manage houses"
        actions={this.actions()}
        isFetching={this.props.isFetching}
        emptyPage={{
          isEmpty: houses.length === 0,
          icon: IMSettings,
          action: <AddButton onClick={this.addHouseHandler}>Add House</AddButton>,
        }}
      >
        <Table type="data">
          <thead>
            <tr>
              <th>Farm</th>
              <th>House</th>
              <th>BAT Scales</th>
              {/* <th>Other sensors</th> */}
              <th></th>
            </tr>
          </thead>
          <tbody>
            {sortString(houses, s => this.getFarmName(s)).map(d => (
              <tr key={d.id}>
                <td>
                  <Farm farmId={d.farmId} />
                </td>
                <td><Label>{d.name}</Label></td>
                <td><Label>{d.scalesCount}</Label></td>
                {/* <td><Label>{this.deviceCount(d.id, false)}</Label></td> */}
                <td className="text-center">
                  <Button className="btn-narrow" onClick={() => this.editHouseHandler(d)} color="primary">
                    Edit House
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Page>
    );
  }
}

HouseList.propTypes = {
  fetchData: PropTypes.func.isRequired,
  openDialogAddHouse: PropTypes.func.isRequired,
  openDialogEditHouse: PropTypes.func.isRequired,
  houses: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    farmId: PropTypes.string,
  })),
  farms: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    country: PropTypes.string,
    address: PropTypes.string,
  })),
  devices: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    unit: PropTypes.string,
  })),
  isFetching: PropTypes.bool.isRequired,
};

HouseList.defaultProps = {
  houses: [],
  farms: [],
  devices: [],
};

const mapStateToProps = (state) => {
  return {
    houses: state.house.items,
    farms: state.farm.items,
    devices: state.device.items,
    isFetching: state.fetch.isFetching,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchData,
  openDialogAddHouse,
  openDialogEditHouse,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HouseList);
