import { fetchAll } from '../../store/Fetch.actions';
import { getFarm, expandFarm } from '../../store/Farm.actions';
import { getHouse, expandHouse } from '../../store/House.actions';

const fetchData = () => (dispatch) => {
  fetchAll(() => [
    getFarm([expandFarm.address, expandFarm.country])(dispatch),
    getHouse(expandHouse.scalesCount)(dispatch),
  ])(dispatch);
};

export default fetchData;
