import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import {
  ButtonSwitch, Input, InputGroup, UncontrolledDropdown, DropdownToggle,
  DropdownMenu, DropdownItem, RangePopover,
} from '@veit/veit-web-controls';

import { openDialogEditFlock } from '../../store/ModalFlock.actions';
import goTo from '../../store/Router.actions';

import Actions from './Actions';
import Subtitle from './Subtitle';
import Navigation from './Navigation';
import Page from '../../components/Page';
import Panel from '../../components/Panel';
import bem from '../../utils/bem';
import arrayUtils from '../../utils/array';
import { weekStats, weekCurve } from '../../utils/filter';

import Chart from './Chart';
import StatsTable from './StatsTable';

import { IMBird } from '@veit/veit-web-controls/dist/icons';

import fetchData from './fetchData';

class Flock extends Component {
  state = {
    showGraph: false,
    curves: ['average'],
    range: {
      min: 1,
      max: 28,
      maxLimit: 999,
      unit: 'day',
      lock: null,
    },
    popoverOpen: false,
  }

  componentDidMount() {
    this.props.fetchData(this.props.id, true);
  }

  componentDidUpdate(prevProps) {
    if (prevProps == null || this.props.id !== prevProps.id) {
      this.props.fetchData(this.props.id, false);
      this.setState({ popoverOpen: false });
    }
    if (this.props.stats.item != null && this.props.stats.item !== prevProps.stats.item) {
      this.setInitRange();
    }
  }

  getRange = (limit, range) => {
    const { minLock, maxLock } = range;
    if (maxLock == null || minLock == null) return limit;
    const max = maxLock > limit.max ? limit.max : maxLock;
    const min = minLock > max ? max : minLock;
    return {
      min,
      max,
    };
  }

  setInitRange = (unit) => {
    const last = (this.props.stats.item || [])
      .reduce((p, c) => (p.day > c.day ? p : c), { day: 0 });
    if (last != null) {
      this.setState((prevState) => {
        const u = unit || prevState.range.unit;
        const d = u === 'day' ? last.day : Math.ceil(last.day / 7);
        const limit = {
          min: 1,
          max: d > 1 ? d : 2,
        };
        const range = this.getRange(limit, prevState.range);
        return {
          range: {
            ...prevState.range,
            ...range,
            maxLimit: range.max,
            unit: u,
          },
        };
      });
    }
  }

  setRangeUnit = (unit) => {
    this.setInitRange(unit);
  }

  setRange = (range) => {
    const newState = {
      range,
    };
    this.setState(newState);
  }

  editFlock = () => {
    this.props.openDialogEditFlock(this.props.flock.item);
  }

  showCurve = (curve, show) => {
    this.setState((prevState) => {
      const curves = arrayUtils.toggle(prevState.curves, curve, show);
      return { curves };
    });
  }

  toggle = (prop) => {
    this.setState(prevState => ({
      [prop]: !prevState[prop],
    }));
  }

  nextFlock = () => {
    if (this.props.flock.item == null) return null;
    return arrayUtils.next(this.props.flock.items, this.props.flock.item.id, f => f.id);
  }

  prevFlock = () => {
    if (this.props.flock.item == null) return null;
    return arrayUtils.prev(this.props.flock.items, this.props.flock.item.id, f => f.id);
  }

  applyFilter(stats, ascending) {
    const useWeek = this.state.range.unit === 'week';

    const filtered = (useWeek ? weekStats(stats) : stats)
      .filter(
        (f) => {
          const { min, max } = this.state.range;
          const d = useWeek ? Math.ceil(f.day / 7) : f.day;
          return d >= min && d <= max;
        },
      );
    return filtered.sort((a, b) => (ascending ? a.day - b.day : b.day - a.day));
  }

  applyFilterCurve(bird, flock) {
    if (bird == null || bird.curvePoints == null || flock.birdId !== bird.id) return null;
    return this.state.range.unit === 'week' ? weekCurve(bird.curvePoints) : bird.curvePoints;
  }

  render() {
    const bm = bem.view('flock');
    const flock = this.props.flock.item || {};
    const stats = this.props.stats.item || {};
    const range = this.state.range.lock || this.state.range;
    const data = (Array.isArray(stats) ? stats : stats.data) || [];
    const nextFlock = this.nextFlock();
    const prevFlock = this.prevFlock();
    return (
      <Page
        className={bm.b()}
        title={flock.name}
        subtitle={
          <Subtitle dateType={this.state.range.unit} flock={flock} bird={this.props.bird.item} />
        }
        actions={<Actions editFlock={this.editFlock} />}
        isFetching={this.props.fetch.isFetching}
        header={<Navigation goTo={this.props.goTo} className={bm.e('navigation')} next={nextFlock} prev={prevFlock} />}
        emptyPage={{
          isEmpty: data.length === 0,
          icon: IMBird,
          text: 'Flock does not have any statistic data.',
        }}
        notFound={flock.id !== this.props.id}
      >
        {
          this.state.showGraph
            ? (
              <Panel className={bm.e('graph')}>
                <Chart
                  stats={this.applyFilter(data, true)}
                  props={this.state.curves}
                  showCurve={this.showCurve}
                  curvePoints={this.applyFilterCurve(this.props.bird.item, flock)}
                  useWeeks={this.state.range.unit === 'week'}
                />
              </Panel>
            ) : (
              <StatsTable useWeeks={this.state.range.unit === 'week'} stats={this.applyFilter(data)} />
            )
        }
        <div className={bm.e('footer')}>
          <ButtonSwitch
            options={[{ title: 'Data points', value: false }, { title: 'Data graphs', value: true }]}
            selected={this.state.showGraph}
            onChange={() => this.toggle('showGraph')}
          />
          <div style={{ flex: 1 }}></div>
          <InputGroup style={{ width: 'unset' }}>
            <RangePopover onChange={this.setRange} range={this.state.range}>
              <Input
                value={`${range.min}-${range.max}. ${this.state.range.unit}`}
                readOnly
              />
            </RangePopover>
            <UncontrolledDropdown addonType="append">
              <DropdownToggle caret color="primary" outline className="form-control">
                {this.state.range.unit === 'week' ? 'Weekly' : 'Daily'}
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem onClick={() => this.setRangeUnit('day')}>Daily</DropdownItem>
                <DropdownItem onClick={() => this.setRangeUnit('week')}>Weekly</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </InputGroup>
          {/*
           <Button style={{ marginLeft: '15px' }} color="primary" outline>Export data</Button>
           */}
        </div>
      </Page>
    );
  }
}

Flock.propTypes = {
  id: PropTypes.string.isRequired,
  fetchData: PropTypes.func.isRequired,
  openDialogEditFlock: PropTypes.func.isRequired,
  goTo: PropTypes.func.isRequired,
  flock: PropTypes.shape({
    item: PropTypes.shape({
      id: PropTypes.string,
    }),
    items: PropTypes.array,

  }),
  stats: PropTypes.shape({
    item: PropTypes.oneOfType([
      PropTypes.shape({
        id: PropTypes.string,
      }),
      PropTypes.array,
    ]),
  }),
  bird: PropTypes.shape({
    item: PropTypes.shape({
      id: PropTypes.string,
      curvePoints: PropTypes.array,
    }),
  }),
  fetch: PropTypes.shape({
    isFetching: PropTypes.bool,
  }),
};

Flock.defaultProps = {
  flock: null,
  stats: null,
  bird: null,
  fetch: null,
};

const mapStateToProps = (state, ownProps) => {
  return {
    id: ownProps.match.params.id,
    flock: state.flock,
    stats: state.stats,
    bird: state.bird,
    fetch: state.fetch,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchData,
  openDialogEditFlock,
  goTo,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Flock);
