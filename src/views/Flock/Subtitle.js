import React from 'react';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import isFloat from 'validator/lib/isFloat';
import SubtitleItem from '../../components/SubtitleItem';
import SexIcon from '../../components/SexIcon';

import SubtitleWrapper from '../../components/SubtitleWrapper';
import DateFormat from '../../components/DateFormat';
import { sexType } from '../../utils/enums';

function isInvalidNumber(number) {
  return number == null || !isFloat(`${number}`);
}

const Target = ({ dateType, targetAge, targetWeight }) => {
  if (isInvalidNumber(targetAge) && isInvalidNumber(targetWeight)) return null;
  return targetWeight
    ? (
      <SubtitleItem title="Target weight :">
        {`${targetWeight} g`}
      </SubtitleItem>
    ) : (
      <SubtitleItem title="Target age :">
        {`${dateType === 'week' ? Math.ceil(targetAge / 7) : targetAge} ${dateType}`}
      </SubtitleItem>
    );
};

Target.propTypes = {
  dateType: PropTypes.string,
  targetAge: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  targetWeight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

Target.defaultProps = {
  dateType: 'day',
  targetAge: null,
  targetWeight: null,
};

const Subtitle = ({
  flock, bird, dateType, ...props
}) => {
  const useWeeks = dateType === 'week';
  const initialAge = useWeeks ? (flock.initialAge / 7) : flock.initialAge;
  const currentAge = Math.ceil(dayjs().diff(flock.startDate, dateType, true) + initialAge);
  const remainingDays = flock.targetAge - flock.initialAge - dayjs().diff(flock.startDate, 'day');
  const finalDate = dayjs().add(remainingDays, 'day').toDate();
  const isScheduled = dayjs(flock.startDate).toDate() > new Date();

  return (
    <SubtitleWrapper {...props}>
      {
        bird && bird.sex != null && bird.sex !== sexType.irrelevant && flock.birdId === bird.id && (
          <React.Fragment>
            <SubtitleItem title="Sex :"><SexIcon sex={bird.sex} style={{ color: 'var(--green)', fontSize: '21px' }} /></SubtitleItem>
            <SubtitleItem title="Bird :">{bird.name}</SubtitleItem>
          </React.Fragment>
        )
      }
      {
        flock && flock.farm && (
          <SubtitleItem title="Farm :">{`${flock.farm.name}, ${flock.farm.country}`}</SubtitleItem>)
      }
      {
        flock.endDate != null
          ? (
            <SubtitleItem title="Finished :">
              <DateFormat date={flock.endDate} />
            </SubtitleItem>
          ) : (
            <React.Fragment>
              {
                (isScheduled)
                  ? (
                    <SubtitleItem title="Scheduled :">
                      <DateFormat date={flock.startDate} />
                    </SubtitleItem>
                  ) : (
                    <SubtitleItem title="Current age :">
                      {`${currentAge} ${dateType}s`}
                    </SubtitleItem>
                  )
              }
              <Target
                dateType={dateType}
                targetAge={flock.targetAge}
                targetWeight={flock.targetWeight}
              />
              {
                isInvalidNumber(flock.targetAge) ? null : (
                  <SubtitleItem title=" Final age on">
                    <DateFormat date={finalDate} />
                  </SubtitleItem>
                )
              }

            </React.Fragment>
          )
      }
    </SubtitleWrapper>
  );
};

Subtitle.propTypes = {
  flock: PropTypes.shape({
    initialAge: PropTypes.number,
  }).isRequired,
  bird: PropTypes.shape({
    name: PropTypes.string,
  }),
  dateType: PropTypes.string,
};

Subtitle.defaultProps = {
  bird: null,
  dateType: null,
};

export default Subtitle;
