import React from 'react';
import { Line } from 'react-chartjs-2';
import PropTypes from 'prop-types';
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  AddButton,
} from '@veit/veit-web-controls';
import { unitType } from '../../utils/enums';
import array from '../../utils/array';
import Round, { roundTo } from '../../components/Round';

function fillMissingDays(points, min, max) {
  if (
    points == null
    || min == null
    || max == null
    || min === max
    || min > max
  ) return [];

  const dict = {};
  points.forEach((point) => { dict[point.day] = point; });
  return array
    .range(max - min + 1, min)
    .map((day) => { return dict[day] != null ? dict[day] : { day }; });
}

const propToName = {
  average: { name: 'Weight', unit: unitType.g, color: '#CFE8F2' },
  gain: { name: 'Gain', unit: unitType.g, color: '#F3C969' },
  count: { name: 'Count', unit: unitType.pieces, color: '#57A773' },
  uniformity: { name: 'Unifor.', unit: unitType.percent, color: '#EE6352' },
};

const Chart = ({
  stats, props, showCurve, useWeeks, curvePoints,
}) => {
  if (stats == null || stats.length === 0) return null;
  const last = stats[stats.length - 1];
  const first = stats[0];
  const st = fillMissingDays(stats, first.day, last.day, useWeeks);
  const curves = [...(props || [])];
  const hasWeight = curves.indexOf('average') !== -1;
  const chart = {
    labels: st.map(i => i.day),
    datasets: [],
  };

  if (hasWeight && curvePoints != null) {
    const cp = fillMissingDays(curvePoints, first.day, last.day + 1, useWeeks);
    chart.datasets.push({
      label: 'Target min',
      pointRadius: 0,
      pointHitRadius: 0,
      fill: false,
      data: cp.map(i => Math.round(i.weight * 0.95)),
      spanGaps: true,
    });
    chart.datasets.push({
      label: 'Target max',
      pointRadius: 0,
      pointHitRadius: 0,
      fill: false,
      data: cp.map(i => Math.round(i.weight * 1.05)),
      spanGaps: true,
    });
  }

  curves.forEach((prop) => {
    chart.datasets.unshift({
      id: prop,
      label: `${propToName[prop].name} (${propToName[prop].unit}) `,
      fill: false,
      borderColor: propToName[prop].color,
      data: st.map(i => (i.count === 0 ? null : roundTo(i[prop]))),
      spanGaps: true,
      cubicInterpolationMode: 'monotone',
    });
  });

  const options = {
    maintainAspectRatio: false,
    responsive: true,
    legend: false,
    animation: false,
    tooltips: {
      enabled: true,
      intersect: false,
      mode: 'x',
    },
    scales: {
      xAxes: [{
        gridLines: {
          drawOnChartArea: true,
          borderDash: [4, 8],
        },
        offset: true,
        ticks: {
          callback: (value) => {
            if (value == null) return null;
            return useWeeks ? value % 7 === 0 ? `${value / 7} week` : null : `${value} day`;
          },
        },
      }],
      yAxes: [{
        gridLines: {
          drawOnChartArea: false,
        },
        ticks: {
          suggestedMin: 0,
          suggestedMax: 100,
        },
      }],
    },
  };

  const disableAddCurve = Object.keys(propToName).length === curves.length;

  return (
    <React.Fragment>
      <div className="bwv-flock__graph-header">
        {
          // eslint-disable-next-line
          props.map(p => (
            <span
              className={`btn-legend${p === 'average' ? ' with-dot' : ''}`}
              style={{ backgroundColor: propToName[p].color }}
              role="button"
              key={p}
              onClick={() => showCurve(p)}
              aria-hidden
            >
              {`${propToName[p].name} (${propToName[p].unit})`}
              &nbsp;:&nbsp;
              {/* {values == null ? last[p] : values[p] || last[p]} */}
              <Round value={last[p]} />
            </span>
          ))
        }
        <UncontrolledDropdown addonType="append">
          <DropdownToggle tag={AddButton} disabled={disableAddCurve}>
            Add curve
          </DropdownToggle>
          <DropdownMenu>
            {
              Object.keys(propToName).filter(f => curves.indexOf(f) === -1).map(k => (
                <DropdownItem key={k} onClick={() => showCurve(k, true)}>
                  {propToName[k].name}
                </DropdownItem>
              ))
            }
          </DropdownMenu>
        </UncontrolledDropdown>

      </div>
      <div style={{ flex: 1 }}>
        <Line
          data={chart}
          options={options}
        />
      </div>
    </React.Fragment>
  );
};

Chart.propTypes = {
  showCurve: PropTypes.func.isRequired,
  stats: PropTypes.arrayOf(PropTypes.shape({
    day: PropTypes.number,
  })),
  curvePoints: PropTypes.arrayOf(PropTypes.shape({
    day: PropTypes.number,
    weight: PropTypes.number,
  })),
  props: PropTypes.arrayOf(PropTypes.string),
  useWeeks: PropTypes.bool,
  from: PropTypes.number,
  to: PropTypes.number,
};

Chart.defaultProps = {
  stats: [],
  curvePoints: null,
  props: null,
  useWeeks: false,
  from: null,
  to: null,
};

export default Chart;
