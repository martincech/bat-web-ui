import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { IMNext, IMPrevious } from '@veit/veit-web-controls/dist/icons';
import { Label } from '@veit/veit-web-controls';

class Navigation extends Component {
  componentDidMount() {
    document.addEventListener('keydown', this.keyDownHandler, false);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.keyDownHandler, false);
  }

  keyDownHandler = (e) => {
    if (e.keyCode !== 39 && e.keyCode !== 37) return;
    const goToFlock = e.keyCode === 39 ? this.props.next : this.props.prev;
    if (goToFlock != null && this.props.goTo != null) this.props.goTo(`/flock/${goToFlock.id}`);
  }

  render() {
    const { className, next, prev } = this.props;
    return (
      <div className={className}>
        {prev && (
          <Link to={`/flock/${prev.id}`}>
            <Label>
              <IMPrevious />
              Previous flock
            </Label>
          </Link>
        )}
        {next && (
          <Link to={`/flock/${next.id}`}>
            <Label>
              Next flock
              <IMNext />
            </Label>
          </Link>
        )}
      </div>
    );
  }
}

Navigation.propTypes = {
  goTo: PropTypes.func,
  className: PropTypes.string,
  next: PropTypes.shape({
    id: PropTypes.string,
  }),
  prev: PropTypes.shape({
    id: PropTypes.string,
  }),
};

Navigation.defaultProps = {
  className: null,
  next: null,
  prev: null,
  goTo: null,
};

export default Navigation;
