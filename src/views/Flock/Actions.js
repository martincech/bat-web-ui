import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@veit/veit-web-controls';

const Actions = ({ editFlock }) => {
  return (
    <React.Fragment>
      {/* <AddButton>Add Reminder</AddButton> */}
      <Button color="primary" outline onClick={editFlock}>Edit Flock</Button>
      {/* <Button color="primary" outline>Import Data</Button> */}
    </React.Fragment>
  );
};

Actions.propTypes = {
  editFlock: PropTypes.func.isRequired,
};

export default Actions;
