import React from 'react';
import PropTypes from 'prop-types';
import {
  Label, Table, StatusIcon,
} from '@veit/veit-web-controls';
import DateFormat from '../../components/DateFormat';
import Round from '../../components/Round';
import SexIcon from '../../components/SexIcon';

const StatsTable = ({ stats, useWeeks }) => {
  return (
    <Table type="data">
      <thead>
        <tr>
          <th>{useWeeks ? 'Week' : 'Day'}</th>
          <th className="text-center">Date</th>
          <th className="text-center">Growth</th>
          <th className="text-center">Sex</th>
          <th>Gain(g)</th>
          <th>Weight(g)</th>
          <th>Count(pcs)</th>
          <th>Unifor.(%)</th>
        </tr>
      </thead>
      <tbody>
        {stats.map(d => (
          <tr key={`${d.day}_${d.sex}`}>
            <td>
              <Label>
                {useWeeks ? Math.ceil(d.day / 7) : d.day}
                {useWeeks && (d.day % 7 !== 0) ? ` (${d.day} day)` : null}
              </Label>
            </td>
            <td className="text-center">
              <Label><DateFormat date={d.dateTime} /></Label>
            </td>
            <td className="text-center"><StatusIcon hasError={!(d.gain > 0)} /></td>
            <td className="text-center"><Label><SexIcon sex={d.sex} /></Label></td>
            <td><Label>{d.gain}</Label></td>
            <td><Label>{d.average}</Label></td>
            <td><Label>{d.count}</Label></td>
            <td>
              <Label>
                <Round value={d.uniformity} />
              </Label>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

StatsTable.propTypes = {
  stats: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
  })).isRequired,
  useWeeks: PropTypes.bool.isRequired,
};

export default StatsTable;
