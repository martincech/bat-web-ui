import { fetchAll } from '../../store/Fetch.actions';
import { getFlock, getFlockBy, expandFlock } from '../../store/Flock.actions';
import { getStatsFlockBy } from '../../store/Stats.actions';
import { getBirdBy, expandBird } from '../../store/Bird.actions';

export default function (id, fetchAllFlocks) {
  return (dispatch) => {
    fetchAll(() => [getFlockBy(id, expandFlock.all)(dispatch).then(async (r) => {
      if (r != null) {
        await Promise.all([
          getStatsFlockBy(id)(dispatch),
          fetchAllFlocks ? getFlock()(dispatch) : null,
          r.birdId ? getBirdBy(r.birdId, expandBird.curvePoints)(dispatch) : null,
        ]);
      }
    })])(dispatch);
  };
}
