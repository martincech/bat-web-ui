import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Button, AddButton } from '@veit/veit-web-controls';

import { openDialogAddHelpPage } from '../../store/ModalHelpPage.actions';
import { getHelpPage } from '../../store/Help.actions';
import Page from '../../components/Page';
import { roleType } from '../../utils/enums';

class HelpList extends Component {
  componentDidMount() {
    this.props.getHelpPage();
  }

  addPage = () => {
    this.props.openDialogAddHelpPage();
  }

  actions = () => {
    return (
      <React.Fragment>
        <Button color="primary" onClick={this.addPage}>
          Add new page
        </Button>
      </React.Fragment>
    );
  }

  render() {
    if (this.props.user == null) return null;
    const isAdmin = roleType.isAdmin(this.props.user.role);
    return (
      <Page
        title="Help"
        actions={isAdmin && <AddButton onClick={this.addPage}>Add Help Page</AddButton>}
      >
        {
          this.props.items.map(l => <Link key={l.slug} to={`/help/${l.slug}`}>{l.title}</Link>)
        }
      </Page>
    );
  }
}

HelpList.propTypes = {
  openDialogAddHelpPage: PropTypes.func.isRequired,
  getHelpPage: PropTypes.func.isRequired,
  user: PropTypes.shape({
    role: PropTypes.string,
  }).isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      slug: PropTypes.string,
      title: PropTypes.string,
    }),
  ).isRequired,
};

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
    items: state.help.items,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  openDialogAddHelpPage,
  getHelpPage,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HelpList);
