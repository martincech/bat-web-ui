import React from 'react';
import PropTypes from 'prop-types';
import { Label } from '@veit/veit-web-controls';
import { connect } from 'react-redux';

function getFarmLocation(farm) {
  if (farm.address != null && farm.country != null) return `${farm.address}, ${farm.country}`;
  return farm.address || farm.country;
}

const Farm = ({
  farms, farmId, houses, houseId,
}) => {
  if (farms == null || (farmId == null && houseId == null)) return null;
  const id = farmId || (houses.find(f => f.id === houseId) || {}).farmId;
  const farm = farms.find(f => f.id === id);
  return farm == null ? null : (
    <React.Fragment>
      <Label>{farm.name}</Label>
      <Label type="info">{getFarmLocation(farm)}</Label>
    </React.Fragment>
  );
};

Farm.propTypes = {
  farmId: PropTypes.string,
  farms: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
  })),
  houseId: PropTypes.string,
  houses: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
  })),
};

Farm.defaultProps = {
  farms: null,
  farmId: null,
  houses: null,
  houseId: null,
};

const mapStateToProps = (state) => {
  return {
    farms: state.farm.items,
    houses: state.house.items,
  };
};

export default connect(mapStateToProps)(Farm);
