import React from 'react';
import dayjs from 'dayjs';
import PropTypes from 'prop-types';
import { StatusIcon } from '@veit/veit-web-controls';
import Tooltip from '../../components/Tooltip';

function isValidUpdateDate(dateTime) {
  if (dateTime == null) return false;
  const yesterda = dayjs().startOf('day').add(-1, 'day');
  return dayjs(dateTime).isAfter(yesterda);
}

function isUpdated(stats) {
  const date = stats.map(m => m.dateTime).reduce((current, value) => current || value, null);
  return {
    updated: isValidUpdateDate(date),
    date,
  };
}

const UpdateStatus = ({ lastStatistics }) => {
  const stats = (lastStatistics || []).filter(f => f != null);
  const result = isUpdated(stats);

  const tooltip = !result.date
    ? 'No data yet.'
    : `Last update ${dayjs(result.date).format('DD/MM/YYYY')}`;

  return (
    <Tooltip text={tooltip}>
      <StatusIcon hasError={!result.updated} />
    </Tooltip>
  );
};

UpdateStatus.propTypes = {
  lastStatistics: PropTypes.arrayOf(PropTypes.shape({
    dateTime: PropTypes.object,
  })),
};

UpdateStatus.defaultProps = {
  lastStatistics: null,
};

export default UpdateStatus;
