import { fetchAll } from '../../store/Fetch.actions';
import { getDevice, expandDevice } from '../../store/Device.actions';
import { getHouse } from '../../store/House.actions';
import { getFarm, expandFarm } from '../../store/Farm.actions';

const fetchData = () => (dispatch) => {
  fetchAll(() => [
    getDevice(expandDevice.lastStatistic)(dispatch),
    getFarm([expandFarm.address, expandFarm.country])(dispatch),
    getHouse()(dispatch),
  ])(dispatch);
};

export default fetchData;
