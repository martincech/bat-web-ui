import React from 'react';
import PropTypes from 'prop-types';
import { Label } from '@veit/veit-web-controls';
import { connect } from 'react-redux';

function getFarmLocation(farm) {
  if (farm.address != null && farm.country != null) return `${farm.address}, ${farm.country}`;
  return farm.address || farm.country;
}

const Farm = ({ farms, farmId }) => {
  if (farmId == null || farms == null) return null;
  const farm = farms.find(f => f.id === farmId);
  return farm == null ? null : (
    <React.Fragment>
      <Label>{farm.name}</Label>
      <Label type="info">{getFarmLocation(farm)}</Label>
    </React.Fragment>
  );
};

Farm.propTypes = {
  farmId: PropTypes.string,
  farms: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
  })),
};

Farm.defaultProps = {
  farms: null,
  farmId: null,
};

const mapStateToProps = (state) => {
  return {
    farms: state.farm.items,
  };
};

export default connect(mapStateToProps)(Farm);
