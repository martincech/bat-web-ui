import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import {
  Table, Label, Button,
} from '@veit/veit-web-controls';
import { IMDialMeter } from '@veit/veit-web-controls/dist/icons';

import { openDialogAddDevice, openDialogEditDevice } from '../../store/ModalDevice.actions';
import goTo from '../../store/Router.actions';

import Page from '../../components/Page';
import bem from '../../utils/bem';
import { deviceTypeName, deviceType } from '../../utils/enums';
import { sortString } from '../../utils/sort';

import fetchData from './fetchData';
import House from './House';
import Farm from '../_partials/Farm';

class ConnectorList extends Component {
  componentDidMount() {
    this.props.fetchData();
  }

  shouldComponentUpdate(nextProps) {
    return this.props.fetch.isFetching !== nextProps.fetch.isFetching
      || this.props.device.items !== nextProps.device.items;
  }

  getValue = (deviceId, unit) => {
    const { average } = (this.props.stats.items.find(f => f.id === deviceId) || {});
    return average == null ? null : `${average} ${unit}`;
  }

  editDeviceHandler(e, device) {
    e.preventDefault();
    e.stopPropagation();
    this.props.openDialogEditDevice(device);
  }

  render() {
    const bm = bem.view('device-list');
    const devices = this.props.device.items.filter(f => f.type === deviceType.terminal);
    return (
      <Page
        className={bm.b()}
        title="My Connectors"
        actions={<Button to="/houses" tag={Link} color="primary" outline>Manage houses</Button>}
        isFetching={this.props.fetch.isFetching}
        emptyPage={{
          isEmpty: this.props.device.items.length === 0,
          icon: IMDialMeter,
          text: 'Nothing here.',
        }}
      >
        <Table type="data">
          <thead>
            <tr>
              <th>Device</th>
              <th>Type</th>
              {/* <th>Connected</th>
              <th>Active</th> */}
              <th>Farm</th>
              <th>House</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {sortString(devices, s => s.name).map(d => (
              <tr key={d.id}>
                <td><Label>{d.name}</Label></td>
                <td><Label>{deviceTypeName[d.type]}</Label></td>
                {/* <td><StatusIcon hasError={!d.connected} /></td>
                <td><StatusIcon hasError={!d.active} /></td> */}
                <td><Farm farmId={d.farmId} /></td>
                <td><House houseId={d.houseId} /></td>
                <td className="text-center">
                  <Button onClick={e => this.editDeviceHandler(e, d)} className="btn-narrow" color="primary">
                    Edit Connector
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Page>
    );
  }
}

ConnectorList.propTypes = {
  fetchData: PropTypes.func.isRequired,
  openDialogAddDevice: PropTypes.func.isRequired,
  openDialogEditDevice: PropTypes.func.isRequired,
  goTo: PropTypes.func.isRequired,
  device: PropTypes.shape({
    items: PropTypes.array,
  }),
  stats: PropTypes.shape({
    items: PropTypes.array,
  }),
  fetch: PropTypes.shape({
    isFetching: PropTypes.bool,
  }),
};

ConnectorList.defaultProps = {
  device: null,
  stats: null,
  fetch: {},
};

const mapStateToProps = (state) => {
  return {
    device: state.device,
    stats: state.stats,
    fetch: state.fetch,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchData,
  openDialogAddDevice,
  openDialogEditDevice,
  goTo,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ConnectorList);
