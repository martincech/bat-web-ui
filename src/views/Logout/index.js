import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { logoutRedirect } from '../../store/Auth.actions';

class Logout extends Component {
  componentWillMount() {
    this.props.logoutRedirect();
  }

  render() {
    return (
      <Redirect to="/" />
    );
  }
}
Logout.propTypes = {
  logoutRedirect: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => bindActionCreators({
  logoutRedirect,
}, dispatch);

export default connect(null, mapDispatchToProps)(Logout);
