import { fetchAll } from '../../store/Fetch.actions';
import { getFarm, expandFarm } from '../../store/Farm.actions';

const fetchData = () => (dispatch) => {
  fetchAll(() => [
    getFarm(expandFarm.list())(dispatch),
  ])(dispatch);
};

export default fetchData;
