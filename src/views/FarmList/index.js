import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  Table, Label, Button, AddButton,
} from '@veit/veit-web-controls';
import { IMSettings } from '@veit/veit-web-controls/dist/icons';

import { openDialogAddFarm, openDialogEditFarm } from '../../store/ModalFarm.actions';
import Page from '../../components/Page';
import bem from '../../utils/bem';
import { sortString } from '../../utils/sort';
import fetchData from './fetchData';

class FarmList extends Component {
  componentDidMount() {
    this.props.fetchData();
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.isFetching !== this.props.isFetching
      || nextProps.farms !== this.props.farms;
  }

  addFarmHandler = () => {
    this.props.openDialogAddFarm();
  }

  editFarmHandler = (farm) => {
    this.props.openDialogEditFarm(farm.id);
  }

  flockCount = (farmId, active) => {
    const flocks = this.props.flocks
      .filter(f => f.farmId === farmId && f.endDate == null)
      .map(m => new Date(m.startDate));
    const now = new Date();
    return (active ? flocks.filter(f => f < now) : flocks.filter(f => f > now)).length;
  }

  actions() {
    return (
      <React.Fragment>
        <AddButton onClick={this.addFarmHandler}>
          Add Farm
        </AddButton>
        <Button to="/houses" tag={Link} color="primary" outline>Manage houses</Button>
      </React.Fragment>
    );
  }

  render() {
    const bm = bem.view('farm-list');
    return (
      <Page
        className={bm.b()}
        title="Manage farms"
        actions={this.actions()}
        isFetching={this.props.isFetching}
        emptyPage={{
          isEmpty: this.props.farms.length === 0,
          icon: IMSettings,
          action: <AddButton onClick={this.addFarmHandler}>Add Farm</AddButton>,
        }}
      >
        <Table type="data">
          <thead>
            <tr>
              <th>Farm</th>
              <th>Address</th>
              <th>Country</th>
              <th>Active flocks</th>
              <th>Scheduled Flocks</th>
              <th>Houses</th>
              <th>Devices</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {sortString(this.props.farms, f => f.name).map(d => (
              <tr key={d.id}>
                <td><Label>{d.name}</Label></td>
                <td><Label>{d.address}</Label></td>
                <td><Label>{d.country}</Label></td>
                <td><Label>{d.activeFlocksCount || 0}</Label></td>
                <td><Label>{d.scheduledFlocksCount || 0}</Label></td>
                <td><Label>{d.housesCount || 0}</Label></td>
                <td><Label>{d.devicesCount || 0}</Label></td>
                <td className="text-center">
                  <Button className="btn-narrow" onClick={() => this.editFarmHandler(d)} color="primary">
                    Edit Farm
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Page>
    );
  }
}

FarmList.propTypes = {
  fetchData: PropTypes.func.isRequired,
  openDialogAddFarm: PropTypes.func.isRequired,
  openDialogEditFarm: PropTypes.func.isRequired,
  isFetching: PropTypes.bool.isRequired,
  farms: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    address: PropTypes.string,
    country: PropTypes.string,
  })),
  houses: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    farmId: PropTypes.string,
  })),
  flocks: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    farmId: PropTypes.string,
  })),
  devices: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    farmId: PropTypes.string,
  })),
};

FarmList.defaultProps = {
  farms: [],
  houses: [],
  flocks: [],
  devices: [],
};

const mapStateToProps = (state) => {
  return {
    farms: state.farm.items,
    houses: state.house.items,
    flocks: state.flock.items,
    devices: state.device.items,
    isFetching: state.fetch.isFetching,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchData,
  openDialogAddFarm,
  openDialogEditFarm,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FarmList);
