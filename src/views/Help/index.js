import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { MegadraftEditor, editorStateFromRaw, editorStateToJSON } from 'megadraft';
import { Button } from '@veit/veit-web-controls';

import { openDialogEditHelpPage } from '../../store/ModalHelpPage.actions';
import { openDialogSendGatewayRequest } from '../../store/ModalGateway.actions';
import { getHelpPageBy, putHelpPageContent } from '../../store/Help.actions';
import Page from '../../components/Page';
import { roleType, helpActionType } from '../../utils/enums';

import ImagePlugin from '../../components/Megadraft/ImagePlugin/plugin';
import TablePlugin from '../../components/Megadraft/TablePlugin/plugin';

class Help extends Component {
  state = {
    editorState: null,
    edit: false,
  };

  componentDidMount() {
    const { slug } = this.props.match.params;
    this.setState({ slug });
    this.props.getHelpPageBy(slug);
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.slug !== prevProps.match.params.slug) {
      this.setState({ slug: this.props.match.params.slug });
    }
  }

  onChange = (editorState) => {
    this.setState({ editorState });
  }

  editContent = () => {
    const { edit } = this.state;
    if (edit && this.state.editorState != null) {
      this.props.putHelpPageContent(this.props.item.id, editorStateToJSON(this.state.editorState));
    }
    this.setState({ edit: !edit, editorState: null });
  }

  edit = () => {
    this.props.openDialogEditHelpPage(this.props.item);
  }

  cancel = () => {
    this.setState({ editorState: null, edit: false });
  }

  actions = () => {
    if (!roleType.isAdmin(this.props.user.role)) return this.userActions();
    return (
      <React.Fragment>
        {
          this.userActions()
        }
        {
          !this.state.edit && (
            <Button color="primary" onClick={() => this.edit()}>
              Edit
            </Button>
          )
        }
        <Button color="primary" onClick={() => this.editContent()}>
          {this.state.edit ? 'Save' : 'Edit Content'}
        </Button>
        {
          this.state.edit && (
            <Button color="primary" onClick={() => this.cancel()}>
              Cancel
            </Button>
          )
        }
      </React.Fragment>
    );
  }

  userActions = () => {
    switch (this.props.item.actions) {
      case helpActionType.requestGateway:
        return (
          <Button
            onClick={() => this.props.openDialogSendGatewayRequest()}
          >
            Request SMS gateway
          </Button>
        );
      default:
        return null;
    }
  }

  render() {
    const { item, user } = this.props;
    const { slug, edit } = this.state;

    if (item == null || user == null || item.slug !== slug) return null;

    const editorState = this.state.editorState
      || editorStateFromRaw(item.content ? JSON.parse(item.content) : null);

    return (
      <Page className="bwv-help" title={item.title} actions={this.actions()} overflow={edit}>
        {
          editorState && (
            <MegadraftEditor
              plugins={[ImagePlugin, TablePlugin]}
              readOnly={!edit}
              editorState={editorState}
              onChange={this.onChange}
            />
          )
        }
      </Page>
    );
  }
}

Help.defaultProps = {
  item: null,
};

Help.propTypes = {
  getHelpPageBy: PropTypes.func.isRequired,
  putHelpPageContent: PropTypes.func.isRequired,
  openDialogEditHelpPage: PropTypes.func.isRequired,
  openDialogSendGatewayRequest: PropTypes.func.isRequired,
  item: PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    slug: PropTypes.string,
    content: PropTypes.string,
    actions: PropTypes.string,
  }),
  user: PropTypes.shape({
    role: PropTypes.string,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      slug: PropTypes.string,
    }),
  }).isRequired,
};

const mapStateToProps = (state) => {
  return {
    item: state.help.item,
    user: state.auth.user,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  openDialogEditHelpPage,
  getHelpPageBy,
  putHelpPageContent,
  openDialogSendGatewayRequest,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Help);
