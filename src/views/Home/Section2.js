import React from 'react';
import SectionText from './SectionText';

const Section2 = () => {
  return (
    <section className="section-2">
      <SectionText title="How it works" subtitle="Just connect cables and start using!" invert>
        {/* eslint-disable-next-line max-len */}
        All measured data is transferred to a server via the Internet and can be viewed in a web-based application anytime and anywhere.
      </SectionText>
    </section>
  );
};

export default Section2;
