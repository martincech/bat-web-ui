import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';

import { loginRedirect } from '../../store/Auth.actions';
import { openDialogRegisterUser } from '../../store/ModalUser.actions';
import bem from '../../utils/bem';

import { ReactComponent as VeitLogo } from '../../content/svg/VEIT_logo_150.svg';

import Section1 from './Section1';
// import Section2 from './Section2';
import Section3 from './Section3';
import Section4 from './Section4';


class Home extends Component {
  render() {
    if (this.props.isAuthenticated) return <Redirect to="/flocks" />;
    const bm = bem.view('home');
    return (
      <React.Fragment>
        <div className="sticky">
          <div className="nav-left">
            <a href="https://www.veit.cz/" target="_blank" rel="noreferrer noopener">
              <VeitLogo height="62px" />
            </a>
          </div>
          <div className="nav-right">
            <div className="link" role="presentation" onClick={() => this.props.openDialogRegisterUser()}>Sign up</div>
            <div className="link" role="presentation" onClick={() => this.props.loginRedirect()}>Log In</div>
          </div>
        </div>
        <div className={bm.b()}>
          <Section1 />
          {/* <Section2 /> */}
          <Section3 />
          <Section4 />
        </div>
      </React.Fragment>

    );
  }
}

Home.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  openDialogRegisterUser: PropTypes.func.isRequired,
  loginRedirect: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  openDialogRegisterUser,
  loginRedirect,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
