import React from 'react';
import SectionText from './SectionText';

const Section3 = () => {
  return (
    <section className="section-3">
      <SectionText title="BAT2 compatible" subtitle="Connect your scales today!" invert>
        {/* eslint-disable-next-line max-len */}
        Do you use BAT2 scales? Just connect them and enjoy the comfort of the BAT Cloud.
      </SectionText>
    </section>
  );
};

export default Section3;
