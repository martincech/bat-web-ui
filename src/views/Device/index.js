import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import {
  Button, Table, Label, StatusIcon,
} from '@veit/veit-web-controls';
import IMErrorMark from '@veit/veit-web-controls/dist/icons/IMErrorMark';

import { openDialogEditDevice } from '../../store/ModalDevice.actions';
import goTo from '../../store/Router.actions';

import fetchData from './fetchData';
import Subtitle from './Subtitle';
import bem from '../../utils/bem';
import { deviceType } from '../../utils/enums';
import Page from '../../components/Page';
import DateFormat from '../../components/DateFormat';
import SexIcon from '../../components/SexIcon';

class Device extends Component {
  componentDidMount() {
    if (this.props.id != null) {
      this.props.fetchData(this.props.id);
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.update == null && this.props.update) {
      this.props.fetchData(this.props.id);
    }
  }

  editDevice = () => {
    this.props.openDialogEditDevice(this.props.device);
  }

  applyFilter = (stats, ascending) => {
    if (stats == null) return [];
    return stats.sort((a, b) => (ascending ? a.day - b.day : b.day - a.day));
  }

  actions() {
    return (
      <React.Fragment>
        <Button color="primary" outline onClick={this.editDevice}>Edit Device</Button>
      </React.Fragment>
    );
  }

  render() {
    const bm = bem.view('device');
    const device = this.props.device || {};
    const data = Array.isArray(this.props.stats)
      ? this.props.stats
      : (this.props.stats || {}).data || [];
    const isWeight = deviceType.isWeight(device.type);
    return (
      <Page
        className={bm.b()}
        title={device.name}
        actions={this.actions()}
        isFetching={this.props.fetch.isFetching}
        notFound={device.id !== this.props.id}
        emptyPage={{
          isEmpty: data.length === 0,
          icon: IMErrorMark,
          text: 'Device does not have any data.',
        }}
        subtitle={(<Subtitle device={device} />)}
      >
        <Table type="data">
          <thead>
            <tr>
              <th className="text-center">Day</th>
              <th className="text-center">Date</th>
              {isWeight
                ? (
                  <React.Fragment>
                    <th className="text-center">Growth</th>
                    <th className="text-center">Sex</th>
                    <th>Gain(g)</th>
                    <th>Weight(g)</th>
                    <th>Count(pcs)</th>
                    <th>Unifor.(%)</th>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <th>{`Value (${device.unit})`}</th>
                    <th>{`Minimum (${device.unit})`}</th>
                    <th>{`Maximum (${device.unit})`}</th>
                    <th></th>
                  </React.Fragment>
                )
              }
            </tr>
          </thead>
          <tbody>
            {this.applyFilter(data).map((d, i) => (
              <tr key={`${d.day || i}_${d.sex}`}>
                <td className="text-center">
                  <Label>{d.day}</Label>
                </td>
                <td className="text-center">
                  <Label><DateFormat date={d.dateTime} /></Label>
                </td>
                {isWeight
                  ? (
                    <React.Fragment>
                      <td className="text-center"><StatusIcon hasError={!(d.gain > 0)} /></td>
                      <td className="text-center"><SexIcon sex={d.sex} /></td>
                      <td><Label>{d.gain}</Label></td>
                      <td><Label>{d.average}</Label></td>
                      <td><Label>{d.count}</Label></td>
                      <td><Label>{d.uniformity}</Label></td>
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <td><Label>{d.average}</Label></td>
                      <td><Label>{d.min}</Label></td>
                      <td><Label>{d.max}</Label></td>
                      <td></td>
                    </React.Fragment>
                  )
                }
              </tr>
            ))}
          </tbody>
        </Table>
      </Page>
    );
  }
}

Device.propTypes = {
  id: PropTypes.string.isRequired,
  fetchData: PropTypes.func.isRequired,
  openDialogEditDevice: PropTypes.func.isRequired,
  goTo: PropTypes.func.isRequired,
  fetch: PropTypes.shape({
    isFetching: PropTypes.bool,
  }).isRequired,
  device: PropTypes.shape({
    id: PropTypes.string,
    houseId: PropTypes.string,
    farmId: PropTypes.string,
  }),
  stats: PropTypes.oneOfType([
    PropTypes.shape({
      id: PropTypes.string,
    }),
    PropTypes.array,
  ]),
  update: PropTypes.bool,
};

Device.defaultProps = {
  device: null,
  stats: null,
  update: null,
};

const mapStateToProps = (state, ownProps) => {
  return {
    id: ownProps.match.params.id,
    device: state.device.item,
    stats: state.stats.item,
    fetch: state.fetch,
    update: state.modal.update,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchData,
  openDialogEditDevice,
  goTo,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Device);
