import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import SubtitleWrapper from '../../components/SubtitleWrapper';
import SubtitleItem from '../../components/SubtitleItem';
import { deviceType, deviceTypeName } from '../../utils/enums';

const Subtitle = ({
  device, farm, house,
}) => {
  return (
    <SubtitleWrapper>
      <SubtitleItem title="Device type :">{deviceTypeName[device.type]}</SubtitleItem>
      {
        device.type === deviceType.bat2Gsm && <SubtitleItem title="Phone number :">{device.phoneNumber}</SubtitleItem>
      }
      {
        farm && device.farmId === farm.id && (
          <SubtitleItem title="Farm :">{`${farm.name}${farm.country ? `, ${farm.country}` : ''}`}</SubtitleItem>)
      }
      {
        house && device.houseId === house.id && (
          <SubtitleItem title="House :">{house.name}</SubtitleItem>)
      }
    </SubtitleWrapper>
  );
};

Subtitle.propTypes = {
  device: PropTypes.shape({
    farmId: PropTypes.string,
    houseId: PropTypes.string,
  }).isRequired,
  farm: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
  }),
  house: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
  }),
};

Subtitle.defaultProps = {
  farm: null,
  house: null,
};

const mapStateToProps = (state) => {
  return {
    farm: state.farm.item,
    house: state.house.item,
  };
};

export default connect(mapStateToProps)(Subtitle);
