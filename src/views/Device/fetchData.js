import { fetchAll } from '../../store/Fetch.actions';
import { getDeviceBy } from '../../store/Device.actions';
import { getStatsDeviceBy } from '../../store/Stats.actions';
import { getFarmBy, expandFarm } from '../../store/Farm.actions';
import { getHouseBy } from '../../store/House.actions';

const fetchData = id => (dispatch) => {
  fetchAll(() => [
    getDeviceBy(id)(dispatch).then(async (r) => {
      if (r != null) {
        await Promise.all([
          getStatsDeviceBy(r.id)(dispatch),
          r.houseId ? getHouseBy(r.houseId)(dispatch) : null,
          r.farmId ? getFarmBy(r.farmId, [expandFarm.country])(dispatch) : null,
        ]);
      }
    }),
  ])(dispatch);
};

export default fetchData;
