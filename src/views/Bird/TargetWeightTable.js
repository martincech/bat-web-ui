import React from 'react';

import PropTypes from 'prop-types';
import { Table, Input, Label } from '@veit/veit-web-controls';

import arrayUtils from '../../utils/array';


const TableRow = ({ useWeeks, point }) => {
  return point == null ? null : (
    <tr>
      <td>
        {
          useWeeks ? (
            <Label>{`${Math.floor(point.day / 7)}${point.day % 7 === 0 ? '' : `\u00a0(${point.day}\u00a0day)`}`}</Label>
          ) : (
            <Label>{point.day}</Label>
          )
        }
      </td>
      <td>
        <Input
          readOnly
          type="number"
          name={point.day}
          defaultValue={point.weight}
        />
      </td>
    </tr>
  );
};

TableRow.propTypes = {
  useWeeks: PropTypes.bool.isRequired,
  point: PropTypes.shape({
    day: PropTypes.number,
    weight: PropTypes.number,
  }),
};

TableRow.defaultProps = {
  point: null,
};

const TargetWeightTable = ({ useWeeks, from, points }) => {
  return (
    <Table type="data">
      <thead>
        <tr>
          <th>{useWeeks ? 'Week' : 'Days'}</th>
          <th>Weigt (g)</th>
        </tr>
      </thead>
      <tbody>
        {arrayUtils.range(10).map(row => (
          <TableRow useWeeks={useWeeks} point={points[from + row]} key={from + row} />
        ))}
      </tbody>
    </Table>
  );
};

TargetWeightTable.propTypes = {
  from: PropTypes.number.isRequired,
  useWeeks: PropTypes.bool,
  points: PropTypes.arrayOf(PropTypes.shape({
    day: PropTypes.number,
    value: PropTypes.number,
  })),
};

TargetWeightTable.defaultProps = {
  useWeeks: false,
  points: [],
};

export default TargetWeightTable;
