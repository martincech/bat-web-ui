import React from 'react';
import PropTypes from 'prop-types';
import SubtitleWrapper from '../../components/SubtitleWrapper';
import Duration from '../../components/Duration';
import SubtitleItem from '../../components/SubtitleItem';

const Subtitle = ({ bird, ...props }) => {
  return (
    <SubtitleWrapper {...props}>
      <SubtitleItem title="Company :">{bird.company}</SubtitleItem>
      <SubtitleItem title="Product :">{bird.product}</SubtitleItem>
      <SubtitleItem title="Duration :">{bird && <Duration value={bird.duration} unit={bird.dateType} />}</SubtitleItem>
      <SubtitleItem title="Status :">Active</SubtitleItem>
    </SubtitleWrapper>
  );
};

Subtitle.propTypes = {
  bird: PropTypes.shape({
    company: PropTypes.string,
    product: PropTypes.string,
    duration: PropTypes.number,
  }).isRequired,
};

export default Subtitle;
