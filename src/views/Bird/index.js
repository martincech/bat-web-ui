import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Button, Container, Row, Col,
} from '@veit/veit-web-controls';

import { openDialogEditBird } from '../../store/ModalBird.actions';
import fetchData from './fetchData';
import Subtitle from './Subtitle';
import TargetWeightTable from './TargetWeightTable';
import Page from '../../components/Page';
import bem from '../../utils/bem';
import { dateType } from '../../utils/enums';

class Bird extends Component {
  componentDidMount() {
    this.props.fetchData(this.props.id);
  }

  editBird = () => {
    this.props.openDialogEditBird(this.props.bird.item);
  }

  getTables = (bird) => {
    if (bird == null || bird.curvePoints == null) return [];
    const lenght = bird.curvePoints.length || 1;
    const tablesCount = Math.ceil(lenght / 10);
    return [...Array(tablesCount).keys()].map(i => i * 10);
  }

  actions = () => {
    return (
      <React.Fragment>
        {/* <Button color="primary" outline onClick={this.editBird}>Edit Bird</Button> */}
        <Button to="/birds" tag={Link} color="primary" outline>Manage Birds</Button>
      </React.Fragment>
    );
  }

  render() {
    const bm = bem.view('bird');
    const { item } = this.props.bird;
    if (item == null) return null;
    const sortedPoints = item.curvePoints == null
      ? []
      : item.curvePoints.sort((a, b) => a.day - b.day);
    return (
      <Page
        className={bm.b()}
        title={item ? item.name : ''}
        subtitle={<Subtitle bird={item} />}
        actions={this.actions()}
        isFetching={this.props.fetch.isFetching}
        notFound={item.id !== this.props.id}
      >
        <Container fluid>
          <Row>
            {item && this.getTables(item).map(m => (
              <Col key={m}>
                <TargetWeightTable
                  from={m}
                  points={sortedPoints}
                  useWeeks={item.dateType === dateType.week}
                />
              </Col>
            ))}
          </Row>
        </Container>
      </Page>
    );
  }
}

Bird.propTypes = {
  fetchData: PropTypes.func.isRequired,
  openDialogEditBird: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  fetch: PropTypes.shape({
    isFetching: PropTypes.bool,
  }).isRequired,
  bird: PropTypes.shape({
    item: PropTypes.object,
  }),
};

Bird.defaultProps = {
  bird: null,
};

const mapStateToProps = (state, ownProps) => {
  return {
    id: ownProps.match.params.id,
    bird: state.bird,
    fetch: state.fetch,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchData,
  openDialogEditBird,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Bird);
