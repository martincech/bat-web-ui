import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Label, Button } from '@veit/veit-web-controls';

import goTo from '../../store/Router.actions';
import { deviceTypeName, unitType } from '../../utils/enums';
import House from './House';
import Farm from '../_partials/Farm';
import UpdateStatus from '../_partials/UpdateStatus';

const getValue = ({ lastStatistics }) => {
  if (lastStatistics == null) return null;
  return lastStatistics.map((ls) => {
    return ls == null || ls.average == null ? null : (
      <Label key={ls.sex}>
        {`${ls.average} ${unitType.g}`}
      </Label>
    );
  });
};

const TableRow = ({ device, editDeviceHandler, linkTo }) => {
  return (
    <tr className="has-pointer" onClick={() => linkTo(`/scale/${device.id}`)} key={device.id}>
      <td><Label>{device.name}</Label></td>
      <td><Label>{deviceTypeName[device.type]}</Label></td>
      {/* <td><StatusIcon hasError={!d.connected} /></td> */}
      <td>
        {
          device.lastStatistics && <UpdateStatus lastStatistics={device.lastStatistics} />
        }
      </td>
      <td>{getValue(device)}</td>
      <td><Farm farmId={device.farmId} houseId={device.houseId} /></td>
      <td><House houseId={device.houseId} /></td>
      <td className="text-center">
        <Button onClick={e => editDeviceHandler(e, device)} className="btn-narrow" color="primary">
          Edit Scale
        </Button>
      </td>
    </tr>
  );
};

TableRow.propTypes = {
  device: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
  }).isRequired,
  linkTo: PropTypes.func.isRequired,
  editDeviceHandler: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => bindActionCreators({
  linkTo: goTo,
}, dispatch);

export default connect(null, mapDispatchToProps)(TableRow);
