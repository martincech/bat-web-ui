import React from 'react';
import PropTypes from 'prop-types';
import { Label } from '@veit/veit-web-controls';
import { connect } from 'react-redux';

const House = ({ houses, houseId }) => {
  if (houseId == null || houses == null) return null;
  const house = houses.find(f => f.id === houseId);
  return house == null ? null : <Label>{house.name}</Label>;
};

House.propTypes = {
  houseId: PropTypes.string,
  houses: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
  })),
};

House.defaultProps = {
  houses: null,
  houseId: null,
};

const mapStateToProps = (state) => {
  return {
    houses: state.house.items,
  };
};

export default connect(mapStateToProps)(House);
