import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import {
  Table, Button, AddButton,
} from '@veit/veit-web-controls';
import { IMDialMeter } from '@veit/veit-web-controls/dist/icons';

import { openDialogAddDevice, openDialogEditDevice } from '../../store/ModalDevice.actions';
import goTo from '../../store/Router.actions';

import FeatureRequest from '../../components/FeatureRequest';
import Page from '../../components/Page';
import bem from '../../utils/bem';
import { deviceType } from '../../utils/enums';
import { sortString } from '../../utils/sort';

import fetchData from './fetchData';
import TableRow from './TableRow';

class DeviceList extends Component {
  componentDidMount() {
    this.props.fetchData();
  }

  shouldComponentUpdate(nextProps) {
    return this.props.fetch.isFetching !== nextProps.fetch.isFetching
      || this.props.device.items !== nextProps.device.items;
  }

  actions = () => {
    return (
      <React.Fragment>
        <FeatureRequest feature="Add Scale">
          <AddButton>Add Scale</AddButton>
        </FeatureRequest>
        {/* <AddButton onClick={() => this.props.openDialogAddDevice()}>Add Scale</AddButton> */}
        <Button to="/houses" tag={Link} color="primary" outline>Manage houses</Button>
      </React.Fragment>
    );
  }

  editDeviceHandler = (e, device) => {
    e.preventDefault();
    e.stopPropagation();
    this.props.openDialogEditDevice(device);
  }

  render() {
    const bm = bem.view('device-list');
    const devices = this.props.device.items.filter(f => f.type !== deviceType.terminal);
    return (
      <Page
        className={bm.b()}
        title="My Scales"
        actions={this.actions()}
        isFetching={this.props.fetch.isFetching}
        emptyPage={{
          isEmpty: this.props.device.items.length === 0,
          icon: IMDialMeter,
          text: 'Nothing here.',
        }}
      >
        <Table type="info">
          <thead>
            <tr>
              <th>Device</th>
              <th>Type</th>
              {/* <th>Connected</th> */}
              <th>Update</th>
              <th>Last value</th>
              <th>Farm</th>
              <th>House</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {sortString(devices, s => s.name).map(d => (
              <TableRow key={d.id} device={d} editDeviceHandler={this.editDeviceHandler} />
            ))}
          </tbody>
        </Table>
      </Page>
    );
  }
}

DeviceList.propTypes = {
  fetchData: PropTypes.func.isRequired,
  openDialogAddDevice: PropTypes.func.isRequired,
  openDialogEditDevice: PropTypes.func.isRequired,
  goTo: PropTypes.func.isRequired,
  device: PropTypes.shape({
    items: PropTypes.array,
  }),
  fetch: PropTypes.shape({
    isFetching: PropTypes.bool,
  }),
};

DeviceList.defaultProps = {
  device: null,
  fetch: {},
};

const mapStateToProps = (state) => {
  return {
    device: state.device,
    fetch: state.fetch,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchData,
  openDialogAddDevice,
  openDialogEditDevice,
  goTo,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(DeviceList);
