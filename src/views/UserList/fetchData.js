import { fetchAll } from '../../store/Fetch.actions';
import { getUser } from '../../store/User.actions';

const fetchData = () => (dispatch) => {
  fetchAll(() => [
    getUser()(dispatch),
  ])(dispatch);
};

export default fetchData;
