import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  Table, Label, Button, AddButton,
} from '@veit/veit-web-controls';

import fetchData from './fetchData';
import { openDialogInviteUser, openDialogEditUser } from '../../store/ModalUser.actions';

import Page from '../../components/Page';
// import DateFormat from '../../components/DateFormat';
import bem from '../../utils/bem';
import { sortString } from '../../utils/sort';
import { roleTypeName, roleType } from '../../utils/enums';


class UserList extends Component {
  componentDidMount() {
    this.props.fetchData();
  }

  inviteUser = () => {
    this.props.openDialogInviteUser({
      companyId: this.props.current.companyId,
    });
  }

  editUser = (user) => {
    this.props.openDialogEditUser(user);
  }

  canEdit(user) {
    if (this.props.current.role === roleType.undefined && user.role !== roleType.undefined) {
      return false;
    }
    return this.props.current.id !== user.id;
  }

  render() {
    const bm = bem.view('user-list');
    const { user, current } = this.props;
    const users = user.items.filter(f => f.companyId === current.companyId);
    return (
      <Page
        className={bm.b()}
        title="Users"
        isFetching={this.props.isFetching}
        actions={(
          <AddButton onClick={this.inviteUser}>
            Invite User
          </AddButton>
        )}
      >
        <Table type="data">
          <thead>
            <tr>
              <th>Email</th>
              <th>Name</th>
              {/* <th>Last login</th> */}
              <th>Role</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {sortString(users, s => s.email).map(i => (
              <tr key={i.id}>
                <td><Label>{i.email}</Label></td>
                <td><Label>{i.name}</Label></td>
                {/* <td><Label><DateFormat date={i.lastLogin} /></Label></td> */}
                <td><Label>{roleTypeName[i.role]}</Label></td>
                <td className="text-center">
                  {
                    this.canEdit(i) && (
                      <Button className="btn-narrow" onClick={() => this.editUser(i)} color="primary">
                        Edit User
                      </Button>
                    )
                  }
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Page>
    );
  }
}

UserList.propTypes = {
  fetchData: PropTypes.func.isRequired,
  openDialogInviteUser: PropTypes.func.isRequired,
  openDialogEditUser: PropTypes.func.isRequired,
  user: PropTypes.shape({
    items: PropTypes.array,
  }).isRequired,
  current: PropTypes.shape({
    id: PropTypes.string,
    companyId: PropTypes.string,
    role: PropTypes.string,
  }).isRequired,
  isFetching: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => {
  return {
    user: state.user,
    current: state.auth.user,
    isFetching: state.fetch.isFetching,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchData,
  openDialogInviteUser,
  openDialogEditUser,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
