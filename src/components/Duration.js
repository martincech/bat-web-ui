import PropTypes from 'prop-types';
import { dateType } from '../utils/enums';

const Duration = ({
  value, unit, singular, valueOnly, unitOnly,
}) => {
  const resultValue = unit === dateType.week ? Math.floor(value / 7) : value;
  if (valueOnly) return resultValue;
  const resultUnit = unit === dateType.week ? (singular ? dateType.week : 'weeks') : (singular ? dateType.day : 'days');
  if (unitOnly) return resultUnit;
  return `${resultValue} ${resultUnit}`;
};

Duration.propTypes = {
  value: PropTypes.number,
  unit: PropTypes.oneOf(['day', 'week']),
  singular: PropTypes.bool,
  valueOnly: PropTypes.bool,
  unitOnly: PropTypes.bool,
};

Duration.defaultProps = {
  value: 0,
  unit: 'day',
  singular: false,
  valueOnly: false,
  unitOnly: false,
};

export default Duration;
