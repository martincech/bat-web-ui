import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { openDialogSendFeatureRequest } from '../store/ModalFeature.actions';

const FeatureRequest = ({ children, feature, openDialog }) => {
  return (
    <div onClick={() => openDialog(feature)} role="presentation">
      {children}
    </div>
  );
};

const mapDispatchToProps = dispatch => bindActionCreators({
  openDialog: openDialogSendFeatureRequest,
}, dispatch);

FeatureRequest.propTypes = {
  children: PropTypes.node,
  feature: PropTypes.string.isRequired,
  openDialog: PropTypes.func.isRequired,
};

FeatureRequest.defaultProps = {
  children: null,
};

export default connect(null, mapDispatchToProps)(FeatureRequest);
