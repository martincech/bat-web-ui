import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Status } from '@veit/veit-web-controls';
import {
  IMBird, IMDialMeter, IMCloudUpload,
} from '@veit/veit-web-controls/dist/icons';
import { getStatus } from '../store/Status.actions';

class StatusBar extends Component {
  componentDidMount() {
    this.props.getStatus();
    this.interval = setInterval(() => this.props.getStatus(), 60000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { status } = this.props;
    return (
      <React.Fragment>
        <Status icon={IMBird} text="Flock growth" hasError={status.flockGrowthHasError} />
        <Status icon={IMDialMeter} text="Devices" hasError={status.devicesHasError} />
        <Status icon={IMCloudUpload} text="Data update" hasError={status.dataUpdateHasError} />
      </React.Fragment>
    );
  }
}

StatusBar.propTypes = {
  status: PropTypes.shape({
    flockGrowthHasError: PropTypes.bool,
    devicesHasError: PropTypes.bool,
    dataUpdateHasError: PropTypes.bool,
  }).isRequired,
  getStatus: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    status: state.status,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  getStatus,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(StatusBar);
