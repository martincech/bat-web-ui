import React from 'react';
import PropTypes from 'prop-types';
import { Label } from '@veit/veit-web-controls';
import bem from '../utils/bem';

const IconPage = ({
  icon: Icon, action, text, column,
}) => {
  const bm = bem.component('icon-page');
  return (
    <div className={bm.b()}>
      <Icon className={bm.e('icon')} />
      <div className={bm.e('content', column ? 'column' : null)}>
        <Label className={bm.e('text')} tag="span">{text}</Label>
        {action}
      </div>
    </div>
  );
};

IconPage.propTypes = {
  icon: PropTypes.func.isRequired,
  action: PropTypes.node,
  text: PropTypes.string,
  column: PropTypes.bool,
};

IconPage.defaultProps = {
  action: null,
  text: 'Nothing here. Start with',
  column: false,
};

export default IconPage;
