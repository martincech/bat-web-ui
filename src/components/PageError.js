import React from 'react';
import { Link } from 'react-router-dom';
import IM500 from '@veit/veit-web-controls/dist/icons/IM500';
import { Button } from '@veit/veit-web-controls/dist/index';
import IconPage from './IconPage';


const PageError = () => {
  return (
    <IconPage
      text="Something went wrong :("
      icon={IM500}
      column
      action={(<Button tag={Link} to="/flocks">Go to dashboard</Button>)}
    />
  );
};

export default PageError;
