import React from 'react';
import { Link } from 'react-router-dom';
import IM404 from '@veit/veit-web-controls/dist/icons/IM404';
import { Button } from '@veit/veit-web-controls/dist/index';
import IconPage from './IconPage';


const PageNotFound = () => {
  return (
    <IconPage
      text="Page not found :("
      icon={IM404}
      column
      action={(<Button tag={Link} to="/flocks">Go to dashboard</Button>)}
    />
  );
};

export default PageNotFound;
