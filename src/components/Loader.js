import React from 'react';
import bem from '../utils/bem';

const bm = bem.component('loader');
const Loader = () => {
  return (
    <div className={bm.b()}>
      <div>Preparing.</div>
      <div>Please wait ...</div>
    </div>
  );
};

export default Loader;
