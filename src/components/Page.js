import React from 'react';
import PropTypes from 'prop-types';
import { Label } from '@veit/veit-web-controls';
import bem from '../utils/bem';
import PageSkeleton from './PageSkeleton';
import IconPage from './IconPage';
import PageNotFound from './PageNotFound';

function setTitle(title) {
  const newTitle = `BAT WEB | ${title}`;
  if (document.title !== newTitle) {
    document.title = newTitle;
  }
}

const Page = ({
  title, subtitle, actions, children, className, isFetching, emptyPage, notFound,
  header, footer, overflow,
}) => {
  setTitle(title);
  if (isFetching) return <PageSkeleton hasSubtitle={subtitle != null} />;
  if (notFound) return <PageNotFound />;
  const bm = bem.component('page');
  const mod = overflow ? bm.m('overflow') : null;
  return (
    <div className={bm.b(mod)}>
      <div className={bm.e('header')}>
        <div className={bm.e('title')}>
          <Label type="title">{title}</Label>
          {subtitle}
        </div>
        <div className={bm.e('actions', 'text-right')}>
          {actions}
        </div>
      </div>
      <div className={bm.e('body', className)}>
        {header}
        {emptyPage != null && emptyPage.isEmpty
          ? (
            <React.Fragment>
              <IconPage icon={emptyPage.icon} action={emptyPage.action} text={emptyPage.text} />
            </React.Fragment>
          )
          : children
        }
        {footer}
      </div>
    </div>
  );
};

Page.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
  actions: PropTypes.node,
  children: PropTypes.node,
  className: PropTypes.string,
  isFetching: PropTypes.bool,
  notFound: PropTypes.bool,
  header: PropTypes.node,
  footer: PropTypes.node,
  overflow: PropTypes.bool,
  emptyPage: PropTypes.shape({
    isEmpty: PropTypes.bool,
    icon: PropTypes.func,
    action: PropTypes.node,
    text: PropTypes.string,
  }),
};

Page.defaultProps = {
  title: '',
  subtitle: null,
  actions: null,
  children: null,
  header: null,
  footer: null,
  className: null,
  isFetching: false,
  emptyPage: null,
  notFound: null,
  overflow: false,
};

export default Page;
