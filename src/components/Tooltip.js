import React from 'react';
import PropTypes from 'prop-types';
import bem from '../utils/bem';

const bm = bem.component('tooltip');

const Tooltip = ({ text, children }) => {
  return (
    <div className={bm.b()}>
      {children}
      <span className="tooltiptext">{text}</span>
    </div>
  );
};

Tooltip.propTypes = {
  text: PropTypes.string.isRequired,
  children: PropTypes.node,
};

Tooltip.defaultProps = {
  children: null,
};

export default Tooltip;
