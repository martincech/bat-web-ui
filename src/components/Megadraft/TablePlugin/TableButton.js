import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { insertDataBlock, MegadraftIcons } from 'megadraft';

class TableButton extends Component {
  onClick = (e) => {
    e.preventDefault();
    this.props.onChange(insertDataBlock(this.props.editorState, { type: 'table-plugin', rows: [] }));
  }

  render() {
    return (
      <button
        className={this.props.className}
        type="button"
        onClick={this.onClick}
        title={this.props.title}
      >
        <MegadraftIcons.ImageIcon className="sidemenu__button__icon" />
      </button>
    );
  }
}

TableButton.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  // eslint-disable-next-line react/forbid-prop-types
  editorState: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
};

TableButton.defaultProps = {
  className: null,
  title: null,
};

export default TableButton;
