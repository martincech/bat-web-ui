import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  MegadraftIcons,
  MegadraftPlugin,
} from 'megadraft';
import { Table, Label } from '@veit/veit-web-controls';

import BaseBlock from '../BaseBlock';


class TableBlock extends Component {
  state = {
    edit: false,
    text: '[]',
  }

  getString = (rows) => {
    try {
      return JSON.stringify(rows, null, 2);
    } catch (e) {
      return '[]';
    }
  }

  getTable = (rows) => {
    const header = rows[0];
    const body = header ? rows.slice(1) : null;
    return (
      <div className="md-table-plugin">
        <Table type="data">
          <thead>
            <tr>
              {header && header.map(v => <th key={v}><Label>{v}</Label></th>)}
            </tr>
          </thead>
          <tbody>
            {
              body && body.map(r => (
                <tr key={r}>
                  {r && r.map(v => <td key={v}><Label>{v}</Label></td>)}
                </tr>
              ))
            }
          </tbody>
        </Table>
      </div>
    );
  }

  edit = (rows) => {
    const { edit } = this.state;
    let state = { edit: !edit };
    if (edit) {
      try {
        const rows = JSON.parse(this.state.text);
        this.props.container.updateData({ rows });
      } catch (e) {
        // eslint-disable-next-line no-undef
        alert('Invalid table content. Required format json format [["header","header"],["value","value"]].');
      }
    } else {
      state = { ...state, text: this.getString(rows) };
    }
    this.setState(state);
  }

  change = (e) => {
    this.setState({ text: e.target.value });
  }

  render() {
    const isReadOnly = this.props.blockProps.getInitialReadOnly();
    const rows = this.props.data.rows || [];

    if (isReadOnly) {
      return this.getTable(rows);
    }

    const { edit } = this.state;

    const actions = edit
      ? [
        {
          key: 'ok',
          icon: MegadraftIcons.CrossIcon,
          action: () => this.edit(),
        },
      ] : [
        {
          key: 'edit',
          icon: MegadraftIcons.EditIcon,
          action: () => this.edit(rows),
        },
        {
          key: 'delete',
          icon: MegadraftIcons.DeleteIcon,
          action: this.props.container.remove,
        },
      ];

    return (
      <BaseBlock {...this.props} actions={actions}>
        <MegadraftPlugin.BlockContent>
          {edit
            ? (
              <MegadraftPlugin.BlockInput value={this.state.text} onChange={this.change} />
            ) : this.getTable(rows)}
        </MegadraftPlugin.BlockContent>
      </BaseBlock>
    );
  }
}

TableBlock.propTypes = {
  blockProps: PropTypes.shape({
    getInitialReadOnly: PropTypes.func,
  }).isRequired,
  container: PropTypes.shape({
    updateData: PropTypes.func,
    remove: PropTypes.func,
  }).isRequired,
  data: PropTypes.shape({
    rows: PropTypes.array,
  }).isRequired,
};

export default TableBlock;
