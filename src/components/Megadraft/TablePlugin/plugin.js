import TableButton from './TableButton';
import TableBlock from './TableBlock';

export default {
  title: 'Table',
  type: 'table-plugin',
  buttonComponent: TableButton,
  blockComponent: TableBlock,
};
