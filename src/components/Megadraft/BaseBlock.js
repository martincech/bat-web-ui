import React from 'react';
import PropTypes from 'prop-types';
import { MegadraftPlugin } from 'megadraft';

const { BlockWrapper, BlockControls, BlockActionGroup } = MegadraftPlugin;

const BaseBlock = ({ actions, children }) => {
  return (
    <BlockWrapper>
      <BlockControls>
        <BlockActionGroup items={actions} />
      </BlockControls>
      {children}
    </BlockWrapper>
  );
};

BaseBlock.propTypes = {
  actions: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.string,
  })).isRequired,
  children: PropTypes.node.isRequired,
};

export default BaseBlock;
