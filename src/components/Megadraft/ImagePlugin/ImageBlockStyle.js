export default {
  image: {
    display: 'inline-block',
    maxWidth: '100%',
    verticalAlign: 'middle',
  },
};
