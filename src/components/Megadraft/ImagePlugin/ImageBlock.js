import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  MegadraftIcons,
  MegadraftPlugin,
} from 'megadraft';

import BaseBlock from '../BaseBlock';
import ImageBlockStyle from './ImageBlockStyle';


class ImageBlock extends Component {
  constructor(props) {
    super(props);

    this.actions = [
      {
        key: 'delete',
        icon: MegadraftIcons.DeleteIcon,
        action: this.props.container.remove,
      },
    ];
  }

  render() {
    const isReadOnly = this.props.blockProps.getReadOnly();
    const content = (<img style={ImageBlockStyle.image} src={this.props.data.src} alt="" />);
    return isReadOnly
      ? (
        <div style={{ textAlign: 'center' }}>
          {content}
        </div>
      ) : (
        <BaseBlock {...this.props} actions={this.actions}>
          <MegadraftPlugin.BlockContent>
            {content}
          </MegadraftPlugin.BlockContent>
        </BaseBlock>
      );
  }
}

ImageBlock.propTypes = {
  blockProps: PropTypes.shape({
    getReadOnly: PropTypes.func,
  }).isRequired,
  container: PropTypes.shape({
    updateData: PropTypes.func,
    remove: PropTypes.func,
  }).isRequired,
  data: PropTypes.shape({
    src: PropTypes.string,
  }).isRequired,
};

export default ImageBlock;
