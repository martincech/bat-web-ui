import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { insertDataBlock, MegadraftIcons } from 'megadraft';

class ImageButton extends Component {
  onClick = (e) => {
    e.preventDefault();
    const src = window.prompt('Enter a URL');
    if (!src) {
      return;
    }

    const data = { src, type: 'image', display: 'medium' };

    this.props.onChange(insertDataBlock(this.props.editorState, data));
  }

  render() {
    return (
      <button
        className={this.props.className}
        type="button"
        onClick={this.onClick}
        title={this.props.title}
      >
        <MegadraftIcons.ImageIcon className="sidemenu__button__icon" />
      </button>
    );
  }
}

ImageButton.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  // eslint-disable-next-line react/forbid-prop-types
  editorState: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
};

ImageButton.defaultProps = {
  className: null,
  title: null,
};

export default ImageButton;
