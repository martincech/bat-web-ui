import React from 'react';
import PropTypes from 'prop-types';
import { Label } from '@veit/veit-web-controls';

const SubtitleItem = ({ title, children }) => {
  return (
    <Label tag="span">
      {title}
      <Label tag="span">
        {children}
      </Label>
    </Label>
  );
};

SubtitleItem.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node,
};

SubtitleItem.defaultProps = {
  children: null,
};

export default SubtitleItem;
