import dayjs from 'dayjs';
import PropTypes from 'prop-types';

const DateFormat = ({ date, format, diff }) => {
  if (date == null) return null;
  if (diff != null) return Math.ceil(dayjs().diff(date, diff, true));
  return dayjs(date).format(format);
};

DateFormat.propTypes = {
  date: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Date),
  ]),
  format: PropTypes.string,
  diff: PropTypes.oneOf([null, 'months', 'weeks', 'days', 'hours']),
};

DateFormat.defaultProps = {
  date: null,
  format: 'DD/MM/YYYY',
  diff: null,
};

export default DateFormat;
