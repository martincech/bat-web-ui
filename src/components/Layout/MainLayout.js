import React, { useState } from 'react';
import PropTypes from 'prop-types';
// import ModalContainer from '../../setup/ModalContainer';
import ErrorBoundary from '../ErrorBoundary';
import Header from './Header';
import Content from './Content';
import Sidebar from './Sidebar';
import { Button } from '@veit/veit-web-controls';
import IMMenu from '@veit/veit-web-controls/dist/icons/IMMenu';

const MainLayout = ({ children }) => {
  const [showSidebar, setShowSidebar] = useState(true);
  return (
    <main className={showSidebar ? 'sidebar-open' : null}>
      <Sidebar show={showSidebar} />
      <Header
        action={
          <Button style={{ display: 'none' }} color="primary" outline={!showSidebar} onClick={() => setShowSidebar(!showSidebar)}><IMMenu /></Button>
        }
      />
      <Content hideSidebar={() => setShowSidebar(true)}>
        <ErrorBoundary>
          {children}
        </ErrorBoundary>
      </Content>
      {/* <ModalContainer /> */}
    </main>
  );
};

MainLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default MainLayout;
