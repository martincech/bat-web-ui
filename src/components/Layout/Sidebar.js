import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import {
  Nav,
  NavItem,
  Collapse,
  NavLink as BSNavLink,
} from '@veit/veit-web-controls';

import {
  IMBird,
  IMDialMeter,
  // IMCalendar,
  IMSettings,
  IMGuide,
  IMFeedback,
} from '@veit/veit-web-controls/dist/icons';

import BatIcon from '../BatIcon';
import { openDialogSendFeedback } from '../../store/ModalFeedback.actions';
import { roleType } from '../../utils/enums';
import bm from '../../utils/bem';

const nav = [
  {
    path: '/flock',
    to: '/flocks',
    name: 'Flocks',
    Icon: IMBird,
    subNav: [
      {
        path: '/flocks', to: '/flocks', name: 'Active', exact: true,
      },
      { path: '/flocks/scheduled', to: '/flocks/scheduled', name: 'Scheduled' },
      { path: '/flocks/finished', to: '/flocks/finished', name: 'Finished' },
      { path: '/flocks/all', to: '/flocks/all', name: 'All' },
    ],
  },
  {
    path: '/scale',
    to: '/scales',
    name: 'Devices',
    Icon: IMDialMeter,
    subNav: [
      { path: '/scale', to: '/scales', name: 'Scales' },
      { path: '/connector', to: '/connectors', name: 'Connectors' },
    ],
  },
  // {
  //   path: '/task', to: '/tasks', name: 'Tasks', Icon: IMCalendar,
  // },
  {
    path: '/farm',
    to: '/farms',
    name: 'Manager',
    Icon: IMSettings,
    subNav: [
      {
        path: '/farm', to: '/farms', name: 'Farms',
      },
      {
        path: '/house', to: '/houses', name: 'Houses',
      },
      {
        path: '/bird', to: '/birds', name: 'Birds',
      },
      {
        path: '/user', to: '/users', name: 'Users',
      },
      // {
      //   path: '/environment', to: '/environment', name: 'Environment',
      // },
    ],
  },
];

const botNav = [
  {
    path: '/help', to: '/help', name: 'Help', Icon: IMGuide, admin: true,
  },
  {
    to: '/feedback', name: 'Feedback', Icon: IMFeedback, action: props => props.openDialogSendFeedback(),
  },
];

class Sidebar extends Component {
  isActive = (location) => {
    return (location.exact
      ? this.props.location.pathname === location.path
      : this.props.location.pathname.indexOf(location.path) !== -1)
      || (location.subNav && location.subNav.find(f => this.isActive(f)) != null);
  }

  filter = (link) => {
    const user = this.props.auth.user || {};
    return link.admin ? roleType.isAdmin(user.role) : true;
  }

  mapItems = (items, bem) => {
    return items.filter(this.filter).map((n) => {
      const { Icon, subNav } = n;
      return (
        <React.Fragment key={n.to}>
          <NavItem
            onClick={n.action ? () => n.action(this.props) : null}
            className={bem.e('nav-item', n.subNav ? 'nav-item-simple' : null)}
            active={this.isActive(n, subNav != null)}
          >
            <BSNavLink tag={n.action ? 'div' : NavLink} className={bem.e('nav-item-collapse')} to={n.to}>
              <div className="d-flex">
                <Icon className={bem.e('nav-item-icon')} />
                <span className=" align-self-start">{n.name}</span>
              </div>
            </BSNavLink>
          </NavItem>
          {subNav && (
            <Collapse isOpen={this.isActive(n)}>
              {
                subNav.filter(this.filter).map(s => (
                  <NavItem
                    key={s.to}
                    className={bem.e('nav-item', 'nav-item-sub')}
                    active={this.isActive(s)}
                  >
                    <BSNavLink tag={NavLink} to={s.to}>
                      <span className="">{s.name}</span>
                    </BSNavLink>
                  </NavItem>
                ))}
            </Collapse>
          )}
        </React.Fragment>
      );
    });
  }

  render() {
    const bem = bm.component('sidebar');
    return (
      <div className={bem.b()}>
        <div className="bwc-sidebar__content">
          <BatIcon className={bem.e('logo')} />
          <Nav vertical>
            {
              this.mapItems(nav, bem)
            }
          </Nav>
          <div style={{ flex: 1 }}></div>
          <Nav vertical>
            {
              this.mapItems(botNav, bem)
            }
          </Nav>
        </div>
      </div>
    );
  }
}

Sidebar.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  auth: PropTypes.shape({
    user: PropTypes.object,
  }).isRequired,
  show: PropTypes.bool,
};

Sidebar.defaultProps = {
  show: true,
};

const mapStateToProps = (state) => {
  return {
    location: state.router.location,
    auth: state.auth,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  openDialogSendFeedback,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
