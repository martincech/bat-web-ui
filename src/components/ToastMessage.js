import React from 'react';
import PropTypes from 'prop-types';
import { IMClose, IMErrorMark, IMCheckMark } from '@veit/veit-web-controls/dist/icons';
import bem from '../utils/bem';


export const ToastError = ({ message }) => {
  return <ToastBase message={message} icon={IMErrorMark} type="error" />;
};

ToastError.propTypes = {
  message: PropTypes.string.isRequired,
};

export const ToastSuccess = ({ message }) => {
  return <ToastBase message={message} icon={IMCheckMark} type="success" />;
};

ToastSuccess.propTypes = {
  message: PropTypes.node.isRequired,
};

const bm = bem.component('toast-message');

const ToastBase = ({ message, icon: Icon, type }) => {
  return (
    <div className={bm.b(type)}>
      <div className={bm.e('icon')}>
        <Icon />
      </div>
      <div className={bm.e('message')}>
        {message}
      </div>
      <div className={bm.e('close')}>
        <IMClose />
      </div>
    </div>
  );
};

ToastBase.propTypes = {
  icon: PropTypes.func.isRequired,
  message: PropTypes.node.isRequired,
  type: PropTypes.oneOf(['success', 'error', null]),
};

ToastBase.defaultProps = {
  type: null,
};

export default ToastBase;
