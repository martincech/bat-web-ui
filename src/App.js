import React from 'react';
import { Switch } from 'react-router';
import { Route } from 'react-router-dom';
import PublicLayout from './components/Layout/PublicLayout';
import LayoutRoute from './setup/LayoutRoute';

import Home from './views/Home';
import Login from './views/Login';
import Logout from './views/Logout';
import Flock from './views/Flock';
import FlockList from './views/FlockList';
import Device from './views/Device';
import DeviceList from './views/DeviceList';
import ConnectorList from './views/ConnectorList';
import FarmList from './views/FarmList';
import HouseList from './views/HouseList';
import BirdList from './views/BirdList';
import Bird from './views/Bird';
import Environment from './views/Environment';
import Help from './views/Help';
import HelpList from './views/HelpList';
import TaskList from './views/TaskList';
import UserList from './views/UserList';
import Tokens from './views/Tokens';

import PageNotFound from './components/PageNotFound';

// ADMIN
import CompanyList from './views/CompanyList';
import { roleType } from './utils/enums';

export default () => (
  <Switch>
    <LayoutRoute
      exact
      publicPath
      path="/"
      layout={PublicLayout}
      component={Home}
    />
    <LayoutRoute
      exact
      publicPath
      path="/logout"
      layout={PublicLayout}
      component={Logout}
    />
    <LayoutRoute
      exact
      publicPath
      path="/login"
      layout={PublicLayout}
      component={Login}
    />
    <LayoutRoute
      path="/flocks/:filter?"
      component={FlockList}
    />
    <LayoutRoute
      path="/flock/:id"
      component={Flock}
    />
    <LayoutRoute
      path="/scales"
      component={DeviceList}
    />
    <LayoutRoute
      path="/scale/:id"
      component={Device}
    />
    <LayoutRoute
      path="/connectors"
      component={ConnectorList}
    />
    <LayoutRoute
      path="/tasks"
      component={TaskList}
    />
    <LayoutRoute
      path="/farms"
      component={FarmList}
    />
    <LayoutRoute
      path="/houses"
      component={HouseList}
    />
    <LayoutRoute
      path="/birds"
      component={BirdList}
    />
    <LayoutRoute
      path="/birds/public"
      component={BirdList}
    />
    <LayoutRoute
      path="/bird/:id"
      component={Bird}
    />
    <LayoutRoute
      path="/environment"
      component={Environment}
    />
    <LayoutRoute
      exact
      path="/help"
      component={HelpList}
      roles={[roleType.veitOperator]}
    />
    <LayoutRoute
      path="/help/:slug"
      component={Help}
      roles={[roleType.veitOperator]}
    />
    <LayoutRoute
      exact
      path="/companies"
      layout={PublicLayout}
      component={CompanyList}
      roles={[roleType.veitOperator]}
    />
    <LayoutRoute
      path="/users"
      component={UserList}
    />
    <Route
      path="/tokens"
      component={Tokens}
    />
    <LayoutRoute
      publicPath
      layout={PublicLayout}
      component={PageNotFound}
    />
  </Switch>
);
