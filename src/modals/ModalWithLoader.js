import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Modal } from '@veit/veit-web-controls';

const ModalWithLoader = ({
  isExecuting, children, dispatch, ...otherProps
}) => {
  return (
    <Modal {...otherProps}>
      {children}
      {isExecuting ? (
        <React.Fragment>
          <div className="modal-content-loader"></div>
          <div className="modal-content-backdrop"></div>
        </React.Fragment>
      ) : null}
    </Modal>
  );
};

ModalWithLoader.propTypes = {
  isExecuting: PropTypes.bool,
  children: PropTypes.node,
  dispatch: PropTypes.func,
};

ModalWithLoader.defaultProps = {
  isExecuting: false,
  children: null,
  dispatch: null,
};

const mapStateToProps = (state) => {
  return {
    isExecuting: state.modal.isExecuting,
  };
};

export default connect(mapStateToProps)(ModalWithLoader);
