import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import {
  ModalHeader,
  ModalBody,
  Container,
  Row,
  Col,
} from '@veit/veit-web-controls';

import ModalActions, { finishDialogEditFlock, deleteDialogFlock } from '../../store/ModalFlock.actions';

import { getBird, getPublicBird } from '../../store/Bird.actions';
import { getHouse } from '../../store/House.actions';
import { getFarm } from '../../store/Farm.actions';
import { getDevice } from '../../store/Device.actions';

import ModalWithLoader from '../ModalWithLoader';
import Footer from '../Footer';
import CloseFlockDialog from './CloseFlockDialog';
import Step1 from './Step1';
import Step2, { hasStep2Error } from './Step2';
import Step3, { hasStep3Error } from './Step3';

class EditFlockModal extends Component {
  componentDidMount() {
    this.fetchData();
  }

  cancel = () => {
    this.props.cancelDialog();
  }

  finish = () => {
    this.props.finishDialogEditFlock(this.props.modal.data);
  }

  delete = () => {
    this.props.deleteDialogFlock(this.props.modal.data.id);
  }

  closeFlock = () => {
    this.props.finishDialogEditFlock({
      ...this.props.modal.data,
      endDate: new Date().toISOString(),
    });
  }

  isScheduled() {
    return dayjs(this.props.modal.data.startDate).toDate() > new Date();
  }

  fetchData() {
    this.props.setValidation({
      1: [hasStep2Error, hasStep3Error],
    });
    this.props.getBird();
    this.props.getPublicBird();
    this.props.getDevice();
    this.props.getHouse();
    this.props.getFarm();
  }

  render() {
    const { errors } = this.props.modal;
    const disabled = this.props.modal.data != null && this.props.modal.data.endDate != null;
    return (
      <ModalWithLoader
        centered
        isOpen
        keyboard={false}
        toggle={this.cancel}
        contentClassName="modal-fluid"
      >
        <ModalHeader toggle={this.cancel}>
          Edit Flock
          {this.props.modal.data && this.props.modal.data.endDate && ' [FINISHED]'}
          {this.props.modal.data && this.isScheduled() && ' [SCHEDULED]'}
        </ModalHeader>
        <ModalBody style={{ maxWidth: 'unset', padding: '0' }}>
          <Container>
            <Row>
              <Col>
                <Step1 disabled={disabled} />
                <br />
                <Step2 disabled={disabled} />
              </Col>
              <Col>
                <Step3 disabled={disabled} />
              </Col>
            </Row>
          </Container>
        </ModalBody>
        <Footer
          finisDialog={this.finish}
          deleteDialog={{ action: this.delete, name: 'Flock' }}
          errors={errors && errors[1]}
        >
          {disabled ? null : <CloseFlockDialog confirm={this.closeFlock} />}
        </Footer>
      </ModalWithLoader>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  setValidation: ModalActions.setValidation,
  updateForm: ModalActions.updateForm,
  finishDialogEditFlock,
  deleteDialogFlock,
  getBird,
  getPublicBird,
  getHouse,
  getFarm,
  getDevice,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

EditFlockModal.propTypes = {
  cancelDialog: PropTypes.func.isRequired,
  setValidation: PropTypes.func.isRequired,
  updateForm: PropTypes.func.isRequired,
  finishDialogEditFlock: PropTypes.func.isRequired,
  deleteDialogFlock: PropTypes.func.isRequired,
  getBird: PropTypes.func.isRequired,
  getPublicBird: PropTypes.func.isRequired,
  getHouse: PropTypes.func.isRequired,
  getFarm: PropTypes.func.isRequired,
  getDevice: PropTypes.func.isRequired,
  modal: PropTypes.shape({
    type: PropTypes.string,
    target: PropTypes.string,
    data: PropTypes.object,
    errors: PropTypes.object,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditFlockModal);
