import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { deviceType } from '../../utils/enums';
import { sortString } from '../../utils/sort';
import ModalActions from '../../store/ModalFlock.actions';
import { isActiveFlock, isScheduledFlock, sensorsInUse } from '../../utils/flock';

import {
  ButtonSwitch,
  Label,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  StatusIcon,
} from '@veit/veit-web-controls';

export function hasStep3Error({
  useDevice, deviceId, houseId, endDate,
}) {
  return {
    target: useDevice == null
      ? deviceId == null
      && houseId == null : (useDevice ? deviceId == null : houseId == null),
    endDate: endDate != null,
  };
}

const Step3 = ({
  modal, houses, farms, devices, disabled, updateForm, updateFormObject, flocks,
}) => {
  const form = modal.data;
  if (form == null) return null;
  const house = houses.find(f => f.id === form.houseId);
  const farm = farms.find(f => f.id === (form.farmId == null ? (house || {}).farmId : form.farmId));
  const sensors = devices.filter(f => f.type !== deviceType.terminal);
  const device = sensors.find(f => f.id === form.deviceId);
  const useDevice = form.useDevice == null ? form.deviceId != null : form.useDevice;

  const edit = form.id != null;
  const isActive = edit && isActiveFlock(modal.base);
  const isScheduled = edit && isScheduledFlock(modal.base);
  const inUse = edit ? {} : sensorsInUse(form.startDate, flocks, sensors);
  return (
    <React.Fragment>
      <Label type="text">Measure by</Label>
      <ButtonSwitch
        disabled={disabled || isActive || isScheduled}
        options={[{ title: 'Whole house', value: false }, { title: 'Single device', value: true }]}
        selected={useDevice}
        onChange={v => updateFormObject({ useDevice: v, deviceId: null })}
      />
      {!edit && <Label type="info">Do you measure your flock only with one sensor or more</Label>}
      <br />
      {
        useDevice
          ? (
            <React.Fragment>
              <Label type="text">Device</Label>
              <UncontrolledDropdown>
                <DropdownToggle disabled={disabled || isActive || isScheduled} color="primary" outline caret>
                  {device && device.name}
                  {device && inUse[device.id] && (<StatusIcon className="float-right" hasError />)}
                </DropdownToggle>
                <DropdownMenu>
                  {
                    sortString(sensors, s => s.name).map(d => (
                      <DropdownItem key={d.id} onClick={() => updateForm(d.id, 'deviceId')}>
                        {d.name}
                        {inUse[d.id] && <StatusIcon className="float-right" hasError />}
                      </DropdownItem>
                    ))
                  }
                </DropdownMenu>
              </UncontrolledDropdown>
              {!edit && !(device && inUse[device.id]) && <Label type="info">Select an existing sensor which measures your flock</Label>}
              {!edit && device && inUse[device.id] && <Label type="error">Selected sensor is already part of active flock</Label>}
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Label type="text">Select farm</Label>
              <UncontrolledDropdown>
                <DropdownToggle disabled={disabled || isActive || isScheduled} color="primary" outline caret>
                  {farm && farm.name}
                </DropdownToggle>
                <DropdownMenu>
                  {
                    sortString(farms, s => s.name).map(f => (
                      <DropdownItem
                        key={f.id}
                        onClick={() => updateFormObject({ farmId: f.id, houseId: null })}
                      >
                        {f.name}
                      </DropdownItem>
                    ))
                  }
                </DropdownMenu>
              </UncontrolledDropdown>
              {!edit && <Label type="info">Choose a farm where to place your flock</Label>}
              <br />
              <React.Fragment>
                <Label type="text">Select house</Label>
                <UncontrolledDropdown>
                  <DropdownToggle disabled={disabled || isActive || isScheduled || farm == null} color="primary" outline caret>
                    {house && house.name}
                    {house && inUse[house.id] && <StatusIcon className="float-right" hasError />}
                  </DropdownToggle>
                  <DropdownMenu>
                    {
                      sortString(houses, s => s.name)
                        .filter(f => (farm == null ? false : farm.id === f.farmId))
                        .map(h => (
                          <DropdownItem key={h.id} onClick={() => updateForm(h.id, 'houseId')}>
                            {h.name}
                            {inUse[h.id] && <StatusIcon className="float-right" hasError />}
                          </DropdownItem>
                        ))
                    }
                  </DropdownMenu>
                </UncontrolledDropdown>
                {!edit && !(house && inUse[house.id]) && <Label type="info">Choose where to place your flocks</Label>}
                {!edit && house && inUse[house.id] && (
                  <Label type="error">
                    Selected house or device(in selected house) is already part of active flock
                  </Label>
                )}
              </React.Fragment>
            </React.Fragment>
          )
      }
    </React.Fragment>
  );
};

Step3.propTypes = {
  modal: PropTypes.shape({
    data: PropTypes.object,
  }).isRequired,
  devices: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
  })).isRequired,
  houses: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
  })).isRequired,
  farms: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
  })).isRequired,
  flocks: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
  })).isRequired,
  updateForm: PropTypes.func.isRequired,
  updateFormObject: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

Step3.defaultProps = {
  disabled: false,
};

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
    devices: state.device.items,
    houses: state.house.items,
    farms: state.farm.items,
    flocks: state.flock.items,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  updateForm: ModalActions.updateForm,
  updateFormObject: ModalActions.updateFormObject,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Step3);
