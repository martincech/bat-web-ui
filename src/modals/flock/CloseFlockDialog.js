import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Label,
} from '@veit/veit-web-controls';

import ModalWithLoader from '../ModalWithLoader';
import bem from '../../utils/bem';
import SubmitHandler from '../SubmitHandler';

class CloseFlockDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };

    this.toggle = this.toggle.bind(this);
  }

  confirm = () => {
    this.props.confirm();
  }

  toggle = () => {
    this.setState(prev => ({
      modal: !prev.modal,
    }));
  }

  render() {
    const bm = bem.modal('close-flock');
    return (
      <React.Fragment>
        <Button outline color="primary" onClick={this.toggle}>Close Flock</Button>
        <div className={bm.b()}>
          <ModalWithLoader className={bm.e('modal')} keyboard={false} centered isOpen={this.state.modal} toggle={this.toggle}>
            <ModalHeader toggle={this.toggle}></ModalHeader>
            <ModalBody>
              <Label type="title">Are you sure?</Label>
              <br />
              <Label className="subtitle">You will close this flock permanently</Label>
            </ModalBody>
            <ModalFooter>
              <Button color="danger" outline onClick={this.toggle}>Don’t do it</Button>
              <Button color="danger" onClick={this.confirm}>Close Flock</Button>
            </ModalFooter>
            <SubmitHandler action={this.confirm} keyCode={13} />
            <SubmitHandler action={this.toggle} keyCode={27} />
          </ModalWithLoader>
        </div>
      </React.Fragment>
    );
  }
}

CloseFlockDialog.propTypes = {
  confirm: PropTypes.func.isRequired,
};

export default CloseFlockDialog;
