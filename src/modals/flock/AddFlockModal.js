import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import ModalActions, { finishDialogAddFlock } from '../../store/ModalFlock.actions';
import { getBird, getPublicBird, expandBird } from '../../store/Bird.actions';
import { getHouse } from '../../store/House.actions';
import { getFarm } from '../../store/Farm.actions';
import { getDevice } from '../../store/Device.actions';

import {
  ModalHeader,
  ModalBody,
} from '@veit/veit-web-controls';

import AnimateHeight from '../AnimateHeight';

import ModalWithLoader from '../ModalWithLoader';
import Steps from '../Steps';
import Footer from '../Footer';
import Step1, { hasStep1Error } from './Step1';
import Step2, { hasStep2Error } from './Step2';
import Step3, { hasStep3Error } from './Step3';

class AddFlockModal extends Component {
  steps = 3;

  componentDidMount() {
    this.fetchData();
  }

  getStep() {
    switch (this.props.modal.step) {
      case 2:
        return <Step2 />;
      case 3:
        return <Step3 />;
      default:
        return <Step1 />;
    }
  }

  fetchData() {
    this.props.setValidation({
      1: hasStep1Error,
      2: hasStep2Error,
      3: hasStep3Error,
    });
    this.props.getBird(expandBird.duration);
    this.props.getPublicBird(expandBird.duration);
    this.props.getDevice();
    this.props.getHouse();
    this.props.getFarm();
  }

  render() {
    const { step } = this.props.modal;
    return (
      <ModalWithLoader
        centered
        isOpen
        keyboard={false}
        toggle={this.props.cancelDialog}
        contentClassName="modal-medium"
      >
        <ModalHeader toggle={this.props.cancelDialog}>Add Flock</ModalHeader>
        <div>
          <Steps steps={this.steps} actualStep={step} />
        </div>
        <ModalBody>
          <AnimateHeight step={step}>
            {this.getStep()}
          </AnimateHeight>
        </ModalBody>
        <Footer
          step={step}
          maxStep={this.steps}
          finisDialog={() => this.props.finishDialogAddFlock(this.props.modal.data)}
          finisDialogText="Add Flock"
        />
      </ModalWithLoader>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  setValidation: ModalActions.setValidation,
  finishDialogAddFlock,
  getBird,
  getPublicBird,
  getHouse,
  getFarm,
  getDevice,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

AddFlockModal.propTypes = {
  cancelDialog: PropTypes.func.isRequired,
  setValidation: PropTypes.func.isRequired,
  finishDialogAddFlock: PropTypes.func.isRequired,
  getBird: PropTypes.func.isRequired,
  getPublicBird: PropTypes.func.isRequired,
  getHouse: PropTypes.func.isRequired,
  getDevice: PropTypes.func.isRequired,
  getFarm: PropTypes.func.isRequired,
  modal: PropTypes.shape({
    type: PropTypes.string,
    target: PropTypes.string,
    step: PropTypes.number,
    data: PropTypes.object,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddFlockModal);
