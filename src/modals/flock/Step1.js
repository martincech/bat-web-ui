import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ModalActions from '../../store/ModalFlock.actions';
import { isActiveFlock, isScheduledFlock } from '../../utils/flock';
import { sortString } from '../../utils/sort';

import {
  Label,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
} from '@veit/veit-web-controls';

export function hasStep1Error({ birdId }) {
  return {
    birdId: birdId == null,
  };
}

const Step1 = ({
  modal, bird, updateFormObject, disabled,
}) => {
  const form = modal.data;
  if (form == null) return null;
  const birds = [...bird.items, ...bird.public];
  const companies = birds.map(b => b.company).filter((v, i, a) => a.indexOf(v) === i);

  const selectedBird = birds.find(f => f.id === form.birdId);
  const product = selectedBird ? selectedBird.product : form.product;
  const company = selectedBird ? selectedBird.company : form.company;

  const companyProducts = company
    ? birds.filter(p => p.company === company)
      .map(b => b.product).filter((v, i, a) => a.indexOf(v) === i)
    : [];
  const productBirds = product ? birds.filter(b => b.product === product) : [];

  const edit = form.id != null;
  const isActive = edit && isActiveFlock(form);
  const isScheduled = edit && isScheduledFlock(form);

  return (
    <React.Fragment>
      <Label type="text">Company</Label>
      <UncontrolledDropdown>
        <DropdownToggle color="primary" outline caret disabled={disabled || isActive || isScheduled}>
          {company}
        </DropdownToggle>
        <DropdownMenu>
          {
            sortString(companies, s => s).map(c => (
              <DropdownItem
                onClick={() => updateFormObject({
                  company: c,
                  product: null,
                  birdId: null,
                })}
                key={c}
              >
                {c}
              </DropdownItem>
            ))
          }
        </DropdownMenu>
      </UncontrolledDropdown>
      {!edit && <Label type="info">From which company is your new flock</Label>}
      <br />
      <Label type="text">Product</Label>
      <UncontrolledDropdown>
        <DropdownToggle color="primary" outline caret disabled={disabled || isActive || isScheduled || company == null}>
          {product}
        </DropdownToggle>
        <DropdownMenu>
          {sortString(companyProducts, s => s).map(p => (
            <DropdownItem
              onClick={() => updateFormObject({ product: p, birdId: null })}
              key={p}
            >
              {p}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </UncontrolledDropdown>
      {!edit && <Label type="info">Which product line of the comapy</Label>}
      <br />
      <Label type="text">Bird</Label>
      <UncontrolledDropdown>
        <DropdownToggle color="primary" outline caret disabled={disabled || isActive || isScheduled || product == null}>
          {selectedBird && selectedBird.name}
        </DropdownToggle>
        <DropdownMenu>
          {sortString(productBirds, s => s.name).map(b => (
            <DropdownItem
              onClick={() => updateFormObject({ birdId: b.id, targetAge: b.duration })}
              key={b.id}
            >
              {b.name}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </UncontrolledDropdown>
      {!edit && <Label type="info">Which bird you have</Label>}
    </React.Fragment>
  );
};

Step1.propTypes = {
  modal: PropTypes.shape({
    data: PropTypes.object,
  }).isRequired,
  bird: PropTypes.shape({
    items: PropTypes.array,
  }).isRequired,
  updateForm: PropTypes.func.isRequired,
  updateFormObject: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

Step1.defaultProps = {
  disabled: false,
};

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
    bird: state.bird,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  updateForm: ModalActions.updateForm,
  updateFormObject: ModalActions.updateFormObject,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Step1);
