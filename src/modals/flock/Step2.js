import React from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import {
  Input,
  Label,
  InputGroup,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  ButtonSwitch,
  Container,
  Row,
  Col,
  CalendarPopover,
} from '@veit/veit-web-controls';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import WeightInput, { weightUnits } from '../../components/WeightInput';
import ModalActions from '../../store/ModalFlock.actions';
import { isActiveFlock } from '../../utils/flock';
import formatDate from '../../utils/format';

export function hasStep2Error({
  name, initialAge, targetAge, targetWeight,
}) {
  return {
    name: name == null || !validator.isLength(validator.trim(name), { min: 1 }),
    initialAge: initialAge == null || !validator.isInt(`${initialAge}`, { min: 0 }),
    target: (targetAge == null || !validator.isInt(`${targetAge}`, { min: 0 }))
      && (targetWeight == null || !validator.isFloat(`${targetWeight}`, { min: 0 })),
  };
}

const Step2 = ({
  updateForm, updateFormEvent, updateFormObject, modal, disabled,
}) => {
  const form = modal.data;
  if (form == null) return null;
  const errors = (modal.errors != null ? modal.errors[2] : null) || {};
  const edit = form.id != null;
  const isActive = edit && isActiveFlock(modal.base);
  return (
    <React.Fragment>
      {!edit && (
        <React.Fragment>
          <Label type="text">Measure goal</Label>
          <ButtonSwitch
            disabled={disabled}
            options={[{ title: 'Ideal growth', value: false }, { title: 'Final weight', value: true }]}
            selected={form.useFinalWeight === true}
            onChange={v => updateFormObject({
              useFinalWeight: v,
              targetAge: v ? null : form.targetAge,
              targetWeight: v ? form.targetWeight : null,
            })}
          />
          <br />
        </React.Fragment>
      )}
      <Label type="text">Flock name/batch</Label>
      <Input disabled={disabled} value={form.name || ''} onChange={e => updateFormEvent(e, 'name')} />
      {!edit && !errors.name && <Label tag="span" type="info">Name your new batch</Label>}
      {errors.name && <Label tag="span" type="error">Flock name is required</Label>}
      <br />
      <Container style={{ padding: 0 }}>
        <Row>
          <Col>
            <Label type="text">Initial age</Label>
            <InputGroup>
              <Input
                min={0}
                disabled={disabled || isActive}
                type="number"
                value={form.initialAge}
                onChange={e => updateFormEvent(e, 'initialAge')}
              />
              <UncontrolledDropdown addonType="append">
                <DropdownToggle disabled={disabled || isActive} caret color="primary" outline className="form-control">
                  {form.dayUnit || 'days'}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={() => updateForm('weeks', 'dayUnit')}>weeks</DropdownItem>
                  <DropdownItem onClick={() => updateForm('days', 'dayUnit')}>days</DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </InputGroup>
            {!edit && !errors.initialAge && <Label type="info">How old is your flock</Label>}
            {errors.initialAge && <Label type="error">Minimal value is 0</Label>}
          </Col>
          <Col>
            <Label type="text">Initial weight</Label>
            <InputGroup>
              <WeightInput
                min={0}
                disabled={disabled || isActive}
                type="number"
                value={form.initialWeight}
                weightUnit={form.weightUnit}
                onChange={e => updateFormEvent(e, 'initialWeight')}
              />
              <UncontrolledDropdown addonType="append">
                <DropdownToggle disabled={disabled || isActive} caret color="primary" outline className="form-control">
                  {form.weightUnit || 'grams'}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={() => updateForm(weightUnits.g, 'weightUnit')}>grams</DropdownItem>
                  <DropdownItem onClick={() => updateForm(weightUnits.kg, 'weightUnit')}>kilograms</DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </InputGroup>
            {!edit && <Label type="info">How much your flock weigh</Label>}
          </Col>
        </Row>
        <Row>
          <Col>
            <Label type="text">Start date</Label>
            <CalendarPopover value={new Date(form.startDate)} disabled={disabled || isActive} onChange={date => updateForm(date, 'startDate')}>
              <Input
                readOnly
                disabled={disabled || isActive}
                type="date"
                value={formatDate(form.startDate || new Date(), 'YYYY-MM-DD')}
                onChange={e => updateFormEvent(e, 'startDate')}
              />
            </CalendarPopover>
            {!edit && <Label type="info">Date from which you start measure your flock</Label>}
          </Col>
          {
            form.useFinalWeight === true || form.targetWeight != null
              ? (
                <Col>
                  <Label type="text">Final weight</Label>
                  <InputGroup>
                    <WeightInput
                      min={0}
                      disabled={disabled || isActive}
                      type="number"
                      value={form.targetWeight || ''}
                      weightUnit={form.targetWeightUnit}
                      onChange={e => updateFormEvent(e, 'targetWeight')}
                    />
                    <UncontrolledDropdown addonType="append">
                      <DropdownToggle disabled={disabled || isActive} caret color="primary" outline className="form-control">
                        {form.targetWeightUnit || 'grams'}
                      </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem onClick={() => updateForm(weightUnits.g, 'targetWeightUnit')}>grams</DropdownItem>
                        <DropdownItem onClick={() => updateForm(weightUnits.kg, 'targetWeightUnit')}>kilograms</DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </InputGroup>
                  {!edit && <Label type="info">Set a target weight</Label>}
                </Col>
              ) : (
                <Col>
                  <Label type="text">Grow period</Label>
                  <InputGroup>
                    <Input min={0} disabled={disabled || isActive} type="number" value={form.targetAge || ''} onChange={e => updateFormEvent(e, 'targetAge')} />
                    <UncontrolledDropdown addonType="append">
                      <DropdownToggle disabled={disabled || isActive} caret color="primary" outline className="form-control">
                        {form.targetAgeUnit || 'days'}
                      </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem onClick={() => updateForm('weeks', 'targetAgeUnit')}>weeks</DropdownItem>
                        <DropdownItem onClick={() => updateForm('days', 'targetAgeUnit')}>days</DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </InputGroup>
                  {!edit && <Label type="info">How long to growth your flock</Label>}
                </Col>
              )
          }
        </Row>
      </Container>
    </React.Fragment>
  );
};

Step2.propTypes = {
  modal: PropTypes.shape({
    data: PropTypes.object,
  }).isRequired,
  updateForm: PropTypes.func.isRequired,
  updateFormEvent: PropTypes.func.isRequired,
  updateFormObject: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

Step2.defaultProps = {
  disabled: false,
};

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  updateForm: ModalActions.updateForm,
  updateFormEvent: ModalActions.updateFormEvent,
  updateFormObject: ModalActions.updateFormObject,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Step2);
