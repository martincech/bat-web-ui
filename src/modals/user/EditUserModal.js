import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ModalActions, { finishDialogEditUser, deleteDialogUser, resetPasswordDialogUser } from '../../store/ModalUser.actions';

import {
  ModalHeader,
  ModalBody,
  Button,
} from '@veit/veit-web-controls';

import ModalWithLoader from '../ModalWithLoader';
import Footer from '../Footer';
import ModalContent, { hasModalContentError } from './ModalContent';

class EditUserModal extends Component {
  componentDidMount() {
    this.props.setValidation({
      1: hasModalContentError,
    });
  }

  cancel = () => {
    this.props.cancelDialog();
  }

  finish = () => {
    this.props.finishDialogEditUser(this.props.modal.data);
  }

  delete = () => {
    this.props.deleteDialogUser(this.props.modal.data.id);
  }

  resetPassword = () => {
    this.props.resetPasswordDialogUser(this.props.modal.data);
  }

  render() {
    const { email } = this.props.modal.data;
    return (
      <ModalWithLoader
        centered
        isOpen
        keyboard={false}
        toggle={this.cancel}
      >
        <ModalHeader toggle={this.cancel}>Edit User</ModalHeader>
        <ModalBody>
          <ModalContent edit />
          <Button color="primary" style={{ margin: 'unset', width: '100%' }} disabled={email == null} onClick={this.resetPassword}>
            Reset Password
          </Button>
        </ModalBody>
        <Footer finisDialog={this.finish} deleteDialog={{ action: this.delete, name: 'User' }} />
      </ModalWithLoader>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  setValidation: ModalActions.setValidation,
  finishDialogEditUser,
  deleteDialogUser,
  resetPasswordDialogUser,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

EditUserModal.propTypes = {
  cancelDialog: PropTypes.func.isRequired,
  setValidation: PropTypes.func.isRequired,
  finishDialogEditUser: PropTypes.func.isRequired,
  deleteDialogUser: PropTypes.func.isRequired,
  resetPasswordDialogUser: PropTypes.func.isRequired,
  modal: PropTypes.shape({
    type: PropTypes.string,
    target: PropTypes.string,
    data: PropTypes.object,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditUserModal);
