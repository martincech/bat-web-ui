import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import isEmail from 'validator/lib/isEmail';
import * as ModalActions from '../../store/Modal.actions';
import {
  Input,
  Label,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
} from '@veit/veit-web-controls';

import { roleType, roleTypeName } from '../../utils/enums';

export function hasModalContentError({ email }) {
  return {
    email: email == null || !isEmail(email),
  };
}

function getRoles(user) {
  if (user.role === roleType.companyAdmin) {
    return [roleType.undefined, roleType.companyAdmin];
  }
  const isAdmin = roleType.isAdmin(user.role);
  return Object.keys(roleTypeName).filter(f => isAdmin || f === roleType.undefined);
}

const ModalContent = ({
  modal, updateFormEvent, updateForm, edit, user,
}) => {
  const form = modal.data;
  if (form == null) return null;
  const roles = getRoles(user);
  return (
    <React.Fragment>
      <br />
      <Label type="text">Email</Label>
      <Input disabled={edit} value={form.email || ''} onChange={e => updateFormEvent(e, 'email')} />
      <br />
      <Label type="text">Name</Label>
      <Input value={form.name || ''} onChange={e => updateFormEvent(e, 'name')} />
      <br />
      <Label type="text">Role</Label>
      <UncontrolledDropdown>
        <DropdownToggle color="primary" outline caret>
          {roleTypeName[form.role]}
        </DropdownToggle>
        <DropdownMenu>
          {
            roles.map(r => (
              <DropdownItem
                disabled={r === roleType.veitOperator}
                onClick={() => updateForm(r, 'role')}
                key={r}
              >
                {roleTypeName[r]}
              </DropdownItem>
            ))
          }
        </DropdownMenu>
      </UncontrolledDropdown>
      <br />
    </React.Fragment>
  );
};

ModalContent.propTypes = {
  modal: PropTypes.shape({
    data: PropTypes.object,
  }).isRequired,
  user: PropTypes.shape({
    role: PropTypes.string,
  }).isRequired,
  updateFormEvent: PropTypes.func.isRequired,
  updateForm: PropTypes.func.isRequired,
  edit: PropTypes.bool,
};

ModalContent.defaultProps = {
  edit: false,
};

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
    user: state.auth.user,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  updateFormEvent: ModalActions.updateFormEvent,
  updateForm: ModalActions.updateForm,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(ModalContent);
