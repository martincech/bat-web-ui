import AddBirdModal from './bird/AddBirdModal';
import EditBirdModal from './bird/EditBirdModal';

import EditComapnyModal from './company/EditCompanyModal';

import AddDeviceModal from './device/AddDeviceModal';
import EditDeviceModal from './device/EditDeviceModal';

import AddFarmModal from './farm/AddFarmModal';
import EditFarmModal from './farm/EditFarmModal';

import FeedbackModal from './feedback/FeedbackModal';
import FeatureRequestModal from './feature/FeatureRequestModal';

import AddFlockModal from './flock/AddFlockModal';
import EditFlockModal from './flock/EditFlockModal';

import AddHouseModal from './house/AddHouseModal';
import EditHouseModal from './house/EditHouseModal';

import InviteUserModal from './user/InviteUserModal';
import EditUserModal from './user/EditUserModal';

import EditProfileModal from './profile/EditProfileModal';

import RegisterUserModal from './register/RegisterUserModal';

import AddHelpPageModal from './help/AddHelpPageModal';
import EditHelpPageModal from './help/EditHelpPageModal';

import GatewayRequestModal from './gateway/GatewayRequestModal';

export default {
  AddBirdModal,
  EditBirdModal,
  EditComapnyModal,
  AddDeviceModal,
  EditDeviceModal,
  AddFarmModal,
  EditFarmModal,
  FeedbackModal,
  FeatureRequestModal,
  AddFlockModal,
  EditFlockModal,
  AddHouseModal,
  EditHouseModal,
  EditProfileModal,
  InviteUserModal,
  EditUserModal,
  RegisterUserModal,
  AddHelpPageModal,
  EditHelpPageModal,
  GatewayRequestModal,
};
