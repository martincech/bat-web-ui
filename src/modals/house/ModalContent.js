import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import validator from 'validator';
import * as ModalActions from '../../store/Modal.actions';
import {
  Input,
  Label,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from '@veit/veit-web-controls';

export function hasModalContentError({ name, farmId }) {
  return {
    name: name == null || !validator.isLength(validator.trim(name), { min: 1 }),
    farmId: farmId == null || !validator.isLength(validator.trim(farmId), { min: 32 }),
  };
}

const ModalContent = ({
  modal, farms, updateFormEvent, updateForm,
}) => {
  const form = modal.data;
  if (form == null) return null;
  const farm = (farms || []).find(f => f.id === form.farmId);
  return (
    <React.Fragment>
      <br />
      <Label type="text">Select farm</Label>
      <UncontrolledDropdown>
        <DropdownToggle color="primary" outline caret>
          {farm && farm.name}
        </DropdownToggle>
        <DropdownMenu>
          {farms.map(p => (
            <DropdownItem
              onClick={() => updateForm(p.id, 'farmId')}
              key={p.id}
            >
              {p.name}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </UncontrolledDropdown>
      <br />
      <Label type="text">House name</Label>
      <Input value={form.name || ''} onChange={e => updateFormEvent(e, 'name')} />
      <br />
    </React.Fragment>
  );
};

ModalContent.propTypes = {
  modal: PropTypes.shape({
    data: PropTypes.object,
  }).isRequired,
  farms: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
    }),
  ).isRequired,
  updateForm: PropTypes.func.isRequired,
  updateFormEvent: PropTypes.func.isRequired,
};


const mapStateToProps = (state) => {
  return {
    modal: state.modal,
    farms: state.farm.items,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  updateForm: ModalActions.updateForm,
  updateFormEvent: ModalActions.updateFormEvent,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(ModalContent);
