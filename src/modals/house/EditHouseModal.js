import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ModalActions, { finishDialogEditHouse, deleteDialogHouse } from '../../store/ModalHouse.actions';

import {
  ModalHeader,
  ModalBody,
} from '@veit/veit-web-controls';

import ModalWithLoader from '../ModalWithLoader';
import Footer from '../Footer';
import ModalContent, { hasModalContentError } from './ModalContent';

class EditHouseModal extends Component {
  componentDidMount() {
    this.props.setValidation({
      1: hasModalContentError,
    });
  }

  cancel = () => {
    this.props.cancelDialog();
  }

  finish = () => {
    this.props.finishDialogEditHouse(this.props.modal.data);
  }

  delete = () => {
    this.props.deleteDialogHouse(this.props.modal.data.id);
  }

  render() {
    return (
      <ModalWithLoader
        centered
        isOpen
        keyboard={false}
        toggle={this.cancel}
      >
        <ModalHeader toggle={this.cancel}>Edit House</ModalHeader>
        <ModalBody>
          <ModalContent />
        </ModalBody>
        <Footer finisDialog={this.finish} deleteDialog={{ action: this.delete, name: 'House' }} />
      </ModalWithLoader>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  setValidation: ModalActions.setValidation,
  finishDialogEditHouse,
  deleteDialogHouse,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

EditHouseModal.propTypes = {
  cancelDialog: PropTypes.func.isRequired,
  setValidation: PropTypes.func.isRequired,
  finishDialogEditHouse: PropTypes.func.isRequired,
  deleteDialogHouse: PropTypes.func.isRequired,
  modal: PropTypes.shape({
    type: PropTypes.string,
    target: PropTypes.string,
    data: PropTypes.object,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditHouseModal);
