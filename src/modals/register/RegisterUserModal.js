import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import {
  ModalHeader,
  ModalBody,
  Button,
  Label,
  Input,
} from '@veit/veit-web-controls';

import ModalActions, { finishDialogRegisterUser } from '../../store/ModalUser.actions';
import SubmitHandler from '../SubmitHandler';
import AnimateHeight from '../AnimateHeight';
import ModalWithLoader from '../ModalWithLoader';
import RegistrResult from './RegisterResult';

class RegisterUserModal extends Component {
  state = {
    email: '',
    name: '',
    company: '',
    code: null,
  }

  updateFormEvent = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  setResult = (response) => {
    this.setState({ code: response.status || 500 });
  }

  finish = () => {
    this.props.finishDialogRegisterUser({
      email: this.state.email,
      name: this.state.name,
      company: this.state.company,
    })
      .then(this.setResult)
      .catch(this.setResult);
  }

  cancel = () => {
    this.props.cancelDialog();
  }

  isValidForm = () => {
    const reg = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return reg.test(this.state.email);
  }

  render() {
    return (
      <ModalWithLoader
        centered
        isOpen
        toggle={this.cancel}
      >
        <ModalHeader toggle={this.cancel}>
          {
            this.state.code == null
              ? 'Create account'
              : this.state.code === 201
                ? 'Your account has been created'
                : 'Failed to create account'
          }
        </ModalHeader>
        <ModalBody>
          <AnimateHeight step={this.state.code == null}>
            {this.state.code == null
              ? (
                <React.Fragment>
                  <center>
                    <Label>To join existing company is possible only by invitation.</Label>
                  </center>
                  <br />
                  <Label>Email</Label>
                  <Input type="text" name="email" value={this.state.email} onChange={this.updateFormEvent} />
                  <Label>Name</Label>
                  <Input type="text" name="name" value={this.state.name} onChange={this.updateFormEvent} />
                  <Label>Company name</Label>
                  <Input type="text" name="company" value={this.state.company} onChange={this.updateFormEvent} />
                  <br />
                  <Button disabled={!this.isValidForm()} color="primary" style={{ width: '100%', marginBottom: '40px' }} onClick={this.finish}>
                    Sign up
                  </Button>
                  <SubmitHandler keyCode={13} action={this.finish} />
                </React.Fragment>
              ) : (
                <RegistrResult
                  code={this.state.code}
                  email={this.state.email}
                  close={this.cancel}
                />
              )}
          </AnimateHeight>
        </ModalBody>
      </ModalWithLoader>
    );
  }
}

RegisterUserModal.propTypes = {
  cancelDialog: PropTypes.func.isRequired,
  finishDialogRegisterUser: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  finishDialogRegisterUser,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterUserModal);
