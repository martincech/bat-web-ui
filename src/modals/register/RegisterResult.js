import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Label,
} from '@veit/veit-web-controls';

const RegisterResultTemplate = ({ children, close }) => (
  <React.Fragment>
    <center>
      <Label>
        {children}
      </Label>
    </center>
    <br />
    <Button color="primary" style={{ width: '100%', marginBottom: '40px' }} onClick={close}>
      Ok
    </Button>
  </React.Fragment>
);

RegisterResultTemplate.propTypes = {
  children: PropTypes.node.isRequired,
  close: PropTypes.func.isRequired,
};

const RegisterResult = ({ code, email, close }) => {
  switch (code) {
    case 201:
      return (
        <RegisterResultTemplate close={close}>
          Mail with temporary password has been sent to your email
          {' '}
          <ins>{email}</ins>
          .
        </RegisterResultTemplate>
      );
    case 412:
      return (
        <RegisterResultTemplate close={close}>
          User with email address
          {' '}
          <ins>{email}</ins>
          {' '}
          already exist!
        </RegisterResultTemplate>
      );
    default:
      return (
        <RegisterResultTemplate close={close}>
          An unexpected error occurred during registration!
        </RegisterResultTemplate>
      );
  }
};

RegisterResult.propTypes = {
  code: PropTypes.number.isRequired,
  close: PropTypes.func.isRequired,
  email: PropTypes.string,
};

RegisterResult.defaultProps = {
  email: null,
};

export default RegisterResult;
