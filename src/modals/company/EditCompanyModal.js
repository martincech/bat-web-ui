import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import {
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Label,
  Input,
} from '@veit/veit-web-controls';

import ModalActions, { finishDialogEditCompany } from '../../store/ModalCompany.actions';
import { getCurrentCompany } from '../../store/Company.actions';
import SubmitHandler from '../SubmitHandler';

import ModalWithLoader from '../ModalWithLoader';

class EditCompanyModal extends Component {
  cancel = () => {
    this.props.cancelDialog();
  }

  finish = () => {
    this.props.finishDialogEditCompany(this.props.modal.data);
  }

  render() {
    const form = this.props.modal.data;
    if (form == null) return null;
    const { updateFormEvent } = this.props;
    return (
      <ModalWithLoader
        centered
        isOpen
        toggle={this.cancel}
      >
        <ModalHeader toggle={this.cancel}>Edit Company</ModalHeader>
        <ModalBody style={{ maxWidth: 'unset', padding: '32px 0' }}>
          <div style={{ display: 'flex' }}>
            <div style={{ flex: 1, padding: '0px 46px' }}>
              <Label type="text">Company name</Label>
              <Input type="text" value={form.name || ''} onChange={e => updateFormEvent(e, 'name')} />
              <br />
              <Label type="text">VAT Number</Label>
              <Input type="text" value={form.vat || ''} onChange={e => updateFormEvent(e, 'vat')} />
              <br />
              <Label type="text">Headquerters address</Label>
              <Input type="text" value={form.address || ''} onChange={e => updateFormEvent(e, 'address')} />
              <br />
              <div style={{ display: 'inline-flex' }}>
                <div style={{ marginRight: '10px' }}>
                  <Label type="text">Zip code</Label>
                  <Input type="text" value={form.zip || ''} onChange={e => updateFormEvent(e, 'zip')} />
                </div>
                <div style={{ marginLeft: '10px' }}>
                  <Label type="text">City</Label>
                  <Input type="text" value={form.city || ''} onChange={e => updateFormEvent(e, 'city')} />
                </div>
              </div>
              <br />
              <Label type="text">Country</Label>
              <Input type="text" value={form.country || ''} onChange={e => updateFormEvent(e, 'country')} />
              <br />
            </div>
            <div style={{ flex: 1, padding: '0px 46px' }}>
              <Label type="text">Contact name</Label>
              <Input type="text" value={form.contactName || ''} onChange={e => updateFormEvent(e, 'contactName')} />
              <br />
              <Label type="text">Telephone</Label>
              <Input type="text" value={form.phone || ''} onChange={e => updateFormEvent(e, 'phone')} />
              <br />
              <Label type="text">Email</Label>
              <Input type="text" value={form.email || ''} onChange={e => updateFormEvent(e, 'email')} />
            </div>
          </div>
        </ModalBody>
        <ModalFooter style={{ maxWidth: 'unset', padding: '15px 46px' }}>
          <Button color="primary" onClick={this.finish}>
            Save changes
          </Button>
          <div style={{ flex: '1' }}></div>
          <Button color="primary" outline onClick={this.cancel}>
            Cancel
          </Button>
        </ModalFooter>
        <SubmitHandler keyCode={13} action={this.finish} />
      </ModalWithLoader>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  updateFormEvent: ModalActions.updateFormEvent,
  updateFormObject: ModalActions.updateFormObject,
  finishDialogEditCompany,
  getCurrentCompany,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

EditCompanyModal.propTypes = {
  cancelDialog: PropTypes.func.isRequired,
  updateFormEvent: PropTypes.func.isRequired,
  updateFormObject: PropTypes.func.isRequired,
  finishDialogEditCompany: PropTypes.func.isRequired,
  getCurrentCompany: PropTypes.func.isRequired,
  modal: PropTypes.shape({
    type: PropTypes.string,
    target: PropTypes.string,
    data: PropTypes.object,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditCompanyModal);
