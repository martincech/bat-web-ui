import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from '../utils/bem';
import {
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Label,
  Input,
} from '@veit/veit-web-controls';

import ModalWithLoader from './ModalWithLoader';
import SubmitHandler from './SubmitHandler';

class DeleteModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      text: '',
    };

    this.toggle = this.toggle.bind(this);
  }

  canDelete = () => {
    if (!this.props.advaced) return true;
    return this.state.text === 'delete';
  }

  confirm = () => {
    if (this.canDelete()) this.props.confirm();
  }

  toggle = () => {
    this.setState(prev => ({
      modal: !prev.modal,
      text: '',
    }));
  }

  render() {
    const bm = bem.modal('delete');

    return (
      <React.Fragment>
        <Button className={this.props.narrow ? 'btn-narrow' : ''} color="primary" outline onClick={this.toggle}>
          Delete&nbsp;
          {this.props.name}
        </Button>
        <div className={bm.b()}>
          <ModalWithLoader keyboard={false} className={bm.e('modal')} centered isOpen={this.state.modal} toggle={this.toggle}>
            <ModalHeader toggle={this.toggle}></ModalHeader>
            <ModalBody>
              <Label type="title">Are you sure?</Label>
              <br />
              <Label className="subtitle">{`You will delete ${this.props.name.toLowerCase()} permanently`}</Label>
              {
                this.props.advaced && (
                  <React.Fragment>
                    <br />
                    <Input
                      value={this.state.text}
                      onChange={e => this.setState({ text: e.target.value })}
                    />
                    <Label className="text">Write &quot;delete&quot; to enable delete button</Label>
                  </React.Fragment>
                )
              }
            </ModalBody>
            <ModalFooter>
              <Button color="danger" outline onClick={this.toggle}>Don’t do it</Button>
              <Button color="danger" onClick={this.confirm} disabled={!this.canDelete()}>
                Delete&nbsp;
                {this.props.name}
              </Button>
            </ModalFooter>
            <SubmitHandler keyCode={13} action={this.confirm} />
            <SubmitHandler keyCode={27} action={this.toggle} />
          </ModalWithLoader>
        </div>
      </React.Fragment>
    );
  }
}

DeleteModal.propTypes = {
  confirm: PropTypes.func.isRequired,
  name: PropTypes.string,
  advaced: PropTypes.bool,
  narrow: PropTypes.bool,
};

DeleteModal.defaultProps = {
  name: 'Flock',
  advaced: false,
  narrow: false,
};

export default DeleteModal;
