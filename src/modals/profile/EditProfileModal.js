import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import {
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Label,
  Input,
  Container,
  Row,
  Col,
} from '@veit/veit-web-controls';
import Avatar from 'react-avatar';

import ModalWithLoader from '../ModalWithLoader';
import ModalActions, { finishDialogEditProfile } from '../../store/ModalProfile.actions';
import SubmitHandler from '../SubmitHandler';

class EditProfileModal extends Component {
  componentDidMount() {
    this.props.updateFormObject(this.props.user);
  }

  onChangeFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e) => {
        this.props.updateForm(e.target.result, 'image');
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  cancel = () => {
    this.props.cancelDialog();
  }

  finish = () => {
    this.props.finishDialogEditProfile(this.props.modal.data);
  }

  render() {
    const form = this.props.modal.data;
    if (form == null) return null;
    const { updateFormEvent } = this.props;
    return (
      <ModalWithLoader
        centered
        isOpen
        toggle={this.cancel}
      >
        <ModalHeader toggle={this.cancel}>Edit Profile</ModalHeader>
        <ModalBody>
          <Container>
            <Row>
              <Col
                xs="5"
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  flexDirection: 'column',
                }}
              >
                {
                  form.image && (
                    <Button style={{ marginBottom: '10px' }} outline onClick={() => this.props.updateForm(null, 'image')} className="btn-narrow" color="primary">
                      Remove
                    </Button>
                  )
                }
                <Avatar
                  style={{ cursor: 'pointer' }}
                  color="#469acf"
                  // onClick={() => this.upload.click()}
                  size="80"
                  round
                  textSizeRatio={1.75}
                  name={form.name || 'A'}
                  src={form.image}
                />
                <input
                  type="file"
                  accept="image/*"
                  ref={(ref) => { this.upload = ref; }}
                  style={{ display: 'none' }}
                  onChange={this.onChangeFile.bind(this)}
                />
              </Col>
              <Col xs="7">
                <Label type="text">Email</Label>
                <Input type="text" disabled defaultValue={form.email || ''} />
                <br />
                <Label type="text">Name</Label>
                <Input type="text" value={form.name || ''} onChange={e => updateFormEvent(e, 'name')} />
              </Col>
            </Row>
          </Container>
          <br />
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.finish}>
            Save changes
          </Button>
          <div style={{ flex: '1' }}></div>
          <Button color="primary" outline onClick={this.cancel}>
            Cancel
          </Button>
        </ModalFooter>
        <SubmitHandler action={this.finish} keyCode={13} />
      </ModalWithLoader>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  updateForm: ModalActions.updateForm,
  updateFormEvent: ModalActions.updateFormEvent,
  updateFormObject: ModalActions.updateFormObject,
  finishDialogEditProfile,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
    user: state.auth.user,
  };
};

EditProfileModal.propTypes = {
  cancelDialog: PropTypes.func.isRequired,
  updateForm: PropTypes.func.isRequired,
  updateFormEvent: PropTypes.func.isRequired,
  updateFormObject: PropTypes.func.isRequired,
  finishDialogEditProfile: PropTypes.func.isRequired,
  user: PropTypes.shape({
    name: PropTypes.string,
    email: PropTypes.string,
  }).isRequired,
  modal: PropTypes.shape({
    type: PropTypes.string,
    target: PropTypes.string,
    data: PropTypes.object,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfileModal);
