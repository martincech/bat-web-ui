import { Component } from 'react';
import PropTypes from 'prop-types';
import { registerEvent, unregisterEvent } from '../utils/keyEvents';

class SubmitHandler extends Component {
  componentDidMount() {
    registerEvent(this.props.keyCode, this.keyDownHandler);
  }

  componentWillUnmount() {
    unregisterEvent(this.props.keyCode, this.keyDownHandler);
  }

  keyDownHandler = () => {
    const { disabled, action } = this.props;
    if (!disabled && action) {
      action();
    }
  }

  render() {
    return null;
  }
}

SubmitHandler.propTypes = {
  action: PropTypes.func.isRequired,
  keyCode: PropTypes.number.isRequired,
  disabled: PropTypes.bool,
};

SubmitHandler.defaultProps = {
  disabled: false,
};

export default SubmitHandler;
