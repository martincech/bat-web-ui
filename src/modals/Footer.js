import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  ModalFooter,
  Button,
  Container,
  Row,
  Col,
} from '@veit/veit-web-controls';

import DeleteModal from './DeleteModal';
import SubmitHandler from './SubmitHandler';
import * as ModalActions from '../store/Modal.actions';

class Footer extends Component {
  keyDownHandler = () => {
    const { step, maxStep } = this.props;
    if (step < maxStep) {
      this.props.setStep(step + 1);
    } else {
      this.props.finisDialog();
    }
  }

  render() {
    const {
      step, maxStep, finisDialog, finisDialogText, setStep, cancelDialog, deleteDialog,
      errors, children,
    } = this.props;
    const hasError = errors != null && errors[step] != null && errors[step].hasError === true;
    return (
      <ModalFooter>
        <Container>
          <Row>
            {
              (deleteDialog || children) && (
                <Col>
                  {deleteDialog
                    && <DeleteModal name={deleteDialog.name} confirm={deleteDialog.action} />}
                  {/* <div style={{ flex: '1' }}></div> */}
                  {children}
                </Col>
              )
            }
            <Col>
              {
                step > 1
                  ? (
                    <Button outline color="primary" onClick={() => setStep(step - 1)}>
                      Back
                    </Button>
                  ) : (
                    <Button outline color="primary" onClick={cancelDialog}>
                      Cancel
                    </Button>
                  )
              }
              {(!deleteDialog && !children) && <div style={{ flex: '1', minWidth: '15px' }}></div>}
              {
                step < maxStep
                  ? (
                    <Button disabled={hasError} color="primary" onClick={() => setStep(step + 1)}>
                      Continue
                    </Button>
                  ) : (
                    <Button disabled={hasError} color="primary" onClick={finisDialog}>
                      {finisDialogText}
                    </Button>
                  )
              }
            </Col>
          </Row>
        </Container>
        <SubmitHandler keyCode={13} action={this.keyDownHandler} disabled={hasError} />
        <SubmitHandler keyCode={27} action={cancelDialog} />
      </ModalFooter>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  setStep: ModalActions.setStep,
}, dispatch);

Footer.propTypes = {
  step: PropTypes.number,
  maxStep: PropTypes.number,
  finisDialog: PropTypes.func.isRequired,
  finisDialogText: PropTypes.string,
  setStep: PropTypes.func.isRequired,
  cancelDialog: PropTypes.func.isRequired,
  deleteDialog: PropTypes.shape({
    name: PropTypes.string,
    action: PropTypes.func,
  }),
  errors: PropTypes.shape(),
  children: PropTypes.node,
};

Footer.defaultProps = {
  step: 1,
  maxStep: 1,
  finisDialogText: 'Save changes',
  deleteDialog: null,
  errors: null,
  children: null,
};

const mapStateToProps = (state) => {
  return {
    errors: state.modal.errors,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
