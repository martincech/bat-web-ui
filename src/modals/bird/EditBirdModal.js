import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import {
  ModalHeader,
  ModalBody,
  Container,
  Row,
  Col,
} from '@veit/veit-web-controls';

import ModalActions, { finishDialogEditBird, deleteDialogBird } from '../../store/ModalBird.actions';

import ModalWithLoader from '../ModalWithLoader';
import Footer from '../Footer';
import Step1 from './Step1';
import Step2 from './Step2';

class EditBirdModal extends Component {
  cancel = () => {
    this.props.cancelDialog();
  }

  finish = () => {
    this.props.finishDialogEditBird(this.props.modal.data);
  }

  delete = () => {
    this.props.deleteDialogBird(this.props.modal.data.id);
  }

  render() {
    return (
      <ModalWithLoader
        centered
        isOpen
        keyboard={false}
        toggle={this.cancel}
        contentClassName="modal-fluid"
      >
        <ModalHeader toggle={this.cancel}>Edit Bird</ModalHeader>
        <ModalBody>
          <Container>
            <Row>
              <Col>
                <Step1 edit />
              </Col>
              <Col>
                <Step2 edit />
              </Col>
            </Row>
          </Container>
        </ModalBody>
        <Footer
          deleteDialog={{ action: this.delete, name: 'Bird' }}
          finisDialog={this.finish}
        />
      </ModalWithLoader>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  finishDialogEditBird,
  deleteDialogBird,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

EditBirdModal.propTypes = {
  cancelDialog: PropTypes.func.isRequired,
  finishDialogEditBird: PropTypes.func.isRequired,
  deleteDialogBird: PropTypes.func.isRequired,
  modal: PropTypes.shape({
    data: PropTypes.object,
    type: PropTypes.string,
    target: PropTypes.string,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditBirdModal);
