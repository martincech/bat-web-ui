import React from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ModalActions from '../../store/ModalFlock.actions';

import {
  Label,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
} from '@veit/veit-web-controls';

export function hasStep1Error({ id }) {
  return {
    id: id == null || !validator.isLength(validator.trim(id), { min: 16 }),
  };
}

const Step1 = ({
  modal, bird, updateFormObject, disabled, edit,
}) => {
  const form = modal.data;
  const birds = edit === true ? bird.items : bird.public;
  if (form == null || birds == null) return null;
  const { product, company } = form;
  const companies = birds.map(b => b.company).filter((v, i, a) => a.indexOf(v) === i);
  const selectedBird = birds.find(f => f.id === form.id);
  const productBirds = product ? birds.filter(b => b.product === product) : [];
  const companyProducts = company ? birds.filter(b => b.company === company)
    .map(b => b.product).filter((v, i, a) => a.indexOf(v) === i) : [];

  return (
    <React.Fragment>
      <Label type="text">Company</Label>
      <UncontrolledDropdown>
        <DropdownToggle color="primary" outline caret disabled={disabled || edit}>
          {company}
        </DropdownToggle>
        <DropdownMenu>
          {
            companies.map(c => (
              <DropdownItem
                onClick={() => updateFormObject({
                  company: c,
                  product: null,
                  birdId: null,
                })}
                key={c}
              >
                {c}
              </DropdownItem>
            ))
          }
        </DropdownMenu>
      </UncontrolledDropdown>
      {!edit && <Label type="info">From which company is your new flock</Label>}
      <br />
      <Label type="text">Product</Label>
      <UncontrolledDropdown>
        <DropdownToggle color="primary" outline caret disabled={disabled || edit || form.company == null}>
          {product}
        </DropdownToggle>
        <DropdownMenu>
          {companyProducts.map(p => (
            <DropdownItem
              onClick={() => updateFormObject({ product: p, birdId: null })}
              key={p}
            >
              {p}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </UncontrolledDropdown>
      {!edit && <Label type="info">Which product line of the comapy</Label>}
      <br />
      <Label type="text">Bird</Label>
      <UncontrolledDropdown>
        <DropdownToggle color="primary" outline caret disabled={disabled || edit || form.product == null}>
          {selectedBird && selectedBird.name}
        </DropdownToggle>
        <DropdownMenu>
          {productBirds.map(b => (
            <DropdownItem
              onClick={() => updateFormObject({ ...form, ...b })}
              key={b.id}
            >
              {b.name}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </UncontrolledDropdown>
      {!edit && <Label type="info">Which bird you have</Label>}
    </React.Fragment>
  );
};

Step1.propTypes = {
  modal: PropTypes.shape({
    data: PropTypes.object,
  }).isRequired,
  bird: PropTypes.shape({
    items: PropTypes.array,
  }).isRequired,
  updateForm: PropTypes.func.isRequired,
  updateFormObject: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  edit: PropTypes.bool,
};

Step1.defaultProps = {
  disabled: false,
  edit: false,
};

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
    bird: state.bird,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  updateForm: ModalActions.updateForm,
  updateFormObject: ModalActions.updateFormObject,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Step1);
