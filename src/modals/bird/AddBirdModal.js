import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import ModalActions, { finishDialogAddBird } from '../../store/ModalBird.actions';
import { getPublicBird } from '../../store/Bird.actions';

import {
  ModalHeader,
  ModalBody,
} from '@veit/veit-web-controls';

import AnimateHeight from '../AnimateHeight';

import Footer from '../Footer';
import ModalWithLoader from '../ModalWithLoader';
import Steps from '../Steps';
import Step1, { hasStep1Error } from './Step1';
import Step2 from './Step2';

class AddBirdModal extends Component {
  steps = 2;

  componentDidMount() {
    this.props.getPublicBird(['sex', 'duration', 'dateType']);
    this.props.setValidation({
      1: hasStep1Error,
    });
  }

  getStep() {
    switch (this.props.modal.step) {
      case 2:
        return <Step2 />;
      default:
        return <Step1 />;
    }
  }

  cancel = () => {
    this.props.cancelDialog();
  }

  finish = () => {
    const { data } = this.props.modal;
    const bird = this.props.birds.find(f => f.id === data.birdId) || {};
    this.props.finishDialogAddBird({ ...bird, ...data });
  }

  render() {
    const { step } = this.props.modal;
    return (
      <ModalWithLoader
        centered
        isOpen
        toggle={this.cancel}
        keyboard={false}
        contentClassName="modal-medium"
      >
        <ModalHeader toggle={this.cancel}>Add Bird</ModalHeader>
        <div>
          <Steps steps={this.steps} actualStep={step} />
        </div>
        <ModalBody>
          <AnimateHeight step={step}>
            {this.getStep()}
          </AnimateHeight>
        </ModalBody>
        <Footer
          step={step}
          maxStep={this.steps}
          finisDialog={this.finish}
          finisDialogText="Add Bird"
        />
      </ModalWithLoader>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  setValidation: ModalActions.setValidation,
  finishDialogAddBird,
  getPublicBird,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
    birds: state.bird.items,
  };
};

AddBirdModal.propTypes = {
  cancelDialog: PropTypes.func.isRequired,
  setValidation: PropTypes.func.isRequired,
  finishDialogAddBird: PropTypes.func.isRequired,
  getPublicBird: PropTypes.func.isRequired,
  modal: PropTypes.shape({
    data: PropTypes.object,
    type: PropTypes.string,
    target: PropTypes.string,
    step: PropTypes.number,
  }).isRequired,
  birds: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
  })).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddBirdModal);
