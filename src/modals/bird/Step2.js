import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  ButtonSwitch,
  Label,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Input,
  InputGroup,
} from '@veit/veit-web-controls';

import * as ModalActions from '../../store/Modal.actions';
import { dateType } from '../../utils/enums';

const Step2 = ({ modal, updateForm, edit }) => {
  const form = modal.data;
  if (form == null) return null;
  const durationDays = form.duration || 0;
  const useWeeks = form.dateType === dateType.week;
  const duration = useWeeks ? Math.floor(durationDays / 7) : durationDays;
  return (
    <React.Fragment>
      <Label type="text">Sex</Label>
      <ButtonSwitch
        options={[{ title: 'Male', value: 'male' }, { title: 'Female', value: 'female' }, { title: 'Irrelevant', value: 'irrelevant' }]}
        selected={form.sex}
        onChange={v => updateForm(v, 'sex')}
      />
      {!edit && <Label type="info">What sex is your bird</Label>}
      <br />
      <Label type="text">Duration</Label>
      <InputGroup>
        <Input
          type="number"
          value={duration}
          onChange={e => updateForm(e.target.value * (useWeeks ? 7 : 1), 'duration')}
        />
        <UncontrolledDropdown addonType="append">
          <DropdownToggle caret color="primary" outline className="form-control">
            {dateType.getPlural(form.dateType || dateType.day)}
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem onClick={() => updateForm(dateType.week, 'dateType')}>weeks</DropdownItem>
            <DropdownItem onClick={() => updateForm(dateType.day, 'dateType')}>days</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </InputGroup>
      {!edit && <Label type="info">How old is your flock</Label>}
    </React.Fragment>
  );
};

Step2.propTypes = {
  modal: PropTypes.shape({
    data: PropTypes.object,
  }).isRequired,
  updateForm: PropTypes.func.isRequired,
  edit: PropTypes.bool,
};

Step2.defaultProps = {
  edit: false,
};

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  updateForm: ModalActions.updateForm,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Step2);
