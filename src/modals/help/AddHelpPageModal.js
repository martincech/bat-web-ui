import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ModalActions, { finishDialogAddHelpPage } from '../../store/ModalHelpPage.actions';

import {
  ModalHeader,
  ModalBody,
} from '@veit/veit-web-controls';

import ModalWithLoader from '../ModalWithLoader';
import Footer from '../Footer';
import ModalContent, { hasModalContentError } from './ModalContent';

class AddHelpPageModal extends Component {
  componentDidMount() {
    this.props.setValidation({
      1: hasModalContentError,
    });
  }

  cancel = () => {
    this.props.cancelDialog();
  }

  finish = () => {
    this.props.finishDialogAddHelpPage(this.props.modal.data);
  }

  render() {
    return (
      <ModalWithLoader
        centered
        isOpen
        keyboard={false}
        toggle={this.cancel}
      >
        <ModalHeader toggle={this.cancel}>Add Help page</ModalHeader>
        <ModalBody>
          <ModalContent />
        </ModalBody>
        <Footer finisDialog={this.finish} finisDialogText="Add Page" />
      </ModalWithLoader>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  setValidation: ModalActions.setValidation,
  finishDialogAddHelpPage,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

AddHelpPageModal.propTypes = {
  cancelDialog: PropTypes.func.isRequired,
  setValidation: PropTypes.func.isRequired,
  finishDialogAddHelpPage: PropTypes.func.isRequired,
  modal: PropTypes.shape({
    type: PropTypes.string,
    target: PropTypes.string,
    data: PropTypes.object,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddHelpPageModal);
