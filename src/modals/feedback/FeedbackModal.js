import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ModalActions, { finishDialogSendFeedback } from '../../store/ModalFeedback.actions';
import bem from '../../utils/bem';
import {
  ModalHeader,
  ModalBody,
  Label,
  Input,
  Button,
} from '@veit/veit-web-controls';
import { IMFeedback, IMSadFace } from '@veit/veit-web-controls/dist/icons';

import AnimateHeight from '../AnimateHeight';
import SubmitHandler from '../SubmitHandler';
import { enableKey, disableKey } from '../../utils/keyEvents';
import ModalWithLoader from '../ModalWithLoader';

class FeedbackModal extends Component {
  cancel = () => {
    this.props.cancelDialog();
  }

  finish = () => {
    this.props.finishDialogSendFeedback(this.props.modal.data);
  }

  getTitle = () => {
    switch (this.props.modal.data.positive) {
      case true:
        return (
          <span>
            <IMFeedback style={{ color: 'green' }} />
            Glad to hear that
          </span>
        );
      case false:
        return (
          <span>
            <IMSadFace style={{ color: 'red' }} />
            Sorry to hear that
          </span>
        );
      default:
        return 'Feedback';
    }
  }

  render() {
    if (this.props.modal.data == null) return null;
    const form = this.props.modal.data;
    const bm = bem.modal('feedback');
    return (
      <ModalWithLoader
        centered
        isOpen
        toggle={this.cancel}
        contentClassName={bm.b('modal-medium')}
      >
        <ModalHeader className={bm.e('header')} toggle={this.cancel}>{this.getTitle()}</ModalHeader>
        <ModalBody className={bm.e('body')}>
          <AnimateHeight step={form.positive == null ? 0 : 1}>
            <Label className={bm.e('subtitle')}>
              {
                form.positive == null ? 'How do you feel about us?' : 'Let us know more'
              }
            </Label>
            <br />
            <div className={bm.e('content')}>
              {
                form.positive == null
                  ? (
                    <React.Fragment>
                      <IMFeedback style={{ color: 'green' }} onClick={() => this.props.updateForm(true, 'positive')} />
                      <IMSadFace style={{ color: 'red' }} onClick={() => this.props.updateForm(false, 'positive')} />
                    </React.Fragment>
                  ) : (
                    <div className={bm.e('content-message')}>
                      <Label>More details</Label>
                      <Input
                        type="textarea"
                        value={form.detail}
                        onChange={e => this.props.updateFormEvent(e, 'detail')}
                        onFocus={() => disableKey(13)}
                        onBlur={() => enableKey(13)}
                      />
                      <Label type="info" className={bm.e('content-checkbox')}>
                        <Input type="checkbox" checked={form.sendData || false} onChange={e => this.props.updateFormEvent(e, 'sendData')} />
                        <span onClick={() => this.props.updateForm(!form.sendData, 'sendData')} role="presentation">
                          Include diagnostics data with information about my device and app usage.
                          Data will be processed strictly by developers to solve the problem,
                          no later than 12 months. After that, they’ll be deleted.
                        </span>
                      </Label>
                    </div>
                  )
              }
            </div>
            <br />
            {
              form.positive == null ? null : <Button color="primary" onClick={this.finish}>Send Feedback</Button>
            }
          </AnimateHeight>
        </ModalBody>
        <SubmitHandler keyCode={13} action={this.finish} />
      </ModalWithLoader>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  updateForm: ModalActions.updateForm,
  updateFormEvent: ModalActions.updateFormEvent,
  finishDialogSendFeedback,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

FeedbackModal.propTypes = {
  cancelDialog: PropTypes.func.isRequired,
  updateForm: PropTypes.func.isRequired,
  updateFormEvent: PropTypes.func.isRequired,
  finishDialogSendFeedback: PropTypes.func.isRequired,
  modal: PropTypes.shape({
    type: PropTypes.string,
    target: PropTypes.string,
    data: PropTypes.object,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(FeedbackModal);
