import React from 'react';
import PropTypes from 'prop-types';

import {
  Label,
  Input,
} from '@veit/veit-web-controls';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ModalActions from '../../store/Modal.actions';

const Step2 = ({ modal, updateFormEvent }) => {
  const form = modal.data;
  if (form == null) return null;
  return (
    <React.Fragment>
      <Label type="text">Contact name</Label>
      <Input value={form.contactName || ''} onChange={e => updateFormEvent(e, 'contactName')} />
      <br />
      <Label type="text">Telephone</Label>
      <Input value={form.phone || ''} onChange={e => updateFormEvent(e, 'phone')} />
      <br />
      <Label type="text">E-mail</Label>
      <Input value={form.email || ''} onChange={e => updateFormEvent(e, 'email')} />
    </React.Fragment>
  );
};

Step2.propTypes = {
  modal: PropTypes.shape({
    data: PropTypes.object,
  }).isRequired,
  updateForm: PropTypes.func.isRequired,
  updateFormEvent: PropTypes.func.isRequired,
};


const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  updateForm: ModalActions.updateForm,
  updateFormEvent: ModalActions.updateFormEvent,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Step2);
