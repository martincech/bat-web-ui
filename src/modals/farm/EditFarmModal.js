import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import {
  ModalHeader,
  ModalBody,
  Container,
  Row,
  Col,
} from '@veit/veit-web-controls';

import ModalActions, { finishDialogEditFarm, deleteDialogFarm } from '../../store/ModalFarm.actions';

import ModalWithLoader from '../ModalWithLoader';
import Footer from '../Footer';
import Step1, { hasStep1Error } from './Step1';
import Step2 from './Step2';

class EditFarmModal extends Component {
  componentDidMount() {
    this.props.setValidation({
      1: hasStep1Error,
    });
  }

  cancel = () => {
    this.props.cancelDialog();
  }

  finish = () => {
    this.props.finishDialogEditFarm(this.props.modal.data);
  }

  delete = () => {
    this.props.deleteDialogFarm(this.props.modal.data.id);
  }

  render() {
    return (
      <ModalWithLoader
        centered
        isOpen
        keyboard={false}
        toggle={this.cancel}
        contentClassName="modal-fluid"
      >
        <ModalHeader toggle={this.cancel}>Edit Farm</ModalHeader>
        <ModalBody>
          <Container>
            <Row>
              <Col>
                <Step1 />
              </Col>
              <Col>
                <Step2 />
              </Col>
            </Row>
          </Container>
        </ModalBody>
        <Footer finisDialog={this.finish} deleteDialog={{ action: this.delete, name: 'Farm' }} />
      </ModalWithLoader>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  setValidation: ModalActions.setValidation,
  finishDialogEditFarm,
  deleteDialogFarm,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

EditFarmModal.propTypes = {
  cancelDialog: PropTypes.func.isRequired,
  finishDialogEditFarm: PropTypes.func.isRequired,
  deleteDialogFarm: PropTypes.func.isRequired,
  setValidation: PropTypes.func.isRequired,
  modal: PropTypes.shape({
    type: PropTypes.string,
    target: PropTypes.string,
    data: PropTypes.object,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditFarmModal);
