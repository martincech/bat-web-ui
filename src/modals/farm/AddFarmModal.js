import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ModalActions, { finishDialogAddFarm } from '../../store/ModalFarm.actions';

import {
  ModalHeader,
  ModalBody,
} from '@veit/veit-web-controls';

import ModalWithLoader from '../ModalWithLoader';
import Footer from '../Footer';
import Steps from '../Steps';
import Step1, { hasStep1Error } from './Step1';
import Step2 from './Step2';

class AddFarmModal extends Component {
  steps = 2;

  componentDidMount() {
    this.props.setValidation({
      1: hasStep1Error,
    });
  }

  getStep() {
    switch (this.props.modal.step) {
      case 2:
        return <Step2 />;
      default:
        return <Step1 />;
    }
  }

  cancel = () => {
    this.props.cancelDialog();
  }

  finish = () => {
    this.props.finishDialogAddFarm(this.props.modal.data);
  }

  render() {
    const { step } = this.props.modal;

    return (
      <ModalWithLoader
        centered
        isOpen
        keyboard={false}
        toggle={this.cancel}
        contentClassName="modal-medium"
      >
        <ModalHeader toggle={this.cancel}>Add Farm</ModalHeader>
        <div>
          <Steps steps={this.steps} actualStep={step} />
        </div>
        <ModalBody>
          <br />
          {this.getStep()}
          <br />
        </ModalBody>
        <Footer
          step={step}
          maxStep={this.steps}
          finisDialog={this.finish}
          finisDialogText="Add Farm"
        />
      </ModalWithLoader>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  setValidation: ModalActions.setValidation,
  finishDialogAddFarm,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

AddFarmModal.propTypes = {
  cancelDialog: PropTypes.func.isRequired,
  setValidation: PropTypes.func.isRequired,
  finishDialogAddFarm: PropTypes.func.isRequired,
  modal: PropTypes.shape({
    type: PropTypes.string,
    target: PropTypes.string,
    step: PropTypes.number,
    data: PropTypes.object,
    errors: PropTypes.object,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddFarmModal);
