import React from 'react';
import validator from 'validator';
import {
  Label,
  Input,
} from '@veit/veit-web-controls';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ModalActions from '../../store/Modal.actions';

export function hasStep1Error({ name }) {
  return {
    name: name == null || !validator.isLength(validator.trim(name), { min: 1 }),
  };
}

const Step1 = ({ modal, updateFormEvent }) => {
  const form = modal.data;
  if (form == null) return null;
  return (
    <React.Fragment>
      <Label type="text">Farm name</Label>
      <Input value={form.name || ''} onChange={e => updateFormEvent(e, 'name')} />
      <br />
      <Label type="text">Address</Label>
      <Input value={form.address || ''} onChange={e => updateFormEvent(e, 'address')} />
      <br />
      <Label type="text">Country</Label>
      <Input value={form.country || ''} onChange={e => updateFormEvent(e, 'country')} />
    </React.Fragment>
  );
};

Step1.propTypes = {
  modal: PropTypes.shape({
    data: PropTypes.object,
  }).isRequired,
  updateForm: PropTypes.func.isRequired,
  updateFormEvent: PropTypes.func.isRequired,
};


const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  updateForm: ModalActions.updateForm,
  updateFormEvent: ModalActions.updateFormEvent,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Step1);
