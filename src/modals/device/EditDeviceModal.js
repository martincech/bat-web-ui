import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import {
  ModalHeader,
  ModalBody,
  Container,
  Row,
  Col,
} from '@veit/veit-web-controls';

import ModalActions, { finishDialogEditDevice, deleteDialogDevice } from '../../store/ModalDevice.actions';
import { getFarm, expandFarm } from '../../store/Farm.actions';
import { getHouse } from '../../store/House.actions';
import { getDeviceByParent } from '../../store/Device.actions';
import { deviceType } from '../../utils/enums';

import ModalWithLoader from '../ModalWithLoader';
import Footer from '../Footer';
import Step1, { hasStep1Error } from './Step1';
import Step2, { hasStep2Error } from './Step2';

class EditDeviceModal extends Component {
  componentDidMount() {
    this.props.getFarm([expandFarm.address, expandFarm.country]);
    this.props.getHouse();
    if (this.props.modal.data.parentId != null) {
      this.props.getDeviceByParent(this.props.modal.data.parentId);
    }
    this.props.setValidation({
      1: [hasStep1Error, hasStep2Error],
    });
  }

  cancel = () => {
    this.props.cancelDialog();
  }

  finish = () => {
    this.props.finishDialogEditDevice(this.props.modal.data);
  }

  delete = () => {
    this.props.deleteDialogDevice(
      this.props.modal.data.id,
      this.props.modal.data.type !== deviceType.terminal ? 'scales' : 'connectors',
    );
  }

  render() {
    const type = this.props.modal.data.type !== deviceType.terminal ? 'Scale' : 'Connector';
    return (
      <ModalWithLoader
        centered
        isOpen
        keyboard={false}
        toggle={this.cancel}
        contentClassName="modal-fluid"
      >
        <ModalHeader toggle={this.cancel}>
          {`Edit ${type}`}
        </ModalHeader>
        <ModalBody>
          <Container>
            <Row>
              <Col>
                <Step1 type={type} />
              </Col>
              <Col>
                <Step2 />
              </Col>
            </Row>
          </Container>
        </ModalBody>
        <Footer
          deleteDialog={{ action: this.delete, name: type }}
          fluid
          finisDialog={this.finish}
        />
      </ModalWithLoader>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  setValidation: ModalActions.setValidation,
  finishDialogEditDevice,
  deleteDialogDevice,
  getDeviceByParent,
  getFarm,
  getHouse,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

EditDeviceModal.propTypes = {
  cancelDialog: PropTypes.func.isRequired,
  setValidation: PropTypes.func.isRequired,
  finishDialogEditDevice: PropTypes.func.isRequired,
  deleteDialogDevice: PropTypes.func.isRequired,
  getDeviceByParent: PropTypes.func.isRequired,
  getFarm: PropTypes.func.isRequired,
  getHouse: PropTypes.func.isRequired,
  modal: PropTypes.shape({
    data: PropTypes.object,
    type: PropTypes.string,
    target: PropTypes.string,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditDeviceModal);
