import React from 'react';
import PropTypes from 'prop-types';
import { ModalBody, Button } from '@veit/veit-web-controls';

import { deviceType, deviceTypeName } from '../../utils/enums';

const SelectDeviceType = ({ onSelect }) => {
  return (
    <ModalBody style={{ paddingBottom: '40px' }}>
      <Button color="primary" style={{ width: '100%' }} onClick={() => onSelect(deviceType.bat2Gsm)} outline>
        {deviceTypeName[deviceType.bat2Gsm]}
      </Button>
      <br />
      <br />
      <Button color="primary" style={{ width: '100%' }} onClick={() => onSelect(deviceType.bat2Cable)} outline>
        {deviceTypeName[deviceType.bat2Cable]}
      </Button>
      <br />
    </ModalBody>
  );
};


SelectDeviceType.propTypes = {
  onSelect: PropTypes.func.isRequired,
};

export default SelectDeviceType;
