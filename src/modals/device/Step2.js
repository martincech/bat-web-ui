import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ModalActions from '../../store/Modal.actions';
import { } from '../../utils/enums';
import {
  ButtonSwitch,
  Label,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from '@veit/veit-web-controls';

import AnimateHeight from '../AnimateHeight';

export function hasStep2Error({ farmId, houseId }) {
  return {
    farmId: farmId == null && houseId == null,
  };
}

const Step2 = ({
  modal, parent, farms, houses, updateForm, updateFormObject,
}) => {
  const form = modal.data;
  if (form == null) return null;
  const edit = form.id != null;

  let canEditHouse = true;
  let canEditFarm = true;

  let farm = null;
  let house = null;

  if (parent != null && form.parentId != null) {
    canEditFarm = parent.farmId == null && parent.houseId == null;
    canEditHouse = parent.houseId == null;

    farm = farms.find(f => f.id === parent.farmId);
    house = houses.find(f => f.id === parent.houseId);
  }

  if (canEditFarm) {
    farm = farms.find(f => f.id === form.farmId);
  }

  if (canEditHouse) {
    house = farm != null && form.houseId != null ? houses.find(f => f.id === form.houseId) : null;
  }

  if (farm == null && form.houseId != null) {
    house = houses.find(f => f.id === form.houseId);
    farm = house != null ? farms.find(f => f.id === house.farmId) : null;
  }

  const farmHouses = farm ? houses.filter(f => f.farmId === farm.id) : houses;
  const useHouse = form.useHouse || form.houseId != null;

  return (
    <React.Fragment>
      <Label type="text">Location</Label>
      <ButtonSwitch
        disabled={!canEditHouse}
        options={[{ title: 'Farm', value: false }, { title: 'House', value: true }]}
        selected={useHouse}
        onChange={v => updateFormObject({ useHouse: v, houseId: null, farmId: (farm || {}).id })}
      />
      {!edit && <Label type="info">You can only place sensors to farm</Label>}
      <br />
      <AnimateHeight step={useHouse}>
        <Label type="text">Select farm</Label>
        <UncontrolledDropdown>
          <DropdownToggle color="primary" outline caret disabled={!canEditFarm}>
            {farm && farm.name}
          </DropdownToggle>
          <DropdownMenu>
            {farms.map(p => (
              <DropdownItem
                onClick={() => updateFormObject({
                  farmId: p.id,
                  houseId: null,
                  useHouse: form.houseId != null || form.useHouse,
                })}
                key={p.id}
              >
                {p.name}
              </DropdownItem>
            ))}
          </DropdownMenu>
        </UncontrolledDropdown>
        {
          form.useHouse || house != null ? (
            <React.Fragment>
              <br />
              <Label type="text">Select house</Label>
              <UncontrolledDropdown>
                <DropdownToggle disabled={farm == null || !canEditHouse} caret color="primary" outline className="form-control">
                  {house && house.name}
                </DropdownToggle>
                <DropdownMenu>
                  {farmHouses.map(p => (
                    <DropdownItem
                      onClick={() => updateForm(p.id, 'houseId')}
                      key={p.id}
                    >
                      {p.name}
                    </DropdownItem>
                  ))}
                </DropdownMenu>
              </UncontrolledDropdown>
              {!edit && <Label type="info">You can group sensors to house</Label>}
            </React.Fragment>
          ) : null
        }
      </AnimateHeight>
    </React.Fragment>
  );
};

Step2.propTypes = {
  modal: PropTypes.shape({
    data: PropTypes.object,
  }).isRequired,
  farms: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
    }),
  ).isRequired,
  houses: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
    }),
  ).isRequired,
  updateForm: PropTypes.func.isRequired,
  updateFormEvent: PropTypes.func.isRequired,
  updateFormObject: PropTypes.func.isRequired,
  parent: PropTypes.shape({
    id: PropTypes.string,
  }),
};

Step2.defaultProps = {
  parent: null,
};

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
    parent: state.device.parent,
    farms: state.farm.items,
    houses: state.house.items,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  updateForm: ModalActions.updateForm,
  updateFormEvent: ModalActions.updateFormEvent,
  updateFormObject: ModalActions.updateFormObject,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Step2);
