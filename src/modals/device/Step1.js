import React from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Label,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Input,
} from '@veit/veit-web-controls';

import {
  deviceType, deviceTypeName, unitType, unitTypeName,
} from '../../utils/enums';
import ModalActions from '../../store/ModalFlock.actions';
import { disableKey, enableKey } from '../../utils/keyEvents';

function validatePhone(phone) {
  const regex = /^\+(?:[0-9] ?){6,14}[0-9]$/;
  return regex.test(phone);
}

export function hasStep1Error({ name, phoneNumber, type }) {
  return {
    name: name == null || !validator.isLength(validator.trim(name), { min: 1 }),
    phoneNumber: type === deviceType.bat2Gsm && !validatePhone(phoneNumber),
  };
}

function formatPhoneNumber(e) {
  const number = e.target.value;
  if (number == null) return '+';
  const formated = number.trim();
  return formated[0] === '+' ? formated : `+${formated}`;
}

const Step1 = ({
  modal, updateForm, updateFormEvent, type,
}) => {
  const form = modal.data;
  if (form == null) return null;
  const edit = form.id != null;
  return (
    <React.Fragment>
      <React.Fragment>
        <Label type="text">{`${type} type`}</Label>
        <UncontrolledDropdown>
          <DropdownToggle color="primary" outline caret disabled>
            {deviceTypeName[form.type]}
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem onClick={() => updateForm(deviceType.bat2Gsm, 'type')}>{deviceTypeName[deviceType.bat2Gsm]}</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </React.Fragment>
      {
        form.type === deviceType.bat2Gsm
        && (
          <React.Fragment>
            <br />
            <Label type="text">Phone number</Label>
            <Input type="text" value={form.phoneNumber || '+'} onChange={e => updateForm(formatPhoneNumber(e), 'phoneNumber')} />
            {!edit && <Label type="info">Fill a phone number of SIM card in your GSM Scale</Label>}
            <br />
            <Label type="text">Metric system</Label>
            <UncontrolledDropdown>
              <DropdownToggle color="primary" outline caret>
                {unitTypeName[form.unit]}
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem onClick={() => updateForm(unitType.kg, 'unit')}>{unitTypeName[unitType.kg]}</DropdownItem>
                <DropdownItem onClick={() => updateForm(unitType.lb, 'unit')}>{unitTypeName[unitType.lb]}</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            {!edit && <Label type="info">Choose a metric system of your GSM Scale</Label>}
          </React.Fragment>
        )
      }

      <br />
      <Label type="text">{`${type} name`}</Label>
      <Input className="form-control" type="text" value={form.name || ''} autoFocus onChange={e => updateFormEvent(e, 'name')} />
      <br />
      <Label type="text">{`${type} description`}</Label>
      <Input
        type="textarea"
        value={form.description || ''}
        onChange={e => updateFormEvent(e, 'description')}
        style={{ margin: '12px 0' }}
        onFocus={() => disableKey(13)}
        onBlur={() => enableKey(13)}
      />
      {!edit && <Label type="info">Additional information</Label>}
    </React.Fragment>
  );
};

Step1.propTypes = {
  modal: PropTypes.shape({
    data: PropTypes.object,
  }).isRequired,
  updateForm: PropTypes.func.isRequired,
  updateFormEvent: PropTypes.func.isRequired,
  type: PropTypes.string,
};

Step1.defaultProps = {
  type: 'Scale',
};

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  updateForm: ModalActions.updateForm,
  updateFormEvent: ModalActions.updateFormEvent,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Step1);
