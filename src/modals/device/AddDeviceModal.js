import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ModalActions, { finishDialogAddDevice } from '../../store/ModalDevice.actions';
import { getFarm } from '../../store/Farm.actions';
import { getHouse } from '../../store/House.actions';
import { deviceType, unitType } from '../../utils/enums';
import goTo from '../../store/Router.actions';

import {
  ModalHeader,
  ModalBody,
} from '@veit/veit-web-controls';


import AnimateHeight from '../AnimateHeight';
import SelectDeviceType from './SelectDeviceType';
import ModalWithLoader from '../ModalWithLoader';
import Footer from '../Footer';
import Steps from '../Steps';
import Step1, { hasStep1Error } from './Step1';
import Step2, { hasStep2Error } from './Step2';

class AddDeviceModal extends Component {
  steps = 2;

  componentDidMount() {
    this.props.getFarm();
    this.props.getHouse();
    this.props.setValidation({
      1: hasStep1Error,
      2: hasStep2Error,
    });
  }

  getStep() {
    switch (this.props.modal.step) {
      case 2:
        return <Step2 />;
      default:
        return <Step1 />;
    }
  }

  cancel = () => {
    this.props.cancelDialog();
  }

  finish = () => {
    this.props.finishDialogAddDevice(this.props.modal.data);
  }

  selectDeviceType = (type) => {
    if (type === deviceType.bat2Gsm) {
      this.props.updateFormObject({ type, unit: unitType.kg });
    } else {
      this.props.finishDialogAddDevice({ type: deviceType.bat2Cable });
    }
  }

  render() {
    const { step } = this.props.modal;
    const { type } = this.props.modal.data;
    return (
      <ModalWithLoader
        centered
        isOpen
        keyboard={false}
        toggle={this.cancel}
        contentClassName="modal-medium"
      >
        <ModalHeader toggle={this.cancel}>Add Scale</ModalHeader>

        {type == null
          ? (
            <SelectDeviceType onSelect={this.selectDeviceType} />
          ) : (
            <React.Fragment>
              <div>
                <Steps steps={this.steps} actualStep={step} />
              </div>
              <ModalBody>
                <AnimateHeight step={step}>
                  {this.getStep()}
                </AnimateHeight>
              </ModalBody>
              <Footer
                step={step}
                maxStep={this.steps}
                finisDialog={() => this.props.finishDialogAddDevice(this.props.modal.data)}
                finisDialogText="Add Scale"
              />
            </React.Fragment>
          )}
      </ModalWithLoader>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  cancelDialog: ModalActions.cancelDialog,
  setValidation: ModalActions.setValidation,
  updateFormObject: ModalActions.updateFormObject,
  finishDialogAddDevice,
  getFarm,
  getHouse,
  goTo,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

AddDeviceModal.propTypes = {
  cancelDialog: PropTypes.func.isRequired,
  setValidation: PropTypes.func.isRequired,
  updateFormObject: PropTypes.func.isRequired,
  finishDialogAddDevice: PropTypes.func.isRequired,
  getFarm: PropTypes.func.isRequired,
  getHouse: PropTypes.func.isRequired,
  goTo: PropTypes.func.isRequired,
  modal: PropTypes.shape({
    data: PropTypes.object,
    type: PropTypes.string,
    target: PropTypes.string,
    step: PropTypes.number,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddDeviceModal);
